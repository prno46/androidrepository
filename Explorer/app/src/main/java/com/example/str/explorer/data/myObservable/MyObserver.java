package com.example.str.explorer.data.myObservable;

import java.util.Observable;
import java.util.Observer;

/**
 * Created by str on 18.01.18.
 *
 */

public class MyObserver implements Observer {

    @Override
    public void update(Observable o, Object arg) {
        if (o.hasChanged()) {
            o.notify();
        }
    }
}
