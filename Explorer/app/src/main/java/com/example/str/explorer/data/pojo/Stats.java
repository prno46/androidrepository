package com.example.str.explorer.data.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by str on 25.09.17.
 *
 */

public class Stats {

    @SerializedName("checkinsCount")
    @Expose
    private Long checkinsCount;
    @SerializedName("usersCount")
    @Expose
    private Long usersCount;
    @SerializedName("tipCount")
    @Expose
    private Long tipCount;


    public Stats() {}


    public Stats(Long checkinsCount, Long usersCount, Long tipCount) {
        super();
        this.checkinsCount = checkinsCount;
        this.usersCount = usersCount;
        this.tipCount = tipCount;
    }

    public Long getCheckinsCount() {
        return checkinsCount;
    }

    public void setCheckinsCount(Long checkinsCount) {
        this.checkinsCount = checkinsCount;
    }

    public Stats withCheckinsCount(Long checkinsCount) {
        this.checkinsCount = checkinsCount;
        return this;
    }

    public Long getUsersCount() {
        return usersCount;
    }

    public void setUsersCount(Long usersCount) {
        this.usersCount = usersCount;
    }

    public Stats withUsersCount(Long usersCount) {
        this.usersCount = usersCount;
        return this;
    }

    public Long getTipCount() {
        return tipCount;
    }

    public void setTipCount(Long tipCount) {
        this.tipCount = tipCount;
    }

    public Stats withTipCount(Long tipCount) {
        this.tipCount = tipCount;
        return this;
    }

}
