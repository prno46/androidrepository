package com.example.str.explorer.data.myObservable;

import com.example.str.explorer.data.pojo.Venue;

import java.util.List;
import java.util.Observable;

/**
 * Created by str on 17.01.18.
 *
 */

public class ObservableVenues extends Observable {

    private List<Venue> venues;

    public ObservableVenues() {
        //venues = new ArrayList<>();
    }

    public void addVenues(List<Venue> venue) {
        this.venues = venue;
        this.setChanged();
        this.notifyObservers(venues);
    }

}
