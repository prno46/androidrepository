package com.example.str.explorer.data.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by str on 12.10.17.
 *
 */

public class Reasons {
    @SerializedName("count")
    @Expose
    private Long count;
    @SerializedName("items")
    @Expose
    private List<Item_> items = new ArrayList<Item_>();


    public Reasons() {
    }


    public Reasons(Long count, List<Item_> items) {
        super();
        this.count = count;
        this.items = items;
    }

    public Long getCount() {
        return count;
    }

    public void setCount(Long count) {
        this.count = count;
    }

    public List<Item_> getItems() {
        return items;
    }

    public void setItems(List<Item_> items) {
        this.items = items;
    }
}
