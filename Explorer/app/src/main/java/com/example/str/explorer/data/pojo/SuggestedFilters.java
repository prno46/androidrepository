package com.example.str.explorer.data.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by str on 12.10.17.
 *
 */

public class SuggestedFilters {
    @SerializedName("header")
    @Expose
    private String header;
    @SerializedName("filters")
    @Expose
    private List<Filter> filters = new ArrayList<Filter>();


    public SuggestedFilters() {
    }


    public SuggestedFilters(String header, List<Filter> filters) {
        super();
        this.header = header;
        this.filters = filters;
    }

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public List<Filter> getFilters() {
        return filters;
    }

    public void setFilters(List<Filter> filters) {
        this.filters = filters;
    }
}
