package com.example.str.explorer.data.source;

import java.util.Observer;

/**
 * Created by str on 10.01.18.
 * Used to communication between Repository and Data sources
 */

public interface DataSourceGateway {

    void loadInitialData();
    //List<Venue> provideVenues();
    void cancelApiCall();

    void registerObserver(Observer observer);

}
