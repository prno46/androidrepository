package com.example.str.explorer.data.source;

/**
 * Created by str on 17.01.18.
 *
 */

public interface ApiCallResultCallback {

    void onSuccess();

    void onFailure();

}
