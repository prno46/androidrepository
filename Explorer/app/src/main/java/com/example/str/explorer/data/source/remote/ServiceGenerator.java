package com.example.str.explorer.data.source.remote;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by str on 11.01.18.
 *
 */

class ServiceGenerator {

    private static ServiceGenerator INSTANCE = null;
    private static final String BASE_URL = "https://api.foursquare.com/v2/venues/";
    private static final Object LOCK = new Object();

    private ServiceGenerator () {}

    private static Retrofit.Builder builder = new Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create());
    private static Retrofit retrofit = builder.build();

    <S> S createService(Class<S> serviceClass){
        return retrofit.create(serviceClass);
    }

    public synchronized static ServiceGenerator getINSTANCE() {
        if(INSTANCE == null) {
            synchronized (LOCK) {
                if (INSTANCE == null) {
                    INSTANCE = new ServiceGenerator();
                }
            }
        }
        return INSTANCE;
    }
}
