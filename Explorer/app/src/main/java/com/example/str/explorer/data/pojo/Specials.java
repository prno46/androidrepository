package com.example.str.explorer.data.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by str on 25.09.17.
 *
 */

public class Specials {

    @SerializedName("count")
    @Expose
    private Long count;
    @SerializedName("items")
    @Expose
    private List<Object> items = new ArrayList<Object>();


    public Specials() {}


    public Specials(Long count, List<Object> items) {
        super();
        this.count = count;
        this.items = items;
    }

    public Long getCount() {
        return count;
    }

    public void setCount(Long count) {
        this.count = count;
    }

    public Specials withCount(Long count) {
        this.count = count;
        return this;
    }

    public List<Object> getItems() {
        return items;
    }

    public void setItems(List<Object> items) {
        this.items = items;
    }

    public Specials withItems(List<Object> items) {
        this.items = items;
        return this;
    }

}
