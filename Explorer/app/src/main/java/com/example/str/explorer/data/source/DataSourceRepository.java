package com.example.str.explorer.data.source;

import java.util.Observer;

/**
 * Created by str on 10.01.18.
 *
 */

public class DataSourceRepository implements DataSourceGateway {

    private DataSourceGateway remoteData;

    public DataSourceRepository(DataSourceGateway remoteData) {
        this.remoteData = remoteData;
    }

    // TODO: 10.01.18 use Dagger2 to inject dependencies
    // TODO: 10.01.18 use Retrofit2 to access Foursquare api


    @Override
    public void loadInitialData() {
        remoteData.loadInitialData();
    }

    @Override
    public void registerObserver(Observer observer) {
        remoteData.registerObserver(observer);
    }

    @Override
    public void cancelApiCall() {
        remoteData.cancelApiCall();
    }
}
