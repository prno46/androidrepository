package com.example.str.explorer.data.pojo;

import android.support.annotation.NonNull;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class VenusSearchResponse {

    @SerializedName("meta")
    @Expose
    private Meta meta;
    @SerializedName("response")
    @Expose
    private Response responser;

    public VenusSearchResponse() {
    }

    public VenusSearchResponse(Meta meta, Response responser) {
        super();
        this.meta = meta;
        this.responser = responser;
    }
    @NonNull
    public Meta getMeta() {
        return meta;
    }

    public void setMeta(Meta meta) {
        this.meta = meta;
    }

    public VenusSearchResponse withMeta(Meta meta) {
        this.meta = meta;
        return this;
    }

    public Response getResponse() {

            return responser;
    }

    public void setResponse(Response responser) {
        this.responser = responser;
    }

    public VenusSearchResponse withResponse(Response responser) {
        this.responser = responser;
        return this;
    }
}

