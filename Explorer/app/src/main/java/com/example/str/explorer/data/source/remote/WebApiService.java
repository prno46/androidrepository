package com.example.str.explorer.data.source.remote;

import com.example.str.explorer.data.pojo.VenusSearchResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by str on 11.01.18.
 *
 */

interface WebApiService {

    @GET("explore")
    Call<VenusSearchResponse> getApiResponse(@Query("ll") String userCoordinates
                                             ,@Query("venuePhotos") int photosOn
                                             ,@Query("client_id") String clientId
                                             ,@Query("client_secret") String clientSecret
                                             ,@Query("v") String date);
}
