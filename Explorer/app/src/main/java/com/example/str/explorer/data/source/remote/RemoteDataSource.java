package com.example.str.explorer.data.source.remote;


import android.support.annotation.NonNull;
import android.util.Log;

import com.example.str.explorer.data.AppConstants;
import com.example.str.explorer.data.myObservable.ObservableVenues;
import com.example.str.explorer.data.pojo.Item;
import com.example.str.explorer.data.pojo.Venue;
import com.example.str.explorer.data.pojo.VenusSearchResponse;
import com.example.str.explorer.data.source.DataSourceGateway;

import java.util.ArrayList;
import java.util.List;
import java.util.Observer;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by str on 10.01.18.
 *
 */

public class RemoteDataSource implements DataSourceGateway{

    private final String CLASS = getClass().getSimpleName();
    private final String TAG = "STR";

    private ServiceGenerator serviceGenerator;
    private WebApiService webApiService;
    private Call<VenusSearchResponse> call;
    private ObservableVenues observableVenues;



    public RemoteDataSource() {
        this.serviceGenerator = ServiceGenerator.getINSTANCE();
        observableVenues = new ObservableVenues();


    }

    // TODO: 10.01.18 purpose of this class is to deliver a list of venues
    // TODO: 10.01.18 and pass it to the ui layer by DataSourceGateway
    // TODO: 10.01.18 the current user location is needed to send http request to Foursquare

    private void bindWebApiService(){
        webApiService = serviceGenerator.createService(WebApiService.class);
    }

    private void bindServiceCall() {
        call = webApiService.getApiResponse(
                AppConstants.USER_LOCATION
                ,AppConstants.PHOTOS_ON
                ,AppConstants.CLIENT_ID
                ,AppConstants.CLIENT_SECRET
                ,AppConstants.DATE
        );
    }

    @Override
    public void loadInitialData() {
        bindWebApiService();
        bindServiceCall();
        Log.d(TAG, CLASS+ " in loadInitialData; number of observers " + observableVenues.countObservers());

        call.enqueue(new Callback<VenusSearchResponse>() {
            @Override
            public void onResponse(@NonNull Call<VenusSearchResponse> call, @NonNull Response<VenusSearchResponse> response) {
                // TODO: 17.01.18 use observer pattern to trace data
                List<Venue> venues = new ArrayList<>();
                if (response.isSuccessful() && response.body()!=null) {
                    VenusSearchResponse apiResponse = response.body();
                    if(apiResponse != null) {
                        List<Item> items = apiResponse.getResponse().getGroups().get(0).getItems();
                        for (Item t : items) {
                            venues.add(t.getVenue());
                            Log.d(TAG, CLASS +" in onResponse; name of venue: " + t.getVenue().getName());
                        }

                    }
                }

                observableVenues.addVenues(venues);

            }

            @Override
            public void onFailure(@NonNull Call<VenusSearchResponse> call, @NonNull Throwable t) {

            }
        });
    }

    @Override
    public void registerObserver(Observer observer) {
        observableVenues.addObserver(observer);
    }

    public void cancelApiCall() {
        call.cancel();
    }


}