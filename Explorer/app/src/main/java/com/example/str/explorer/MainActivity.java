package com.example.str.explorer;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.example.str.explorer.data.pojo.Venue;
import com.example.str.explorer.data.source.DataSourceGateway;
import com.example.str.explorer.data.source.DataSourceRepository;
import com.example.str.explorer.data.source.remote.RemoteDataSource;

import java.util.List;
import java.util.Observable;
import java.util.Observer;

/**
 * It is a new version of FoursquareLocationApp -> 10.01.2018
 *
 *Celem jest stworzenie aplikacji prezentującej listę najbardziej popularnych lokali znajdujących
 * się w pobliżu użytkownika. Do ich pobrania należy użyć API Foursquare.
 * Każdy element listy powinien wyświetlać miniaturkę lokalu, jego nazwę i liczbę osób,
 * które zrobiły tam check-in.
 *
 *Kilka rzeczy, na które warto zwrócić uwagę:
 - zadbaj o architekturę aplikacji
 - można popisać się wykorzystując biblioteki “third-parties”
 - staraj się pisać czysty, modularny kod
 - do pobrania local wykorzystaj endpoint search or explore z API Foursquare’a
 - upewnij się, że aplikacja będzie działać offline (bez crasha i wycieków pamięci)
 - jeżeli wystarczy Ci czasu, testy albo ciekawe rozwiązania UX są mile widziane”
 */

/*
    Foursquare url:
    https://api.foursquare.com/v2/venues/explore?ll=54.634636,18.342598&client_id=3EPYLEY4ASFV5TGB51UDLT5OEHVL4ZT3D0KHIF0DCP1X5UKX&client_secret=0USUUTGWYCCFJOANZS0M4OEEBSCCY0SX0TUNLEZ33KZZMGYL&v=20170524

    CLIENT_ID = "3EPYLEY4ASFV5TGB51UDLT5OEHVL4ZT3D0KHIF0DCP1X5UKX";
    CLIENT_SECRET = "0USUUTGWYCCFJOANZS0M4OEEBSCCY0SX0TUNLEZ33KZZMGYL"
 */
public class MainActivity extends AppCompatActivity implements Observer{

    private final String CLASS = getClass().getSimpleName();
    private final String TAG = "STR";

    //only for prototyping

    private DataSourceGateway gateway;
    private List<Venue> venues;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.d(TAG, CLASS+ " in onCreate ");

        gateway = new DataSourceRepository(new RemoteDataSource());
        gateway.registerObserver(this);
        gateway.loadInitialData();

    }

    @Override
    protected void onStop() {
        super.onStop();
        // TODO: 17.01.18 unregister observer, cancel api call
        gateway.cancelApiCall();
    }

    @Override
    public void update(Observable o, Object arg) {
        Log.d(TAG, CLASS + " in update ");
        if(arg != null) {
            Log.d(TAG, CLASS + " in update if -> true ");
            List<Venue> vs = (List<Venue>) arg;
            String name = vs.get(0).getName();
            Log.d(TAG, CLASS + " in update if -> true; venue[0] name: " + name);

        }
    }
}
