package com.example.android.earthquakeinfo;

import android.text.TextUtils;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by str on 18.05.17.
 *
 */

 final class QueryUtils {

    private QueryUtils() {}

     static List<Earthquake> fetchEarthQuakeData (String requestUrl){
        Log.d("STR",  " in fetchEarthQuakeData method in QueryUtils.class " );
        URL url = createUrl(requestUrl);
        String jsonResponse = null;

        try {
            jsonResponse = makeHttpRequest(url);
        }catch (IOException io) {
            Log.e("STR", "Error closing input stream", io);
        }
        return extractDataFromJson(jsonResponse);
    }

    private static URL createUrl (String stringUrl) {
        URL url = null;
        try {
            url = new URL(stringUrl);
        }catch (MalformedURLException e) { Log.d("STR", " QueryUtils.class " + " Error with creating url");}
        return url;
    }

    private static String makeHttpRequest (URL url) throws IOException {
        String jsonResponse = "";
        if (url == null) {return jsonResponse;}

        HttpURLConnection urlConnection = null;
        InputStream inputStream = null;
        try {
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setReadTimeout(10000);
            urlConnection.setConnectTimeout(15000);
            urlConnection.setRequestMethod("GET");
            urlConnection.connect();

            if(urlConnection.getResponseCode() == 200) {
                inputStream = urlConnection.getInputStream();
                jsonResponse = readFromStream(inputStream);
            } else {
                Log.e("STR", "Error response code " + urlConnection.getResponseCode());
            }
        }catch (IOException ex) {
            Log.e("STR", "Error with retrieving JSON results. " + ex);
        } finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
            if(inputStream != null){
                inputStream.close();
            }
        }
        return jsonResponse;
    }

    private static String readFromStream (InputStream inputStream) throws IOException {
        StringBuilder output = new StringBuilder();
        if(inputStream != null) {
            InputStreamReader inputStreamReader = new InputStreamReader(inputStream, Charset.forName("UTF-8"));
            BufferedReader reader = new BufferedReader(inputStreamReader);
            String line = reader.readLine();
            while (line!=null){
                output.append(line);
                line = reader.readLine();
            }
        }
        return output.toString();
    }

    private static List<Earthquake> extractDataFromJson (String jsonResponse) {
        if(TextUtils.isEmpty(jsonResponse)) {
            return null;
        }
        List<Earthquake> quakes = new ArrayList<>();

        try {
            JSONObject baseJObject = new JSONObject(jsonResponse);
            JSONArray featureArray = baseJObject.getJSONArray("features");

            if (featureArray.length()>0) {
                for (int i=0; i< featureArray.length(); i++){
                    JSONObject currentFeature = featureArray.getJSONObject(i);
                    JSONObject properties = currentFeature.getJSONObject("properties");

                    double magnitude = properties.getDouble("mag");
                    String location = properties.getString("place");
                    long time = properties.getLong("time");
                    String url = properties.getString("url");

                    Earthquake earthquake = new Earthquake(magnitude, location, time, url);
                    quakes.add(earthquake);
                }

            }
        } catch (JSONException j) {
            Log.e("STR", "Parsing JSON is not valid" , j);
        }
        return quakes;
    }
}
