package com.example.android.earthquakeinfo;

import android.content.AsyncTaskLoader;
import android.content.Context;
import android.util.Log;

import java.util.List;

/**
 * Created by str on 10.09.17.
 *
 */

class EarthquakeLoader extends AsyncTaskLoader<List<Earthquake>> {

    private String url;

    EarthquakeLoader(Context context, String url) {
        super(context);
        this.url = url;
    }

    @Override
    protected void onStartLoading() {
        Log.d("STR", getClass().getSimpleName() + " in onStartLoading");
        forceLoad();
    }

    @Override
    public List<Earthquake> loadInBackground() {
        Log.d("STR", getClass().getSimpleName() + " in loadInBackground");
        if(url == null) {
            return null;
        }
        return QueryUtils.fetchEarthQuakeData(url);
    }
}
