package com.example.android.earthquakeinfo.model;

import java.util.List;

/**
 * Created by str on 21.05.17.
 */

public class EarthquakeInfo {

    private String type;
    private Metadata metadata;
    private List<Feature> features;
    private double[] bbox;


    public String getType() {
        return type;
    }

    public Metadata getMetadata() {
        return metadata;
    }

    public List<Feature> getFeatures() {
        return features;
    }

    public double[] getBbox() {
        return bbox;
    }
}

