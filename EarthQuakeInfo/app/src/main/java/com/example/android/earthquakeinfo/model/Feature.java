package com.example.android.earthquakeinfo.model;

/**
 * Created by str on 21.05.17.
 */

public class Feature {
    private String type;
    private String id;
    private Properties properties;
    private Geometry geometry;

    public String getType() {
        return type;
    }

    public String getId() {
        return id;
    }

    public Properties getProperties() {
        return properties;
    }

    public Geometry getGeometry() {
        return geometry;
    }
}
