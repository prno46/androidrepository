package com.example.android.earthquakeinfo;

/**
 * Created by str on 18.05.17.
 *
 */

public class Earthquake {

    private double mag;
    private String location;
    private long mTimeInMilliseconds;
    private String url;

    public Earthquake(double mag, String location, long mTimeInMilliseconds, String url) {
        this.mag = mag;
        this.location = location;
        this.mTimeInMilliseconds = mTimeInMilliseconds;
        this.url = url;
    }

    public String getLocation() {
        return location;
    }

    public long getmTimeInMilliseconds() {
        return mTimeInMilliseconds;
    }

    public double getMag() {
        return mag;
    }

    public String getUrl() {
        return url;
    }
}
