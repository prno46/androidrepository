package com.example.android.earthquakeinfo.model;

/**
 * Created by str on 21.05.17.
 */

class Properties {

    private double mag;
    private String place;
    private long time;
    private long updated;
    private int tz;
    private String url;
    private String detail;
    private int felt;
    private double cdi;
    private double mmi;
    private String alert;
    private String status;
    private int tsunami;
    private int sig;
    private String net;
    private String code;
    private String ids;
    private String source;
    private String types;
    private String nst;
    private double dmin;
    private double rms;
    private int gap;
    private String magType;
    private String type;
    private String title;

    public double getMag() {
        return mag;
    }

    public String getPlace() {
        return place;
    }

    public long getTime() {
        return time;
    }

    public String getUrl() {
        return url;
    }
}
