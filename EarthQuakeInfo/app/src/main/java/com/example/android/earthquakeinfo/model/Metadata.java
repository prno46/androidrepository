package com.example.android.earthquakeinfo.model;

/**
 * Created by str on 21.05.17.
 */

class Metadata {
    private long generated;
    private String url;
    private String title;
    private int status;
    private String api;
    private int limit;
    private int offset;
    private int count;
}
