package com.example.str.dbhelperkotlin.data.model

data class SongsAndArtists constructor(
         val songTitle: String="none",
         val artistName: String="none",
         val albumName: String="none",
         val trackNo:Int )