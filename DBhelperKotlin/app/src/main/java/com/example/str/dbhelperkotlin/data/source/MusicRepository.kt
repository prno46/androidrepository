package com.example.str.dbhelperkotlin.data.source

import android.util.Log
import com.example.str.dbhelperkotlin.data.model.SongsAndArtists
import com.example.str.dbhelperkotlin.data.source.local.MusicDataSource

class MusicRepository constructor(private val localRepository: MusicDataSource): MusicDataSource {

    @Suppress("PrivatePropertyName")
    private val TAG: String = "STR ${javaClass.simpleName}"
    val inMemoryCache: MutableMap<Int, SongsAndArtists> = mutableMapOf()
    private var isCacheDirty: Boolean = false

    override fun saveSong(song: SongsAndArtists, callbacks: MusicDataSource.OperationCallbacks) {
        localRepository.saveSong(song, callbacks)
    }

    override fun deleteSongByTitle(song: SongsAndArtists, callback: MusicDataSource.OperationCallbacks) {
        localRepository.deleteSongByTitle(song, callback)
    }

    override fun deleteArtistByName(song: SongsAndArtists, callback: MusicDataSource.OperationCallbacks) {
        localRepository.deleteArtistByName(song, callback)
    }

    override fun deleteAlbumByName(song: SongsAndArtists, callback: MusicDataSource.OperationCallbacks) {
        localRepository.deleteAlbumByName(song, callback)
    }

    override fun editSongTitle(oldSong: SongsAndArtists, newSong: SongsAndArtists, callback: MusicDataSource.OperationCallbacks) {
        localRepository.editSongTitle(oldSong, newSong, callback)
    }

    override fun editSongTrack(oldSong: SongsAndArtists, newSong: SongsAndArtists, callback: MusicDataSource.OperationCallbacks) {
        localRepository.editSongTrack(oldSong, newSong, callback)
    }

    override fun editSongAlbum(oldSong: SongsAndArtists, newSong: SongsAndArtists, callback: MusicDataSource.OperationCallbacks) {
        localRepository.editSongAlbum(oldSong, newSong, callback)
    }

    override fun editSongArtist(oldSong: SongsAndArtists, newSong: SongsAndArtists, callback: MusicDataSource.OperationCallbacks) {
        localRepository.editSongArtist(oldSong, newSong, callback)
    }

    private fun refreshCache(songs: List<SongsAndArtists>) {
        if (inMemoryCache.isNotEmpty()) {
            inMemoryCache.clear()
        }
        for ((key, s: SongsAndArtists) in songs.withIndex()) {
            inMemoryCache[key] = s
        }
        isCacheDirty = false
    }

    override fun getAllSongs(callback: MusicDataSource.LoadAllSongsCallback) {
        Log.d(TAG, " in getAllSongs before first IF - inMemoryCache is ${inMemoryCache.size}")
        if (inMemoryCache.isNotEmpty() && (!isCacheDirty)) {
            callback.onLoadSuccessful(inMemoryCache.values.toList())
            Log.d(TAG, " in getAllSongs before return, inMemoryCache = ${inMemoryCache.size}")
            return
        }
        if (isCacheDirty) {
            Log.d(TAG, " in getAllSongs in second IF - isCacheDirty = $isCacheDirty")
            localRepository.getAllSongs(object: MusicDataSource.LoadAllSongsCallback{
                override fun onLoadSuccessful(songs: List<SongsAndArtists>) {
                    refreshCache(songs)
                    callback.onLoadSuccessful(inMemoryCache.values.toList())
                }

                override fun onLoadFailure() {
                    //good place to fetch data from internet
                }
            })
        }
    }

    override fun refreshData()  {
        isCacheDirty = true
    }

    override fun closeDBConnection() {
        localRepository.closeDBConnection()
    }
}