package com.example.str.dbhelperkotlin.views.addNewSongView

import com.example.str.dbhelperkotlin.views.aCommon.MvpView

interface AddSongMvpView: MvpView {

    fun registerClickListener(listener: SongMvpViewImpl.OnSaveClick): Unit
    fun unregisterClickListener(): Unit
    fun onSuccess(): Unit
    fun onFailed(): Unit
}