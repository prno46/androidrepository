package com.example.str.dbhelperkotlin.views.editOrDeleteView

import android.view.Menu
import android.view.MenuItem
import com.example.str.dbhelperkotlin.data.model.SongsAndArtists
import com.example.str.dbhelperkotlin.databinding.ActivityDeleteAndEditBinding
import com.example.str.dbhelperkotlin.views.aCommon.MvpView

interface DeleteEditMvpView: MvpView {

    fun bindSelectedSong(song: SongsAndArtists): Unit
    fun unBindSelectedSong(): Unit
    fun bindMenuItem(item: MenuItem):Unit
    fun registerOnActionListener(listener: EditDeleteViewImpl.OnActionListener): Unit
    fun unRegisterOnActionListener(): Unit
    fun bindActionMenu(menu: Menu)
    fun getEditBinding(): ActivityDeleteAndEditBinding
}