package com.example.str.dbhelperkotlin.views.editOrDeleteView


import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import com.example.str.dbhelperkotlin.common.BaseActivity
import com.example.str.dbhelperkotlin.data.model.SongsAndArtists
import com.example.str.dbhelperkotlin.views.aCommon.ActionConstants.ALBUM_NAME
import com.example.str.dbhelperkotlin.views.aCommon.ActionConstants.ARTIST_NAME
import com.example.str.dbhelperkotlin.views.aCommon.ActionConstants.SONG_TITLE
import com.example.str.dbhelperkotlin.views.aCommon.ActionConstants.SONG_TRACK_NO
import com.example.str.dbhelperkotlin.views.aCommon.MvpViewFactory
import com.example.str.dbhelperkotlin.views.showAllSongsView.SongsArtistsViewHolder
import javax.inject.Inject

class DeleteAndEditActivity : BaseActivity(), EditDeleteViewImpl.OnActionListener, UseCaseCallback {
    private val TAG: String = "STR ${javaClass.simpleName}"

    private val editDeleteView: DeleteEditMvpView by lazy { mvpViewFactory.newInstance(EditDeleteViewImpl::class.java) }

    @Inject lateinit var mvpViewFactory: MvpViewFactory
    @Inject lateinit var deleteUseCase: DeleteUseCase
    @Inject lateinit var editUseCase: EditUseCase

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Log.d(TAG, "in onCreate")
        presentationComponent.injectInToEditDeleteActivity(this)
        setSupportActionBar(editDeleteView.getEditBinding().toolbarDelete)
    }

    override fun onStart() {
        super.onStart()
        editDeleteView.bindSelectedSong(selectedSong())
        editDeleteView.registerOnActionListener(this)
        deleteUseCase.registerUseCaseCallback(this)
        editUseCase.registerUseCaseCallback(this)
    }

    override fun onStop() {
        super.onStop()
        editDeleteView.unBindSelectedSong()
        editDeleteView.unRegisterOnActionListener()
        deleteUseCase.unRegisterUseCaseCallback()
        editUseCase.unRegisterUseCaseCallback()
    }

    private fun getSelectedSongId(): Int {
        require(intent.extras.containsKey(SongsArtistsViewHolder.KEY_ID)) {
            "KEY_ID is required to obtain the songs Id value" }
        return intent.getIntExtra(SongsArtistsViewHolder.KEY_ID, 111)
    }

    private fun selectedSong(): SongsAndArtists {
        return checkNotNull(editUseCase.getSingleSongFromRepo(getSelectedSongId()))
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        editDeleteView.bindActionMenu(menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        editDeleteView.bindMenuItem(item)
        return super.onOptionsItemSelected(item)
    }

    override fun actionDelete(newSong: SongsAndArtists, action: Int?) {
        if (action == SONG_TRACK_NO) {
            Toast.makeText(this,"Cannot delete a song by its track", Toast.LENGTH_SHORT).show()
        }
        useCaseHandler(action, newSong, deleteUseCase)
    }

    override fun actionEdit(newSong: SongsAndArtists,action: Int?) {
        Log.d(TAG, "in actionEdit, value of action is $action")
        useCaseHandler(action, newSong, editUseCase)
    }

    private fun useCaseHandler(action: Int?, newSong: SongsAndArtists, useCase: UseCase) {
        when(useCase) {
            is DeleteUseCase -> {actionHandler(action, newSong, useCase)}
            is EditUseCase -> {actionHandler(action, newSong, useCase)}
        }
    }

    private fun actionHandler(action: Int?, newSong: SongsAndArtists, useCase: UseCase) {
        when(action) {
            SONG_TITLE -> {useCase.takeActionOnSongTitle(newSong, selectedSong())}
            SONG_TRACK_NO -> {useCase.takeActionOnSongTrackNo(newSong, selectedSong())}
            ALBUM_NAME -> {useCase.takeActionOnAlbumName(newSong, selectedSong())}
            ARTIST_NAME -> {useCase.takeActionOnArtistName(newSong, selectedSong())}
            else ->{Toast.makeText(this, "Select an item to delete or edit", Toast.LENGTH_SHORT).show()}
        }
    }

    override fun onOperationSuccess() {
        finish()
    }

    override fun onOperationFailure() {
        Toast.makeText(this, "Operation was not successful try again", Toast.LENGTH_SHORT).show()
        finish()
    }
}
