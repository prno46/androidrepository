package com.example.str.dbhelperkotlin.data.source.local.sqlite

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.util.Log
import com.example.str.dbhelperkotlin.data.source.local.sqlite.MusicDatabaseUpgradeToV2.AlbumTable.ALTER_ALBUM_TABLE
import com.example.str.dbhelperkotlin.data.source.local.sqlite.MusicDatabaseUpgradeToV2.AlbumTable.COPY_ALL_ALBUMS
import com.example.str.dbhelperkotlin.data.source.local.sqlite.MusicDatabaseUpgradeToV2.AlbumTable.CREATE_ALBUMS_TABLE
import com.example.str.dbhelperkotlin.data.source.local.sqlite.MusicDatabaseUpgradeToV2.AlbumTable.DROP_OLD_ALBUM_TABLE
import com.example.str.dbhelperkotlin.data.source.local.sqlite.MusicDatabaseUpgradeToV2.ArtistTable.ALTER_ARTIST_TABLE
import com.example.str.dbhelperkotlin.data.source.local.sqlite.MusicDatabaseUpgradeToV2.ArtistTable.COPY_ALL_ARTIST
import com.example.str.dbhelperkotlin.data.source.local.sqlite.MusicDatabaseUpgradeToV2.ArtistTable.CREATE_ARTIST_TABLE
import com.example.str.dbhelperkotlin.data.source.local.sqlite.MusicDatabaseUpgradeToV2.ArtistTable.DROP_OLD_ARTIST_TABLE
import com.example.str.dbhelperkotlin.data.source.local.sqlite.MusicDatabaseUpgradeToV2.SongsTable.ALTER_SONGS_TABLE
import com.example.str.dbhelperkotlin.data.source.local.sqlite.MusicDatabaseUpgradeToV2.SongsTable.COPY_ALL_SONGS
import com.example.str.dbhelperkotlin.data.source.local.sqlite.MusicDatabaseUpgradeToV2.SongsTable.CREATE_SONGS_TABLE
import com.example.str.dbhelperkotlin.data.source.local.sqlite.MusicDatabaseUpgradeToV2.SongsTable.DROP_OLD_SONGS_TABLE
import java.lang.Exception
import java.sql.SQLException

class DataBaseHelper constructor(context: Context)
    : SQLiteOpenHelper(
        context.applicationContext,
        MusicDatabaseContract.DATABASE_NAME, null, MusicDatabaseUpgradeToV2.DATABASE_VERSION) {

    @Suppress("PrivatePropertyName")
    private val TAG: String = "STR ${javaClass.simpleName}"

    /** to set the Pragma foreign_keys = on  -> to enable Delete on cascade */
    override fun onOpen(db: SQLiteDatabase) {
        super.onOpen(db)
        if (!db.isReadOnly) {
            Log.d(TAG, "in onOpen")
            db.setForeignKeyConstraintsEnabled(true)
        }
    }

    override fun onCreate(db: SQLiteDatabase) {
        try {
            db.execSQL(CREATE_ARTIST_TABLE)
            db.execSQL(CREATE_ALBUMS_TABLE)
            db.execSQL(CREATE_SONGS_TABLE)
        } catch (e: SQLException ) {
            println("The creation of the tables was unsuccessful")
        }
    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        Log.d(TAG, " in onUpgrade")
        db.setForeignKeyConstraintsEnabled(false)
        val upgradeTo: Int = oldVersion + 1
        when(upgradeTo) {
            2 -> {
                db.beginTransaction()
                try {
                    db.execSQL(ALTER_ARTIST_TABLE)
                    db.execSQL(ALTER_ALBUM_TABLE)
                    db.execSQL(ALTER_SONGS_TABLE)

                    db.execSQL(CREATE_ARTIST_TABLE)
                    db.execSQL(CREATE_ALBUMS_TABLE)
                    db.execSQL(CREATE_SONGS_TABLE)

                    db.execSQL(COPY_ALL_ARTIST)
                    db.execSQL(COPY_ALL_ALBUMS)
                    db.execSQL(COPY_ALL_SONGS)

                    db.execSQL(DROP_OLD_ARTIST_TABLE)
                    db.execSQL(DROP_OLD_ALBUM_TABLE)
                    db.execSQL(DROP_OLD_SONGS_TABLE)
                    db.setTransactionSuccessful()
                }catch (e: Exception){
                    Log.e(TAG, " in onUpgrade ${e.message}")
                }finally {
                    db.endTransaction()
                }
            }
            3 -> {/* todo code for upgrade to V3 if needed*/ }
            4 -> {/* todo code for upgrade to V4 if needed */}
        }
    }
}


