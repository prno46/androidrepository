package com.example.str.dbhelperkotlin.dependency.application

import android.app.Application
import com.example.str.dbhelperkotlin.data.source.local.SqLiteDaoImpl
import com.example.str.dbhelperkotlin.data.source.local.SqliteDao
import com.example.str.dbhelperkotlin.data.source.local.sqlite.DataBaseHelper
import dagger.Module
import dagger.Provides

@Module
class DatabaseModule constructor(private val app: Application) {

    @AppScope
    @Provides
    fun provideSqlInstance(): DataBaseHelper {
        return DataBaseHelper(app)
    }

    @Provides
    fun provideDaoImpl(sqlInstance: DataBaseHelper): SqliteDao {
        return SqLiteDaoImpl(sqlInstance)
    }
}