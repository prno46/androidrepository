package com.example.str.dbhelperkotlin.data.source.local.sqlite

import android.provider.BaseColumns
import com.example.str.dbhelperkotlin.data.source.local.sqlite.MusicDatabaseUpgradeToV2.AlbumTable.ALBUMS_TABLE
import com.example.str.dbhelperkotlin.data.source.local.sqlite.MusicDatabaseUpgradeToV2.ArtistTable.ARTIST_TABLE

object MusicDatabaseUpgradeToV2 {

    const val DATABASE_VERSION: Int = 2

    object ArtistTable: BaseColumns {
        const val ARTIST_TABLE: String = "artists"
        private const val ARTIST_COLUMN_NAME: String = "artistName"
        private const val RENAME_ARTIST_TO: String = "artists_old"


        val CREATE_ARTIST_TABLE: String = """CREATE TABLE $ARTIST_TABLE
            |(${BaseColumns._ID} INTEGER PRIMARY KEY, $ARTIST_COLUMN_NAME TEXT NOT NULL)""".trimMargin()

        val ALTER_ARTIST_TABLE: String = """ALTER TABLE $ARTIST_TABLE
            |RENAME TO $RENAME_ARTIST_TO""".trimMargin()

        val DROP_OLD_ARTIST_TABLE: String = "DROP TABLE IF EXISTS $RENAME_ARTIST_TO"

        val COPY_ALL_ARTIST: String = """INSERT INTO $ARTIST_TABLE
            |SELECT * FROM $RENAME_ARTIST_TO""".trimMargin()

    }

    object AlbumTable: BaseColumns {
        const val ALBUMS_TABLE: String = "albums"
        private const val ALBUM_COLUMN_ARTIST_ID: String = "artistId"
        private const val ALBUM_COLUMN_NAME: String = "albumName"
        private const val FOREIGN_KEY_ARTISTS: String = "fk_artists"
        private const val RENAME_ALBUMS_TO: String = "albums_old"

        val CREATE_ALBUMS_TABLE: String = """CREATE TABLE $ALBUMS_TABLE
            |(${BaseColumns._ID} INTEGER PRIMARY KEY, $ALBUM_COLUMN_NAME TEXT NOT NULL,
            |$ALBUM_COLUMN_ARTIST_ID INTEGER,
            |CONSTRAINT $FOREIGN_KEY_ARTISTS FOREIGN KEY ($ALBUM_COLUMN_ARTIST_ID)
            |REFERENCES $ARTIST_TABLE(${BaseColumns._ID})
            |ON DELETE CASCADE)""".trimMargin()

        val ALTER_ALBUM_TABLE: String = """ALTER TABLE $ALBUMS_TABLE
            |RENAME TO $RENAME_ALBUMS_TO""".trimMargin()

        val DROP_OLD_ALBUM_TABLE: String = "DROP TABLE IF EXISTS $RENAME_ALBUMS_TO"

        val COPY_ALL_ALBUMS: String = """INSERT INTO $ALBUMS_TABLE
            |SELECT * FROM $RENAME_ALBUMS_TO""".trimMargin()
    }

    object SongsTable: BaseColumns {
        private const val SONGS_TABLE: String = "songs"
        private const val SONGS_COLUMN_ALBUM_ID: String = "albumId"
        private const val SONGS_COLUMN_TITLE: String = "songTitle"
        private const val SONGS_COLUMN_TRACK_NO: String = "trackNumber"
        private const val FOREIGN_KEY_ALBUMS: String = "fk_albums"
        private const val RENAME_SONGS_TO: String = "songs_old"

        val CREATE_SONGS_TABLE: String = """CREATE TABLE $SONGS_TABLE
            |(${BaseColumns._ID} INTEGER PRIMARY KEY, $SONGS_COLUMN_ALBUM_ID INTEGER,
            |$SONGS_COLUMN_TITLE TEXT NOT NULL, $SONGS_COLUMN_TRACK_NO INTEGER,
            |CONSTRAINT $FOREIGN_KEY_ALBUMS FOREIGN KEY ($SONGS_COLUMN_ALBUM_ID)
            |REFERENCES $ALBUMS_TABLE (${BaseColumns._ID})
            |ON DELETE CASCADE)""".trimMargin()

        val ALTER_SONGS_TABLE: String = """ALTER TABLE $SONGS_TABLE
            |RENAME TO $RENAME_SONGS_TO""".trimMargin()

        val DROP_OLD_SONGS_TABLE: String = "DROP TABLE IF EXISTS $RENAME_SONGS_TO"

        val COPY_ALL_SONGS: String = """INSERT INTO $SONGS_TABLE
            |SELECT * FROM $RENAME_SONGS_TO""".trimMargin()
    }

}