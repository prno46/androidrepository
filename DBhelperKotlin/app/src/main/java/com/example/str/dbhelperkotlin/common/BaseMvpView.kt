package com.example.str.dbhelperkotlin.common

import android.content.Context
import android.view.View

abstract class BaseMvpView: ViewMvp {

    private lateinit var rootView: View
    val context: Context by lazy { rootView.context }

    protected fun settRootView (root: View) {
        rootView = root
    }

 /*   protected fun <T: View>  findViewById(@IdRes id: Int):T {
        return rootView.findViewById(id) as T
    }*/

    /*protected fun getStringResource (@StringRes id: Int): String {
        return context.getString(id)
    }*/

}

