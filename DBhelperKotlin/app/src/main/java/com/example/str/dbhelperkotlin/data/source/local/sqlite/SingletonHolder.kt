package com.example.str.dbhelperkotlin.data.source.local.sqlite

/**
 * probably the most efficient way to implement Singleton pattern in Kotlin
 * when we cannot use Object class as a Singleton
 * when extending a class that takes parameters
 *
 * when we use dagger we do not need to use this class to provide a singleton
 * if we only instantiate all dependencies only once using Dagger
 * it will simulate ~ a singleton behavior
 */

open class SingletonHolder<out T, in A> constructor(creator: (A) -> T) {
    private var creator: ((A)-> T)? = creator

    @Volatile
    private var instance: T?  = null

    fun getInstance(arg: A):T {
        val i = instance
        if(i != null) {
            return i
        }
        return synchronized(this) {
            val i2 = instance
            if (i2 != null){
                i2
            }else {
                val created = creator!!(arg)
                instance = created
                creator = null
                created
            }
        }
    }

    /** example of usage:
     *  we need to create a companion object that implements this class in the class that we want to be a singleton
     *  example below:
     *  in DataBaseHelper.class
     *  companion object: SingletonHolder<DataBaseHelper, Context>(::DataBaseHelper)
     *
     *  and than just use it in a class where we want to initialize our singleton
     *  val db = DataBaseHelper.getInstance(argument)
     *
     * */
}