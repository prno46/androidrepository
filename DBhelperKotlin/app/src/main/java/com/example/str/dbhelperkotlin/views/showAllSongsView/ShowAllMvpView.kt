package com.example.str.dbhelperkotlin.views.showAllSongsView

import android.view.View
import com.example.str.dbhelperkotlin.data.model.SongsAndArtists
import com.example.str.dbhelperkotlin.databinding.ActivityMainBinding
import com.example.str.dbhelperkotlin.views.aCommon.MvpView

interface ShowAllMvpView: MvpView {

    fun root(): View
    fun showRecyclerView()
    fun showProgressbar()
    fun hideProgressBar()
    fun updateAdapter(songs: List<SongsAndArtists>)
    fun getMainBinding(): ActivityMainBinding
}