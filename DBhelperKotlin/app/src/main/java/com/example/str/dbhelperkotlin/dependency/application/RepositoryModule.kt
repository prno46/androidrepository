package com.example.str.dbhelperkotlin.dependency.application

import com.example.str.dbhelperkotlin.data.executors.ThreadDispatcher
import com.example.str.dbhelperkotlin.data.source.MusicRepository
import com.example.str.dbhelperkotlin.data.source.local.LocalMusicRepository
import com.example.str.dbhelperkotlin.data.source.local.MusicDataSource
import com.example.str.dbhelperkotlin.data.source.local.SqliteDao
import dagger.Module
import dagger.Provides

@Module
class RepositoryModule {

    @Provides
    fun provideLocalMusicRepository(dao: SqliteDao, threadDispatcher: ThreadDispatcher): MusicDataSource {
        return LocalMusicRepository(dao, threadDispatcher)
    }

    @AppScope
    @Provides
    fun provideMusicRepository(localRepo: MusicDataSource): MusicRepository {
        return MusicRepository(localRepo)
    }
}