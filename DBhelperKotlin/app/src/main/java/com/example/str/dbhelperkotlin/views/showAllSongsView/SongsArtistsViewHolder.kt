package com.example.str.dbhelperkotlin.views.showAllSongsView

import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.View
import com.example.str.dbhelperkotlin.data.model.SongsAndArtists
import com.example.str.dbhelperkotlin.databinding.SingleSongArtistBinding
import com.example.str.dbhelperkotlin.views.editOrDeleteView.DeleteAndEditActivity
import kotlin.properties.Delegates

class SongsArtistsViewHolder(private val binding: SingleSongArtistBinding):
        RecyclerView.ViewHolder(binding.root) {

    private val TAG: String = "STR ${javaClass.simpleName}"
    private var myAdapterPosition: Int by Delegates.notNull()

    companion object KeyId {
        const val KEY_ID: String = "adapterId"
    }

    /**
     * code responsible for creation an animation, when you click on item displayed by recycler view
     * the wave goes form the point where you have touched item on the screen
     */
    init {
        binding.holder = this
        binding.root.setOnTouchListener { v, event ->
           v.apply {
               binding.viewHolder.id
               background
               drawableHotspotChanged(event.x, event.y)
           }
           false
       }
    }

    fun onClick(v: View) {
        Log.d(TAG, " in onClick ")
        val intent: Intent = Intent(v.context, DeleteAndEditActivity::class.java)
        with(intent){
            putExtra(KEY_ID, myAdapterPosition) }
        v.context.startActivity(intent)
    }

    fun bind(song: SongsAndArtists, position: Int):Unit {
        binding.song = song
        myAdapterPosition = position
    }
}