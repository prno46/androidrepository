package com.example.str.dbhelperkotlin.data.executors

import java.util.concurrent.Executor
import java.util.concurrent.Executors

class DiskIOThread constructor(
        private val diskIO: Executor = Executors.newSingleThreadExecutor()): Executor {

    override fun execute(command: Runnable?) {
        diskIO.execute(command)
    }
}