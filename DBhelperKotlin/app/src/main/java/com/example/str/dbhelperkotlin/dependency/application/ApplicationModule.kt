package com.example.str.dbhelperkotlin.dependency.application

import com.example.str.dbhelperkotlin.data.executors.ThreadDispatcher
import dagger.Module
import dagger.Provides

@Module
class ApplicationModule {

    @AppScope
    @Provides
    fun provideThreadDispatcher(): ThreadDispatcher {
        return ThreadDispatcher()
    }
}