package com.example.str.dbhelperkotlin.data.source.local

import android.content.ContentValues
import android.database.Cursor
import android.database.SQLException
import android.database.sqlite.SQLiteDatabase
import android.provider.BaseColumns
import android.util.Log
import com.example.str.dbhelperkotlin.data.model.SongsAndArtists
import com.example.str.dbhelperkotlin.data.source.local.sqlite.DataBaseHelper
import com.example.str.dbhelperkotlin.data.source.local.sqlite.MusicDatabaseContract.AlbumTable.ALBUMS_TABLE
import com.example.str.dbhelperkotlin.data.source.local.sqlite.MusicDatabaseContract.AlbumTable.ALBUM_COLUMN_ARTIST_ID
import com.example.str.dbhelperkotlin.data.source.local.sqlite.MusicDatabaseContract.AlbumTable.ALBUM_COLUMN_NAME
import com.example.str.dbhelperkotlin.data.source.local.sqlite.MusicDatabaseContract.ArtistTable.ARTIST_COLUMN_NAME
import com.example.str.dbhelperkotlin.data.source.local.sqlite.MusicDatabaseContract.ArtistTable.ARTIST_TABLE
import com.example.str.dbhelperkotlin.data.source.local.sqlite.MusicDatabaseContract.SongsTable.QUERY_ALL_SONGS_AND_ARTISTS
import com.example.str.dbhelperkotlin.data.source.local.sqlite.MusicDatabaseContract.SongsTable.SONGS_COLUMN_ALBUM_ID
import com.example.str.dbhelperkotlin.data.source.local.sqlite.MusicDatabaseContract.SongsTable.SONGS_COLUMN_TITLE
import com.example.str.dbhelperkotlin.data.source.local.sqlite.MusicDatabaseContract.SongsTable.SONGS_COLUMN_TRACK_NO
import com.example.str.dbhelperkotlin.data.source.local.sqlite.MusicDatabaseContract.SongsTable.SONGS_TABLE

class SqLiteDaoImpl constructor(sqLite: DataBaseHelper): SqliteDao {

    @Suppress("PrivatePropertyName")
    private val TAG: String = "STR ${javaClass.simpleName}"
    private val db: SQLiteDatabase by lazy { sqLite.writableDatabase }

    private fun isInDatabase(searchedValue: String, columnName: String, tableName: String): Long {
        /** projection - insert only those columns which values you are going to use
         *  the columns have zero-based index - counting stats from zero
         *  if you want to return all columns insert null instead of projection
         */
        val projection: Array<String> = arrayOf(BaseColumns._ID, columnName)
        val selection: String = "$columnName LIKE ?"
        val selectionArgs: Array<String> = arrayOf(searchedValue)
        val cursor: Cursor = db.query(tableName, projection, selection, selectionArgs, null, null, null)
        return cursor.use { cr ->
            if (cr.moveToNext() && (cr.getString(1) == searchedValue)) {
                Log.d(TAG, "in isInDatabase method -> $searchedValue is already in database")
                cr.getLong(cr.getColumnIndex("_id"))
            } else {
                Log.d(TAG, "in isInDatabase method -> no such $searchedValue in database")
                -1
            }
        }
    }

    private fun insertIntoArtists(artist: String): Long {
        val isAlreadyInDb: Long = isInDatabase(artist, ARTIST_COLUMN_NAME, ARTIST_TABLE)
        return try {
            when(isAlreadyInDb) {
                -1L -> {
                    val values: ContentValues = ContentValues().apply {
                        put(ARTIST_COLUMN_NAME, artist)
                    }
                    db.insert(ARTIST_TABLE, null, values)
                }
                else -> isAlreadyInDb
            }
        }catch (e: SQLException) {
            Log.d(TAG, "in insertIntoArtists exception was thrown: ${e.message}")
            -1
        }
    }

    private fun insertIntoAlbum(albumName: String, artistId: Long): Long {
        val isAlreadyInDb: Long = isInDatabase(albumName, ALBUM_COLUMN_NAME, ALBUMS_TABLE)
        return try {
            when(isAlreadyInDb) {
                -1L -> {
                    val values: ContentValues = ContentValues().apply {
                        put(ALBUM_COLUMN_NAME, albumName)
                        put(ALBUM_COLUMN_ARTIST_ID, artistId)
                    }
                    db.insert(ALBUMS_TABLE, null, values)
                }
                else -> isAlreadyInDb
            }
        }catch (e: SQLException) {
            Log.d(TAG, "in insertIntoAlbum exception was thrown: ${e.message}")
            -1
        }
    }

    override fun insertIntoSong(songTitle: String, artistName: String, albumName: String, trackNo: Int): Boolean {
        val isAlreadyInDb: Long = isInDatabase(songTitle, SONGS_COLUMN_TITLE, SONGS_TABLE)
        db.beginTransaction()
        return try {
            when(isAlreadyInDb) {
                -1L -> {
                    val artistRowId: Long = insertIntoArtists(artistName)
                    val albumRowId: Long = insertIntoAlbum(albumName, artistRowId)
                    val values: ContentValues = ContentValues().apply {
                        put(SONGS_COLUMN_ALBUM_ID, albumRowId)
                        put(SONGS_COLUMN_TITLE, songTitle)
                        put(SONGS_COLUMN_TRACK_NO, trackNo)
                    }
                    db.insert(SONGS_TABLE, null, values)
                    db.setTransactionSuccessful()
                    Log.d(TAG, "in insertIntoSong transaction was successful")
                    true
                }
                else -> {
                    Log.d(TAG, "in insertIntoSong song already in database")
                    true
                }
            }
        } catch (e: SQLException) {
            Log.d(TAG, "in insertIntoSong transaction not successful ${e.message}")
            Log.d(TAG, "in insertIntoSong preforming a rollback")
            false
        }finally {
            Log.d(TAG, "in insertIntoSong end of transaction")
            db.endTransaction()
        }
    }

    override fun updateSongTitle(newTitle: Any, oldTitle: String): Int {
        return updateTable(SONGS_TABLE, SONGS_COLUMN_TITLE, newTitle, oldTitle)
    }

    override fun updateSongTrack(newTrackNo: Any, oldTrackNo: Int): Int {
        return updateTable(SONGS_TABLE, SONGS_COLUMN_TRACK_NO, newTrackNo, oldTrackNo.toString())
    }

    override fun updateArtistName(newArtistName: Any, oldArtistName: String): Int {
        return updateTable(ARTIST_TABLE, ARTIST_COLUMN_NAME, newArtistName, oldArtistName)
    }

    override fun updateAlbumName(newAlbumName: Any, oldAlbumName: String): Int {
        return updateTable(ALBUMS_TABLE, ALBUM_COLUMN_NAME, newAlbumName, oldAlbumName)
    }

    private fun updateTable(tableToUpdate: String, columnToUpdate: String,
                            newValue: Any, searchedValue: String): Int {
        val values: ContentValues = ContentValues()
        val selection:String = "$columnToUpdate LIKE ?"
        val selectionArgs:Array<String> = arrayOf(searchedValue)
        return try {
            when(newValue) {
                is Number -> {
                    newValue as Int
                    values.put(SONGS_COLUMN_TRACK_NO, newValue)
                }
                else -> {
                    newValue as String
                    values.put(columnToUpdate, newValue)
                }
            }
            db.update(tableToUpdate, values, selection, selectionArgs)
        } catch (e: SQLException) {
            Log.d(TAG, "in updateTable update was not successful: ${e.message}")
            -1
        }
    }

    override fun deleteSongByTitle(songTitle: String): Int {
        val selection: String = "$SONGS_COLUMN_TITLE LIKE ?"
        val selectionArg: Array<String> = arrayOf(songTitle)
        return try {
            db.delete(SONGS_TABLE, selection, selectionArg)
        } catch (e: SQLException) {
            Log.d(TAG, "in deleteSong was not successful ${e.message}")
            -1
        }
    }

    override fun deleteArtistByName(artist: String): Int {
        val selection: String = "$ARTIST_COLUMN_NAME LIKE ?"
        val selectionArg: Array<String> = arrayOf(artist)
        return try {
            db.delete(ARTIST_TABLE, selection, selectionArg)
        } catch (e: SQLException) {
            Log.e(TAG, "in deleteArtistByName ${e.message}")
            -1
        }
    }

    override fun deleteAlbumByName(albumName: String): Int {
        val selection: String = "$ALBUM_COLUMN_NAME LIKE ?"
        val selectionArg: Array<String> = arrayOf(albumName)
        return try {
            db.delete(ALBUMS_TABLE, selection, selectionArg)
        } catch (e: SQLException) {
            Log.e(TAG, "in deleteAlbumByName ${e.message}")
            -1
        }
    }

    override fun queryAllSongs(): List<SongsAndArtists> {
        /**we can use a SQLiteQueryBuilder.class to build a query with inner join*/
        val listOfSongs: MutableList<SongsAndArtists> = mutableListOf()
        lateinit var cursor: Cursor
        try {
            cursor = db.rawQuery(QUERY_ALL_SONGS_AND_ARTISTS, null)
            while (cursor.moveToNext()) {
                listOfSongs.add(SongsAndArtists(
                        cursor.getString(0),
                        cursor.getString(1),
                        cursor.getString(2),
                        cursor.getInt(3))
                )
            }
        } catch (e: SQLException) {
            Log.e(TAG, "in queryAllSongs ${e.message}")
        }finally {
            cursor.close()
        }
        return listOfSongs

        //todo try this with left join instead inner join
    }

    override fun closeSqliteConnection() {
        when(db.isOpen){
            true->db.close()}
    }
}

