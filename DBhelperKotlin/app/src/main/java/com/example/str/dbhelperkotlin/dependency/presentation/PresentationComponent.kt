package com.example.str.dbhelperkotlin.dependency.presentation

import com.example.str.dbhelperkotlin.MainActivity
import com.example.str.dbhelperkotlin.views.addNewSongView.AddNewSong
import com.example.str.dbhelperkotlin.views.editOrDeleteView.DeleteAndEditActivity
import dagger.Subcomponent

@Subcomponent(modules = [PresentationModule::class, UseCaseModule::class])
interface PresentationComponent {

    fun injectInToAddNewSongActivity(activity: AddNewSong)
    fun injectInToEditDeleteActivity(activity: DeleteAndEditActivity)
    fun injectInToMainActivity(activity: MainActivity)
}