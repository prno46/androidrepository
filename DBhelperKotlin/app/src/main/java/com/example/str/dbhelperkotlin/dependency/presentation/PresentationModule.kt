package com.example.str.dbhelperkotlin.dependency.presentation

import android.support.v7.app.AppCompatActivity
import android.view.LayoutInflater
import android.view.MenuInflater
import com.example.str.dbhelperkotlin.views.aCommon.MvpViewFactory
import dagger.Module
import dagger.Provides


@Module
class PresentationModule constructor(private val activity: AppCompatActivity){

    /**Views Implementations*/

    @Provides
    fun provideViewFactory(layoutInflater: LayoutInflater, menuInflater: MenuInflater):MvpViewFactory {
        return MvpViewFactory(menuInflater, layoutInflater)
    }

    /**Commons*/

    @Provides
    fun provideMenuInflater(): MenuInflater {
        return activity.menuInflater
    }

    @Provides
    fun provideLayoutInflater(): LayoutInflater {
        return LayoutInflater.from(activity)
    }
}