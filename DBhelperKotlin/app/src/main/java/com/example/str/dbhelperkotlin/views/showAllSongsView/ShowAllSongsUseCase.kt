package com.example.str.dbhelperkotlin.views.showAllSongsView

import com.example.str.dbhelperkotlin.data.model.SongsAndArtists
import com.example.str.dbhelperkotlin.data.source.MusicRepository
import com.example.str.dbhelperkotlin.data.source.local.MusicDataSource

class ShowAllSongsUseCase constructor(private val repository: MusicRepository) {

    interface ShowAllSongsUseCaseListener {
        fun showProgress()
        fun onDataLoaded(songs: List<SongsAndArtists>)
        fun onLoadedFailed()
    }

    private var isLoadedFirstTime: Boolean = true
    private var listener: ShowAllSongsUseCaseListener? = null

    fun startLoadingData() {
        loadAllSongs(false)
    }

    private fun loadAllSongs(forceUpdate: Boolean) {
        loadSongsAndArtists(forceUpdate || isLoadedFirstTime, true)
        isLoadedFirstTime = false
    }

    private fun loadSongsAndArtists(forceUpdate: Boolean, showLoadingScreen: Boolean) {
        if (showLoadingScreen) {
            listener?.showProgress()
        }
        if (forceUpdate) {
            repository.refreshData()
        }
        repository.getAllSongs(object: MusicDataSource.LoadAllSongsCallback{
            override fun onLoadSuccessful(songs: List<SongsAndArtists>) {
                listener?.onDataLoaded(songs)
            }

            override fun onLoadFailure() {
                listener?.onLoadedFailed()
            }
        })
    }

    fun registerListener(useCaseListener: ShowAllSongsUseCaseListener?) {
        listener = useCaseListener
    }

    fun unregisterListener() {
        listener = null
    }
}