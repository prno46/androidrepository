package com.example.str.dbhelperkotlin.views.aCommon

object ActionConstants {

    const val SONG_TITLE: Int = 10
    const val SONG_TRACK_NO: Int = 20
    const val ALBUM_NAME: Int = 30
    const val ARTIST_NAME: Int = 40
}