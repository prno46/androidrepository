package com.example.str.dbhelperkotlin.data.source.local

import com.example.str.dbhelperkotlin.data.executors.ThreadDispatcher
import com.example.str.dbhelperkotlin.data.model.SongsAndArtists

class LocalMusicRepository constructor(
        private val dao: SqliteDao,
        private val doInBackground: ThreadDispatcher): MusicDataSource {

    override fun saveSong(song: SongsAndArtists, callbacks: MusicDataSource.OperationCallbacks) {
        val saveRunnable: Runnable = Runnable {
            val save: Boolean = dao.insertIntoSong(song.songTitle, song.artistName, song.albumName, song.trackNo)
            /**the code below allows to communicate the results to UI Thread*/
            doInBackground.mainThread.execute(Runnable {
                when(save) {
                    false -> callbacks.onFailure()
                    true -> callbacks.onSuccess()
                }
            })
        }
        doInBackground.diskIO.execute(saveRunnable)
    }

    override fun deleteSongByTitle(song: SongsAndArtists, callback: MusicDataSource.OperationCallbacks) {
        val deleteRunnable: Runnable = Runnable {
            val delete: Int = dao.deleteSongByTitle(song.songTitle)
            doInBackground.mainThread.execute(Runnable {
                when(delete) {
                    -1 -> callback.onFailure()
                    else -> callback.onSuccess()
                }
            })
        }
        doInBackground.diskIO.execute(deleteRunnable)
    }

    override fun deleteAlbumByName(song: SongsAndArtists, callback: MusicDataSource.OperationCallbacks) {
        val deleteAlbum: Runnable = Runnable {
            val delete: Int = dao.deleteAlbumByName(song.albumName)
            doInBackground.mainThread.execute{
                when(delete){
                    -1 -> callback.onFailure()
                    else -> callback.onSuccess()
                }
            }
        }
        doInBackground.diskIO.execute(deleteAlbum)
    }

    override fun deleteArtistByName(song: SongsAndArtists, callback: MusicDataSource.OperationCallbacks) {
        val deleteArtistRunnable: Runnable = Runnable {
            val deleteArt: Int = dao.deleteArtistByName(song.artistName)
            doInBackground.mainThread.execute{
                when(deleteArt) {
                    -1 -> {callback.onFailure()}
                    else -> {callback.onSuccess()}
                }
            }
        }
        doInBackground.diskIO.execute(deleteArtistRunnable)
    }

    override fun editSongTitle(oldSong: SongsAndArtists, newSong: SongsAndArtists, callback: MusicDataSource.OperationCallbacks) {
        val editRunnable: Runnable = Runnable {
            val edit: Int = dao.updateSongTitle(newSong.songTitle, oldSong.songTitle)
            doInBackground.mainThread.execute(Runnable {
                when(edit) {
                    -1 -> callback.onFailure()
                    else -> callback.onSuccess()
                }
            })
        }
        doInBackground.diskIO.execute(editRunnable)
    }

    override fun editSongTrack(oldSong: SongsAndArtists, newSong: SongsAndArtists, callback: MusicDataSource.OperationCallbacks) {
        val editRunnable: Runnable = Runnable {
            val edit: Int = dao.updateSongTrack(newSong.trackNo, oldSong.trackNo)
            doInBackground.mainThread.execute(Runnable {
                when(edit) {
                    -1 -> callback.onFailure()
                    else -> callback.onSuccess()
                }
            })
        }
        doInBackground.diskIO.execute(editRunnable)
    }

    override fun editSongAlbum(oldSong: SongsAndArtists, newSong: SongsAndArtists, callback: MusicDataSource.OperationCallbacks) {
        val editRunnable: Runnable = Runnable {
            val edit: Int = dao.updateAlbumName(newSong.albumName, oldSong.albumName)
            doInBackground.mainThread.execute(Runnable {
                when(edit) {
                    -1 -> callback.onFailure()
                    else -> callback.onSuccess()
                }
            })
        }
        doInBackground.diskIO.execute(editRunnable)
    }

    override fun editSongArtist(oldSong: SongsAndArtists, newSong: SongsAndArtists, callback: MusicDataSource.OperationCallbacks) {
        val editRunnable: Runnable = Runnable {
            val edit: Int = dao.updateArtistName(newSong.artistName, oldSong.artistName)
            doInBackground.mainThread.execute(Runnable {
                when(edit) {
                    -1 -> callback.onFailure()
                    else -> callback.onSuccess()
                }
            })
        }
        doInBackground.diskIO.execute(editRunnable)
    }

    override fun getAllSongs(callback: MusicDataSource.LoadAllSongsCallback) {
        val getAllSongs: Runnable = Runnable {
            val songs: List<SongsAndArtists>? = dao.queryAllSongs()
            doInBackground.mainThread.execute {
                when(songs) {
                    null -> callback.onLoadFailure()
                    else -> callback.onLoadSuccessful(songs)
                }
            }
        }
        doInBackground.diskIO.execute(getAllSongs)
    }

    override fun closeDBConnection() {
        dao.closeSqliteConnection()
    }

    override fun refreshData() {
        TODO("not implemented - no need to implement this method - think of refactoring - Interface segregation")
    }
}