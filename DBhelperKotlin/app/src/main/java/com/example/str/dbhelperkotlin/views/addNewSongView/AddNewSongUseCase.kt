package com.example.str.dbhelperkotlin.views.addNewSongView

import android.util.Log
import com.example.str.dbhelperkotlin.data.model.SongsAndArtists
import com.example.str.dbhelperkotlin.data.source.local.MusicDataSource

class AddNewSongUseCase constructor(private val repo: MusicDataSource){

    interface SaveSongUseCaseListener {
        fun saveSuccessful(): Unit
        fun saveFailed(): Unit
    }

    private val TAG: String = "STR ${javaClass.simpleName}"
    private var useCaseListener: SaveSongUseCaseListener? = null

    fun registerUseCaseListener(listener: SaveSongUseCaseListener): Unit {
        useCaseListener = listener
    }

    fun unregisterUseCaseListener(): Unit {
        useCaseListener = null
    }

    fun saveNewSong(song: SongsAndArtists): Unit {
        Log.d(TAG, "in saveNewSong method")
        repo.saveSong(song, object: MusicDataSource.OperationCallbacks{
            override fun onSuccess() {
                useCaseListener?.saveSuccessful()
            }

            override fun onFailure() {
                useCaseListener?.saveFailed()
            }
        })
        repo.refreshData()
    }
}