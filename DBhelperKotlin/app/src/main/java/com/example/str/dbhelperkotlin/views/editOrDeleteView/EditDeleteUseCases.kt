package com.example.str.dbhelperkotlin.views.editOrDeleteView

import android.util.Log
import com.example.str.dbhelperkotlin.data.model.SongsAndArtists
import com.example.str.dbhelperkotlin.data.source.MusicRepository
import com.example.str.dbhelperkotlin.data.source.local.MusicDataSource


interface UseCaseCallback {
    fun onOperationSuccess()
    fun onOperationFailure()
}


sealed class UseCase {

    var callback: UseCaseCallback? = null

    abstract fun takeActionOnSongTitle(newSong: SongsAndArtists, oldSong:SongsAndArtists)
    abstract fun takeActionOnSongTrackNo(newSong: SongsAndArtists, oldSong:SongsAndArtists)
    abstract fun takeActionOnAlbumName(newSong: SongsAndArtists, oldSong:SongsAndArtists)
    abstract fun takeActionOnArtistName(newSong: SongsAndArtists, oldSong:SongsAndArtists)
    abstract fun registerUseCaseCallback(useCaseCallBack: UseCaseCallback?)
    abstract fun unRegisterUseCaseCallback()
    abstract fun getSingleSongFromRepo(id: Int): SongsAndArtists?

}

class DeleteUseCase constructor(private val repo: MusicRepository): UseCase() {

    override fun registerUseCaseCallback(useCaseCallBack: UseCaseCallback?){
        callback = useCaseCallBack
    }

    override fun unRegisterUseCaseCallback() {
        callback = null
    }

    override fun takeActionOnSongTitle(newSong: SongsAndArtists, oldSong:SongsAndArtists) {
        deleteSongByTitle(newSong)
    }

    private fun deleteSongByTitle(newSong: SongsAndArtists) {
        repo.deleteSongByTitle(newSong, object: MusicDataSource.OperationCallbacks {
            override fun onSuccess() {
                callback?.onOperationSuccess()
            }
            override fun onFailure() {
                callback?.onOperationFailure()
            }
        })
        repo.refreshData()
    }

    override fun takeActionOnSongTrackNo(newSong: SongsAndArtists, oldSong:SongsAndArtists) {}

    override fun takeActionOnAlbumName(newSong: SongsAndArtists, oldSong:SongsAndArtists) {
        deleteSongByAlbumName(newSong)
    }

    private fun deleteSongByAlbumName(newSong: SongsAndArtists) {
        repo.deleteAlbumByName(newSong, object: MusicDataSource.OperationCallbacks{
            override fun onSuccess() {
                callback?.onOperationSuccess()
            }
            override fun onFailure() {
                callback?.onOperationFailure()
            }
        })
        repo.refreshData()
    }

    override fun takeActionOnArtistName(newSong: SongsAndArtists, oldSong:SongsAndArtists) {
        deleteSongByArtistName(newSong)
    }

    private fun deleteSongByArtistName(newSong: SongsAndArtists) {
        repo.deleteArtistByName(newSong, object: MusicDataSource.OperationCallbacks{
            override fun onSuccess() {
                callback?.onOperationSuccess()
            }
            override fun onFailure() {
                callback?.onOperationFailure()
            }
        })
        repo.refreshData()
    }

    override fun getSingleSongFromRepo(id: Int): SongsAndArtists? {
        return repo.inMemoryCache[id]
    }
}


class EditUseCase constructor(private val repo: MusicRepository): UseCase() {

    private val TAG: String = "STR ${javaClass.simpleName}"

    override fun registerUseCaseCallback(useCaseCallBack: UseCaseCallback?) {
        callback = useCaseCallBack
    }

    override fun unRegisterUseCaseCallback() {
        callback = null
    }

    override fun takeActionOnSongTitle(newSong: SongsAndArtists, oldSong:SongsAndArtists) {
        editSongTitle(newSong, oldSong)
    }

    private fun editSongTitle(newSong: SongsAndArtists, oldSong:SongsAndArtists) {
        repo.editSongTitle(oldSong, newSong, object: MusicDataSource.OperationCallbacks{
            override fun onSuccess() {
                callback?.onOperationSuccess()
            }
            override fun onFailure() {
                callback?.onOperationFailure()
            }
        })
        repo.refreshData()
    }

    override fun takeActionOnSongTrackNo(newSong: SongsAndArtists, oldSong:SongsAndArtists) {
        Log.d(TAG, "oldSong track: ${oldSong.trackNo}, newSong track: ${newSong.trackNo}")
        editSongTrack(oldSong, newSong)
    }

    private fun editSongTrack(oldSong: SongsAndArtists, newSong: SongsAndArtists) {
        repo.editSongTrack(oldSong, newSong, object: MusicDataSource.OperationCallbacks{
            override fun onSuccess() {
                callback?.onOperationSuccess()
            }
            override fun onFailure() {
                callback?.onOperationFailure()
            }
        })
        repo.refreshData()
    }

    override fun takeActionOnAlbumName(newSong: SongsAndArtists, oldSong:SongsAndArtists) {
        editSongAlbum(oldSong, newSong)
    }

    private fun editSongAlbum(oldSong: SongsAndArtists, newSong: SongsAndArtists) {
        repo.editSongAlbum(oldSong, newSong, object: MusicDataSource.OperationCallbacks{
            override fun onSuccess() {
                callback?.onOperationSuccess()
            }
            override fun onFailure() {
                callback?.onOperationFailure()
            }
        })
        repo.refreshData()
    }

    override fun takeActionOnArtistName(newSong: SongsAndArtists, oldSong:SongsAndArtists) {
        editSongArtist(oldSong, newSong)
    }

    private fun editSongArtist(oldSong: SongsAndArtists, newSong: SongsAndArtists) {
        repo.editSongArtist(oldSong, newSong, object: MusicDataSource.OperationCallbacks{
            override fun onSuccess() {
                callback?.onOperationSuccess()
            }
            override fun onFailure() {
                callback?.onOperationFailure()
            }
        })
        repo.refreshData()
    }

    override fun getSingleSongFromRepo(id: Int): SongsAndArtists? {
        return repo.inMemoryCache[id]

    }
}
