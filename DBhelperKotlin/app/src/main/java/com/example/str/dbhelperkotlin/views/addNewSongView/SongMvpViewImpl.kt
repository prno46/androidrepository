package com.example.str.dbhelperkotlin.views.addNewSongView

import android.app.Activity
import android.databinding.DataBindingUtil
import android.view.LayoutInflater
import android.widget.Toast
import com.example.str.dbhelperkotlin.R
import com.example.str.dbhelperkotlin.common.BaseMvpView
import com.example.str.dbhelperkotlin.data.model.SongsAndArtists
import com.example.str.dbhelperkotlin.databinding.ActivityAddNewSongBinding

class SongMvpViewImpl constructor(private val inflater: LayoutInflater):
        BaseMvpView(), AddSongMvpView {

    private  val TAG: String = "STR ${javaClass.simpleName}"

    interface OnSaveClick{
        fun onFabClicked(song: SongsAndArtists): Unit
    }

    private val newSong: SongsAndArtists by lazy { createNewSong() }
    private var fabClickListener: OnSaveClick? = null

    private val binding: ActivityAddNewSongBinding by lazy {
        DataBindingUtil.setContentView<ActivityAddNewSongBinding>(
                (inflater.context) as Activity,
                R.layout.activity_add_new_song
        )
    }

    init {
        binding.songViewImp = this
    }

    override fun registerClickListener(listener: OnSaveClick): Unit {
        fabClickListener = listener
    }

    override fun unregisterClickListener(): Unit {
        fabClickListener = null
    }

    override fun onSuccess(): Unit {
        Toast.makeText(binding.root.context, "${newSong.songTitle} was successfully saved", Toast.LENGTH_SHORT).show()
    }

    override fun onFailed(): Unit {
        Toast.makeText(binding.root.context, "${newSong.songTitle} -> save has failed", Toast.LENGTH_SHORT).show()
    }

    fun fabClicked() {
        Toast.makeText(binding.root.context, "Song was saved", Toast.LENGTH_SHORT).show()
        fabClickListener?.onFabClicked(newSong)
    }

    private fun createNewSong(): SongsAndArtists {
        return SongsAndArtists(
                binding.newSongTitle.text.toString(),
                binding.newArtistName.text.toString(),
                binding.newAlbumName.text.toString(),
                binding.newTrackNumber.text.toString().toInt()
        )
    }
}