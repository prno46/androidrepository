package com.example.str.dbhelperkotlin.temp

import com.example.str.dbhelperkotlin.data.executors.ThreadDispatcher
import com.example.str.dbhelperkotlin.data.source.local.LocalMusicRepository
import com.example.str.dbhelperkotlin.data.source.local.SqLiteDaoImpl
import com.example.str.dbhelperkotlin.data.source.local.sqlite.DataBaseHelper
import kotlin.properties.ReadOnlyProperty
import kotlin.reflect.KProperty

/**example of kotlin delegation property - ensures that we use a vale only once*/
fun <T> onlyOnce(initializer: () -> T) = object : ReadOnlyProperty<Any?, T> {
    private var isInitialized = false

    override fun getValue(thisRef: Any?, property: KProperty<*>): T {
        if (isInitialized) {
            throw RuntimeException("This variable shouldn't be accessed multiple times")
        }
        isInitialized = true
        return initializer()
    }
}

fun injectLocalMusicRepository(sql: DataBaseHelper, dispatcher: ThreadDispatcher): LocalMusicRepository {
    return LocalMusicRepository(
            SqLiteDaoImpl(sql), dispatcher
    )
}

