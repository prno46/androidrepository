package com.example.str.dbhelperkotlin.views.addNewSongView

import android.os.Bundle
import android.util.Log
import com.example.str.dbhelperkotlin.common.BaseActivity
import com.example.str.dbhelperkotlin.data.model.SongsAndArtists
import com.example.str.dbhelperkotlin.views.aCommon.MvpViewFactory
import javax.inject.Inject

class AddNewSong : BaseActivity(), SongMvpViewImpl.OnSaveClick, AddNewSongUseCase.SaveSongUseCaseListener {

    private val TAG: String = "STR ${javaClass.simpleName}"

    private val viewMvp: AddSongMvpView by lazy { viewFactory.newInstance(SongMvpViewImpl::class.java) }
    @Inject lateinit var viewFactory: MvpViewFactory
    @Inject lateinit var addUseCase: AddNewSongUseCase

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Log.d(TAG, "in onCreate")
        presentationComponent.injectInToAddNewSongActivity(this)
    }

    override fun onStart() {
        super.onStart()
        viewMvp.registerClickListener(this)
        addUseCase.registerUseCaseListener(this)
    }

    override fun onStop() {
        super.onStop()
        viewMvp.unregisterClickListener()
        addUseCase.unregisterUseCaseListener()
    }

    override fun onFabClicked(song: SongsAndArtists) {
        addUseCase.saveNewSong(song)
        finish()
    }

    override fun saveSuccessful() {
        viewMvp.onSuccess()
    }

    override fun saveFailed() {
        viewMvp.onFailed()
    }
}
