package com.example.str.dbhelperkotlin.common

import android.support.v7.app.AppCompatActivity
import com.example.str.dbhelperkotlin.application.MyApplication
import com.example.str.dbhelperkotlin.dependency.application.ApplicationComponent
import com.example.str.dbhelperkotlin.dependency.presentation.PresentationComponent
import com.example.str.dbhelperkotlin.dependency.presentation.PresentationModule
import com.example.str.dbhelperkotlin.dependency.presentation.UseCaseModule

/**
 * this class is the base class for all activities
 * is required in implementing a DI to the project
 */

open class BaseActivity : AppCompatActivity() {

    private val appComponent: ApplicationComponent by lazy {(application as MyApplication).appComponent }

    @Suppress("LeakingThis")
    val presentationComponent: PresentationComponent by lazy {
       appComponent.presentationComponent(PresentationModule(this), UseCaseModule())
    }
}
