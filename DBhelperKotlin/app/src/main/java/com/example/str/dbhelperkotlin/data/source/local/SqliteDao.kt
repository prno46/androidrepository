package com.example.str.dbhelperkotlin.data.source.local

import com.example.str.dbhelperkotlin.data.model.SongsAndArtists

interface SqliteDao {

    fun insertIntoSong(songTitle: String, artistName: String, albumName: String, trackNo: Int): Boolean
    fun closeSqliteConnection()
    fun queryAllSongs(): List<SongsAndArtists>
    fun deleteArtistByName(artist: String): Int
    fun deleteSongByTitle (songTitle:String): Int
    fun deleteAlbumByName(albumName: String): Int
    fun updateSongTitle(newTitle: Any, oldTitle: String): Int
    fun updateSongTrack(newTrackNo: Any, oldTrackNo: Int): Int
    fun updateArtistName(newArtistName: Any, oldArtistName: String ): Int
    fun updateAlbumName(newAlbumName: Any, oldAlbumName: String): Int
}