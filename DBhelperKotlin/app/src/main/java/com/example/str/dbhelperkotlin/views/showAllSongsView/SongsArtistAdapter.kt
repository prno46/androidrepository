package com.example.str.dbhelperkotlin.views.showAllSongsView

import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.example.str.dbhelperkotlin.R
import com.example.str.dbhelperkotlin.data.model.SongsAndArtists
import com.example.str.dbhelperkotlin.databinding.SingleSongArtistBinding

class SongsArtistAdapter (private var listOfSongs: List<SongsAndArtists>)
                                            : RecyclerView.Adapter<SongsArtistsViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): SongsArtistsViewHolder {
        val binding: SingleSongArtistBinding = DataBindingUtil
                .inflate(LayoutInflater.from(parent?.context),
                         R.layout.single_song_artist, parent, false)
        return SongsArtistsViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return listOfSongs.size
    }

    override fun onBindViewHolder(holder: SongsArtistsViewHolder, position: Int) {
        holder.bind(listOfSongs[position], holder.adapterPosition)
    }

    fun provideNewDataSet(newListOfSongs: List<SongsAndArtists> ): Unit {
        listOfSongs = newListOfSongs
    }

}