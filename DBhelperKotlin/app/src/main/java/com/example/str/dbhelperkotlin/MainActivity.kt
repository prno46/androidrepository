package com.example.str.dbhelperkotlin

import android.os.Bundle
import android.util.Log
import com.example.str.dbhelperkotlin.common.BaseActivity
import com.example.str.dbhelperkotlin.data.model.SongsAndArtists
import com.example.str.dbhelperkotlin.views.aCommon.MvpViewFactory
import com.example.str.dbhelperkotlin.views.showAllSongsView.ShowAllMvpView
import com.example.str.dbhelperkotlin.views.showAllSongsView.ShowAllMvpViewImpl
import com.example.str.dbhelperkotlin.views.showAllSongsView.ShowAllSongsUseCase
import javax.inject.Inject


class MainActivity : BaseActivity(), ShowAllSongsUseCase.ShowAllSongsUseCaseListener {
    private val TAG: String = "STR ${javaClass.simpleName}"

    private val showAllView: ShowAllMvpView by lazy { mvpViewFactory.newInstance(ShowAllMvpViewImpl::class.java) }
    @Inject lateinit var mvpViewFactory: MvpViewFactory
    @Inject lateinit var showAllSongsUseCase: ShowAllSongsUseCase

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Log.d(TAG," in onCreate ")
        presentationComponent.injectInToMainActivity(this)
        setSupportActionBar(showAllView.getMainBinding().toolbar)
    }

    override fun showProgress() {
        showAllView.showProgressbar()
    }

    override fun onDataLoaded(songs: List<SongsAndArtists>) {
        showAllView.updateAdapter(songs)
        showAllView.hideProgressBar()
    }

    override fun onLoadedFailed() {
        showAllView.hideProgressBar()
    }

    override fun onStart() {
        super.onStart()
        showAllSongsUseCase.registerListener(this)
        showAllSongsUseCase.startLoadingData()
        showAllView.showRecyclerView()
    }

    override fun onStop() {
        super.onStop()
        showAllSongsUseCase.unregisterListener()
    }
}

