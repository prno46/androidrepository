package com.example.str.dbhelperkotlin.dependency.presentation

import javax.inject.Scope

@MustBeDocumented
@Scope
annotation class PresentationScope