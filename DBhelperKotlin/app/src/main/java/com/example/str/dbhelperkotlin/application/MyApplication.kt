package com.example.str.dbhelperkotlin.application

import android.app.Application
import android.util.Log
import com.example.str.dbhelperkotlin.dependency.application.ApplicationComponent
import com.example.str.dbhelperkotlin.dependency.application.DaggerApplicationComponent
import com.example.str.dbhelperkotlin.dependency.application.DatabaseModule

class MyApplication: Application() {

    @Suppress("PrivatePropertyName")
    private val TAG: String = "STR ${javaClass.simpleName}"

    val appComponent: ApplicationComponent by lazy { DaggerApplicationComponent.builder()
            .databaseModule(DatabaseModule(this))
            .build()
    }

    override fun onCreate() {
        super.onCreate()
        Log.d(TAG, "in onCreate")
    }
}