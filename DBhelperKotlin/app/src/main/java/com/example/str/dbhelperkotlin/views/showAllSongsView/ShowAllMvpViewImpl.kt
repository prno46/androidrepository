package com.example.str.dbhelperkotlin.views.showAllSongsView

import android.app.Activity
import android.content.Intent
import android.databinding.DataBindingUtil
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.ProgressBar
import com.example.str.dbhelperkotlin.R
import com.example.str.dbhelperkotlin.common.BaseMvpView
import com.example.str.dbhelperkotlin.data.model.SongsAndArtists
import com.example.str.dbhelperkotlin.databinding.ActivityMainBinding
import com.example.str.dbhelperkotlin.views.addNewSongView.AddNewSong

class ShowAllMvpViewImpl constructor(
        private val layoutInflater: LayoutInflater): BaseMvpView(), ShowAllMvpView {

    private val TAG: String = "STR ${javaClass.simpleName}"

    private val mainActivityBinding: ActivityMainBinding by lazy {
        DataBindingUtil.setContentView<ActivityMainBinding>((layoutInflater.context) as Activity , R.layout.activity_main)
    }
    private val progressBar: ProgressBar by lazy { mainActivityBinding.progressBar }
    private val recyclerView: RecyclerView  by lazy {
        mainActivityBinding.recyclerViewSongsArtists
    }
    private val recyclerViewDivider: DividerItemDecoration by lazy {
        DividerItemDecoration(mainActivityBinding.root.context, LinearLayoutManager(mainActivityBinding.root.context).orientation)
    }
    private val recyclerViewAdapter: SongsArtistAdapter by lazy { SongsArtistAdapter(emptyList())}

    init {
        Log.d(TAG, "in init block")
        mainActivityBinding.fragmentView = this
        recyclerViewDivider.setDrawable(
                mainActivityBinding.root.context.resources.getDrawable(R.drawable.devider,
                        mainActivityBinding.root.context.applicationContext.theme)
        )
    }

    override fun root(): View {
        return mainActivityBinding.root
    }

    fun addNewSong(view: View) {
        mainActivityBinding.fabAddSong.setOnClickListener {
            val intent: Intent = Intent(mainActivityBinding.root.context, AddNewSong::class.java)
            mainActivityBinding.root.context.startActivity(intent)
        }
    }

    override fun showRecyclerView() {
        recyclerView.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(mainActivityBinding.root.context)
            adapter = recyclerViewAdapter
            addItemDecoration(recyclerViewDivider)
        }
    }

    override fun showProgressbar() {
        progressBar.visibility = View.VISIBLE
    }

    override fun hideProgressBar() {
        progressBar.visibility = View.GONE
    }

    override fun updateAdapter(songs: List<SongsAndArtists>) {
        recyclerViewAdapter.provideNewDataSet(songs)
        recyclerViewAdapter.notifyDataSetChanged()
    }

    override fun getMainBinding(): ActivityMainBinding {
        return mainActivityBinding
    }

}



