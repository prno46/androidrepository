package com.example.str.dbhelperkotlin.data.source.local

import com.example.str.dbhelperkotlin.data.model.SongsAndArtists

interface MusicDataSource  {

    interface OperationCallbacks {
        fun onSuccess(): Unit
        fun onFailure(): Unit
    }

    interface LoadAllSongsCallback {
        fun onLoadSuccessful(songs: List<SongsAndArtists>): Unit
        fun onLoadFailure(): Unit
    }

    fun saveSong(song: SongsAndArtists, callbacks: OperationCallbacks): Unit
    fun getAllSongs(callback: LoadAllSongsCallback): Unit
    fun refreshData(): Unit
    fun closeDBConnection(): Unit
    fun deleteArtistByName(song: SongsAndArtists, callback: OperationCallbacks): Unit
    fun deleteSongByTitle(song: SongsAndArtists, callback: OperationCallbacks): Unit
    fun deleteAlbumByName(song: SongsAndArtists, callback: OperationCallbacks): Unit
    fun editSongTitle (oldSong: SongsAndArtists, newSong: SongsAndArtists, callback: OperationCallbacks): Unit
    fun editSongTrack(oldSong: SongsAndArtists, newSong: SongsAndArtists, callback: OperationCallbacks):Unit
    fun editSongAlbum(oldSong: SongsAndArtists, newSong: SongsAndArtists, callback: OperationCallbacks):Unit
    fun editSongArtist(oldSong: SongsAndArtists, newSong: SongsAndArtists, callback: OperationCallbacks):Unit

}
