package com.example.str.dbhelperkotlin.views.editOrDeleteView

import android.app.Activity
import android.databinding.DataBindingUtil
import android.util.Log
import android.view.*
import android.widget.CheckBox
import com.example.str.dbhelperkotlin.R
import com.example.str.dbhelperkotlin.common.BaseMvpView
import com.example.str.dbhelperkotlin.data.model.SongsAndArtists
import com.example.str.dbhelperkotlin.databinding.ActivityDeleteAndEditBinding
import com.example.str.dbhelperkotlin.views.aCommon.ActionConstants.ALBUM_NAME
import com.example.str.dbhelperkotlin.views.aCommon.ActionConstants.ARTIST_NAME
import com.example.str.dbhelperkotlin.views.aCommon.ActionConstants.SONG_TITLE
import com.example.str.dbhelperkotlin.views.aCommon.ActionConstants.SONG_TRACK_NO

class EditDeleteViewImpl constructor(
        private val layoutInflater: LayoutInflater,
        private val menuInflater: MenuInflater): BaseMvpView(), DeleteEditMvpView {

    private val TAG: String = "STR ${javaClass.simpleName}"

    interface OnActionListener {
        fun actionDelete(newSong: SongsAndArtists, action: Int?): Unit
        fun actionEdit(newSong: SongsAndArtists, action: Int?): Unit
    }

    private val binding: ActivityDeleteAndEditBinding by lazy {
        DataBindingUtil
                .setContentView<ActivityDeleteAndEditBinding> ((layoutInflater.context) as Activity, R.layout.activity_delete_and_edit)
    }

    private lateinit var menuItem: MenuItem
    private var onActionListener: OnActionListener? = null
    private var toEditOrDelete: Int? = null
    private lateinit var actionMenu: Menu

    init {
        Log.d(TAG, "in init block binding instance = $binding")
        settRootView(binding.root)
        binding.viewImpl = this
    }

    override fun getEditBinding():ActivityDeleteAndEditBinding {
        return binding
    }

    override fun registerOnActionListener(listener: OnActionListener) {
        onActionListener = listener
    }

    override fun unRegisterOnActionListener() {
        onActionListener = null
    }

    override fun bindSelectedSong(song: SongsAndArtists): Unit {
        Log.d(TAG, "the song title ${song.songTitle}")
        binding.mySong = song
    }

    override fun unBindSelectedSong() {
        binding.mySong = null
    }

    override fun bindActionMenu(menu: Menu) {
        actionMenu = menu
        menuInflater.inflate(R.menu.menu_toolbar, actionMenu)
        setActionMenuVisibility(false)
    }

    private fun setActionMenuVisibility(isVisible: Boolean) {
        when(isVisible) {
            false -> {actionMenu.setGroupVisible(R.id.menuItemGroup, false)}
            else -> {actionMenu.setGroupVisible(R.id.menuItemGroup, true)}
        }
    }

    override fun bindMenuItem(item: MenuItem): Unit {
        menuItem = item
        menuActions()
    }

    private fun menuActions(): Unit {
        when (menuItem.itemId) {
            R.id.delete -> { onActionListener?.actionDelete(binding.mySong!!, toEditOrDelete)}
            R.id.edit -> { onActionListener?.actionEdit(createNewSong(), toEditOrDelete)}
        }
    }

    fun onCheckBoxClicked(view: View): Unit {
        val checked: Boolean = (view as CheckBox).isChecked
        setActionMenuVisibility(checked)
        Log.d(TAG," in onCheckBoxClicked value of checked is $checked")
        when (view.id) {
            R.id.checkBoxTitle -> { setOperation(SongTitleCheckBox(binding, checked)) }
            R.id.checkBoxTrack -> { setOperation(SongTrackCheckBox(binding, checked)) }
            R.id.checkBoxAlbum -> { setOperation(AlbumNameCheckBox(binding, checked)) }
            R.id.checkBoxArtist -> { setOperation(ArtistNameCheckBox(binding, checked))}
        }
    }

    private fun setOperation(op: Operations) {
        toEditOrDelete = when (op) {
            is SongTitleCheckBox -> { SONG_TITLE }
            is SongTrackCheckBox -> { SONG_TRACK_NO }
            is AlbumNameCheckBox -> { ALBUM_NAME }
            is ArtistNameCheckBox -> { ARTIST_NAME }
            else -> {null}
        }
        Log.d(TAG, "in setOperation value of toEditOrDelete is $toEditOrDelete")
    }

    private fun createNewSong(): SongsAndArtists {
        return SongsAndArtists(
                binding.editSong.text.toString(),
                binding.editArtist.text.toString(),
                binding.editAlbum.text.toString(),
                binding.editTrack.text.toString().toInt()
        )
    }

}