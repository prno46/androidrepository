package com.example.str.dbhelperkotlin.dependency.application

import com.example.str.dbhelperkotlin.dependency.presentation.PresentationComponent
import com.example.str.dbhelperkotlin.dependency.presentation.PresentationModule
import com.example.str.dbhelperkotlin.dependency.presentation.UseCaseModule
import dagger.Component

@AppScope
@Component(modules = [ApplicationModule::class, DatabaseModule::class, RepositoryModule::class])
interface ApplicationComponent {

    fun presentationComponent(presentationModule: PresentationModule, useCaseModule: UseCaseModule): PresentationComponent
}