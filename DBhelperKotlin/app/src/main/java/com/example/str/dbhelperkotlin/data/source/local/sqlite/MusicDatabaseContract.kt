package com.example.str.dbhelperkotlin.data.source.local.sqlite

import android.provider.BaseColumns
import com.example.str.dbhelperkotlin.data.source.local.sqlite.MusicDatabaseContract.AlbumTable.ALBUMS_TABLE
import com.example.str.dbhelperkotlin.data.source.local.sqlite.MusicDatabaseContract.AlbumTable.ALBUM_COLUMN_ARTIST_ID
import com.example.str.dbhelperkotlin.data.source.local.sqlite.MusicDatabaseContract.AlbumTable.ALBUM_COLUMN_NAME
import com.example.str.dbhelperkotlin.data.source.local.sqlite.MusicDatabaseContract.ArtistTable.ARTIST_COLUMN_NAME
import com.example.str.dbhelperkotlin.data.source.local.sqlite.MusicDatabaseContract.ArtistTable.ARTIST_TABLE

object MusicDatabaseContract {
    const val DATABASE_NAME: String = "music.db"
    const val DATABASE_VERSION: Int = 1
    const val ENABLE_FOREIGN_KEYS: String = "PRAGMA foreign_keys = ON"

    object AlbumTable: BaseColumns {
        /**when we implement BaseColumns we do not need to specify ID field
         * it is given by default, we access it by  - BaseColumns._ID -
         */
        const val ALBUMS_TABLE: String = "albums"
        const val ALBUM_COLUMN_ARTIST_ID: String = "artistId"
        const val ALBUM_COLUMN_NAME: String = "albumName"

        /** SQL queries and commands*/

        /**we do not need to use IF NOT EXISTS because onCreate method is called only once
         * when database with this name does not exists, if it already exists this method is not
         * invoked
         */
        val CREATE_ALBUMS_TABLE: String = """CREATE TABLE $ALBUMS_TABLE
            |(${BaseColumns._ID} INTEGER PRIMARY KEY, $ALBUM_COLUMN_NAME TEXT NOT NULL,
            |$ALBUM_COLUMN_ARTIST_ID INTEGER)""".trimMargin()
    }

    object ArtistTable: BaseColumns {
        const val ARTIST_TABLE: String = "artists"
        const val ARTIST_COLUMN_NAME: String = "artistName"

        val CREATE_ARTIST_TABLE: String = """CREATE TABLE $ARTIST_TABLE
            |(${BaseColumns._ID} INTEGER PRIMARY KEY, $ARTIST_COLUMN_NAME TEXT NOT NULL)""".trimMargin()
        /*val INSERT_ARTIST_NAME: String = """INSERT INTO $ARTIST_TABLE ($ARTIST_COLUMN_NAME)
            |VALUES (?)""".trimMargin()
        val SELECT_ALL_FROM_ARTISTS: String = """SELECT * FROM $ARTIST_TABLE""".trimMargin()*/
    }

    object SongsTable: BaseColumns {
        const val SONGS_TABLE: String = "songs"
        const val SONGS_COLUMN_ALBUM_ID: String ="albumId"
        const val SONGS_COLUMN_TITLE: String = "songTitle"
        const val SONGS_COLUMN_TRACK_NO: String = "trackNumber"

        /** SQL queries and commands*/
        val CREATE_SONGS_TABLE: String = """CREATE TABLE $SONGS_TABLE
            |(${BaseColumns._ID} INTEGER PRIMARY KEY, $SONGS_COLUMN_ALBUM_ID INTEGER,
            |$SONGS_COLUMN_TITLE TEXT NOT NULL, $SONGS_COLUMN_TRACK_NO INTEGER)""".trimMargin()

        val QUERY_ALL_SONGS_AND_ARTISTS: String = """SELECT $SONGS_COLUMN_TITLE, $ARTIST_TABLE.$ARTIST_COLUMN_NAME,
            |$ALBUMS_TABLE.$ALBUM_COLUMN_NAME, $SONGS_COLUMN_TRACK_NO FROM $SONGS_TABLE
            |INNER JOIN $ALBUMS_TABLE ON $SONGS_TABLE.$SONGS_COLUMN_ALBUM_ID = $ALBUMS_TABLE.${BaseColumns._ID}
            |INNER JOIN $ARTIST_TABLE ON $ALBUMS_TABLE.$ALBUM_COLUMN_ARTIST_ID = $ARTIST_TABLE.${BaseColumns._ID}
            |ORDER BY $ARTIST_TABLE.$ARTIST_COLUMN_NAME ASC""".trimMargin()
    }
}