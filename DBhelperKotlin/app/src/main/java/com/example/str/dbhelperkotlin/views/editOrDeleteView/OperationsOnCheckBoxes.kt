package com.example.str.dbhelperkotlin.views.editOrDeleteView

import com.example.str.dbhelperkotlin.databinding.ActivityDeleteAndEditBinding

sealed class Operations

class SongTitleCheckBox constructor(binding: ActivityDeleteAndEditBinding, checked: Boolean): Operations(){
    init {
        when(checked) {
            true -> {
                binding.checkBoxTrack.isEnabled  = false
                binding.checkBoxAlbum.isEnabled  = false
                binding.checkBoxArtist.isEnabled = false
            }
            else -> NoOperationSelected(binding)
        }
    }
}

class SongTrackCheckBox constructor(binding: ActivityDeleteAndEditBinding, checked: Boolean): Operations(){
    init {
        when(checked) {
            true -> {
                binding.checkBoxTitle.isEnabled  = false
                binding.checkBoxAlbum.isEnabled  = false
                binding.checkBoxArtist.isEnabled = false
            }
            else -> NoOperationSelected(binding)
        }
    }
}

class AlbumNameCheckBox constructor(binding: ActivityDeleteAndEditBinding, checked: Boolean): Operations(){
    init {
        when(checked) {
            true -> {
                binding.checkBoxTitle.isEnabled  = false
                binding.checkBoxTrack.isEnabled  = false
                binding.checkBoxArtist.isEnabled = false
            }
            else -> NoOperationSelected(binding)
        }
    }
}

class ArtistNameCheckBox constructor(binding: ActivityDeleteAndEditBinding, checked: Boolean): Operations(){
    init {
        when(checked) {
            true -> {
                binding.checkBoxTitle.isEnabled  = false
                binding.checkBoxTrack.isEnabled  = false
                binding.checkBoxAlbum.isEnabled  = false
            }
            else -> NoOperationSelected(binding)
        }
    }
}

class NoOperationSelected constructor(binding: ActivityDeleteAndEditBinding): Operations(){
    init {
        binding.checkBoxTitle.isEnabled  = true
        binding.checkBoxTrack.isEnabled  = true
        binding.checkBoxAlbum.isEnabled  = true
        binding.checkBoxArtist.isEnabled = true
    }
}