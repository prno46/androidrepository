package com.example.str.dbhelperkotlin.dependency.application

import javax.inject.Scope

@MustBeDocumented
@Scope
annotation class AppScope