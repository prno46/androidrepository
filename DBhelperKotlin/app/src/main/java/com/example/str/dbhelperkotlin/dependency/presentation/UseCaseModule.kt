package com.example.str.dbhelperkotlin.dependency.presentation

import com.example.str.dbhelperkotlin.data.source.MusicRepository
import com.example.str.dbhelperkotlin.views.addNewSongView.AddNewSongUseCase
import com.example.str.dbhelperkotlin.views.editOrDeleteView.DeleteUseCase
import com.example.str.dbhelperkotlin.views.editOrDeleteView.EditUseCase
import com.example.str.dbhelperkotlin.views.showAllSongsView.ShowAllSongsUseCase
import dagger.Module
import dagger.Provides

@Module
class UseCaseModule {

    /**Use cases*/
    @Provides
    fun provideSaveNewSongUseCase(repository: MusicRepository): AddNewSongUseCase {
        return AddNewSongUseCase(repository)
    }

    @Provides
    fun provideDeleteUseCase(repository: MusicRepository): DeleteUseCase {
        return DeleteUseCase(repository)
    }

    @Provides
    fun provideEditUseCase(repository: MusicRepository): EditUseCase {
        return EditUseCase(repository)
    }

    @Provides
    fun provideShowAllSongsUseCase(repository: MusicRepository): ShowAllSongsUseCase {
        return ShowAllSongsUseCase(repository)
    }
}