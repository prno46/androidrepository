package com.example.str.dbhelperkotlin.views.aCommon

import android.view.LayoutInflater
import android.view.MenuInflater
import com.example.str.dbhelperkotlin.views.addNewSongView.SongMvpViewImpl
import com.example.str.dbhelperkotlin.views.editOrDeleteView.EditDeleteViewImpl
import com.example.str.dbhelperkotlin.views.showAllSongsView.ShowAllMvpViewImpl

class MvpViewFactory constructor(
         val menuInflater: MenuInflater,
         val layoutInflater: LayoutInflater) {

    private  val TAG: String = "STR ${javaClass.simpleName}"

    inline fun <reified T: MvpView>  newInstance(viewClass: Class<T>): T {
        lateinit var mvpView: MvpView
        mvpView = when (viewClass) {
             SongMvpViewImpl::class.java -> SongMvpViewImpl(layoutInflater)
             EditDeleteViewImpl::class.java -> EditDeleteViewImpl(layoutInflater, menuInflater)
             ShowAllMvpViewImpl::class.java -> ShowAllMvpViewImpl(layoutInflater)
            else -> throw IllegalArgumentException("Unsupported view class ${mvpView::class.simpleName}")
        }
        return  mvpView as T
    }
}