package com.example.str.dbhelperkotlin.data.executors

import android.os.Handler
import android.os.Looper
import java.util.concurrent.Executor


class ThreadDispatcher constructor(
        val diskIO: Executor = DiskIOThread(),
       // private val networkIO: Executor = Executors.newFixedThreadPool(THREAD_COUNT),
        val mainThread: Executor = MainThread){

     companion object MainThread : Executor {
        //private const val THREAD_COUNT: Int = 2
        private val mainThreadHandler: Handler = Handler(Looper.getMainLooper())
        @JvmStatic
        override fun execute(command: Runnable?) {
            mainThreadHandler.post(command)
        }
    }
}