package com.spectral.databindingtestapp

import android.databinding.DataBindingUtil
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.spectral.databindingtestapp.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity(), BasicView  {

    private lateinit var bind: ActivityMainBinding
    private lateinit var presenter: Presenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        bind = DataBindingUtil.setContentView(this, R.layout.activity_main)
        presenter = Presenter()
        presenter.setBasicView(this)
        bind.pres = presenter
    }

    override fun displayText(message: String) {
        bind.textView.text = message
    }
}
