package com.spectral.databindingtestapp


class Presenter {

    private val str: String = "some text form presenter"
    private lateinit var customView: BasicView

    fun display () {
        customView.displayText(str)
    }

    fun setBasicView(view: BasicView){
        customView = view
    }
}