package com.spectral.databindingtestapp

interface BasicView {

    fun displayText(message: String): Unit
}