package com.spectral.databindingtestapp;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 10}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0006\u0010\u0007\u001a\u00020\bJ\u000e\u0010\t\u001a\u00020\b2\u0006\u0010\n\u001a\u00020\u0004R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082D\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000b"}, d2 = {"Lcom/spectral/databindingtestapp/Presenter;", "", "()V", "customView", "Lcom/spectral/databindingtestapp/BasicView;", "str", "", "display", "", "setBasicView", "view", "app_debug"})
public final class Presenter {
    private final java.lang.String str = "some text form presenter";
    private com.spectral.databindingtestapp.BasicView customView;
    
    public final void display() {
    }
    
    public final void setBasicView(@org.jetbrains.annotations.NotNull()
    com.spectral.databindingtestapp.BasicView view) {
    }
    
    public Presenter() {
        super();
    }
}