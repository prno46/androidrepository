package com.str.favoritelocationsfoursquare;

import android.arch.core.executor.testing.InstantTaskExecutorRule;
import android.arch.lifecycle.LiveData;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.str.favoritelocationsfoursquare.pojo.FQVenusSearchResponse;
import com.str.favoritelocationsfoursquare.pojo.Venue;
import com.str.favoritelocationsfoursquare.retrofit.FoursquareService;
import com.str.favoritelocationsfoursquare.retrofit.LiveDataCallAdapterFactory;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.mock.BehaviorDelegate;
import retrofit2.mock.MockRetrofit;
import retrofit2.mock.NetworkBehavior;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertTrue;

/**
 * Created by str on 17.12.17.
 * Testing foursquareservis with Retrofit mock
 */

@RunWith(JUnit4.class)
public class FoursquareServiceTest {

    @Rule // allow excecurtion of async code on main thread
    public InstantTaskExecutorRule instantTaskExecutorRule = new InstantTaskExecutorRule();

    private  Response<FQVenusSearchResponse> response;

    @Before
    public  void initMockRetrofit() throws InterruptedException, IOException {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://api.foursquare.com/v2/venues/")
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(new LiveDataCallAdapterFactory())
                .build();

        NetworkBehavior networkBehavior = NetworkBehavior.create();

        MockRetrofit mockRetrofit = new MockRetrofit.Builder(retrofit)
                .networkBehavior(networkBehavior)
                .build();

        BehaviorDelegate<FoursquareService> behaviorDelegate = mockRetrofit.create(FoursquareService.class);
        MockFourSquareService mockFourSquareService = new MockFourSquareService(behaviorDelegate);

        Call<FQVenusSearchResponse> call =  mockFourSquareService.getListOfVenues2(
                "54.634636,18.342598", 1
                ,"3EPYLEY4ASFV5TGB51UDLT5OEHVL4ZT3D0KHIF0DCP1X5UKX"
                ,"0USUUTGWYCCFJOANZS0M4OEEBSCCY0SX0TUNLEZ33KZZMGYL"
                ,"20170524"
        );
        response = call.execute();
    }

    @Test
    public void shouldConnectSuccesfully()  {

        assertTrue("Connection with mockRetrofit is unsuccessfull " ,response.isSuccessful());
        assertEquals("Response code is invalid ", 200, response.code());
    }


    @Test
    public void shouldContainFiveVenueObjects() {
        int numberOfVenues = response.body().getResponse().getGroups().get(0).getItems().size();
        assertEquals("There should be five items ", 5 ,numberOfVenues);
    }

    @Test
    public void shouldVerifyDetailsOfFirstVenueObject() {

        Venue venue = response.body().getResponse().getGroups().get(0).getItems().get(0).getVenue();
        long checkInsCount = venue.getStats().getCheckinsCount();
        String venueName = venue.getName();
        String vId =  venue.getId();

        assertEquals("There should be 137 checkinsCount ", 137,checkInsCount);
        assertEquals("Incorrect id of the Venue, should be: 4edb84272c5b50ab9fe56ff5 "
                , "4edb84272c5b50ab9fe56ff5",vId);

        assertEquals("Name of this venue should be : Nordowi Môl ", "Nordowi Môl",venueName);
    }


    class MockFourSquareService implements FoursquareService {

        private final BehaviorDelegate<FoursquareService> behaviorDelegate;

        MockFourSquareService(BehaviorDelegate<FoursquareService> behaviorDelegate) {
            this.behaviorDelegate = behaviorDelegate;
        }

        @Override
        public LiveData<FQVenusSearchResponse> getListOfVenues(
                String coordinates
                , int photosOn
                , String clientId
                , String clientSecret
                , String date) {

            FQVenusSearchResponse response = serverResponseFromStream();
            if(response!= null) {
                return behaviorDelegate
                        .returningResponse(response)
                        .getListOfVenues(coordinates, photosOn, clientId, clientSecret, date);
            } else {
                return null;
            }

            // this does not work, problem with LiveData + Retrofit Testing
        }

        @Override
        public Call<FQVenusSearchResponse> getListOfVenues2(
                String coordinates
                , int photosOn
                , String clientId
                , String clientSecret
                , String date) {

            FQVenusSearchResponse response = serverResponseFromStream();
            if(response!=null) {
                return behaviorDelegate
                        .returningResponse(response)
                        .getListOfVenues2(coordinates, photosOn, clientId, clientSecret, date);
            } else {
                return null;
            }
        }

        private FQVenusSearchResponse serverResponseFromStream() {
            Gson gson = new GsonBuilder().create();
            InputStream inputStream = getClass()
                    .getClassLoader()
                    .getResourceAsStream("response/venues.json");
            InputStreamReader reader = new InputStreamReader(inputStream);
            return gson.fromJson(reader, FQVenusSearchResponse.class);
        }
    }
}
