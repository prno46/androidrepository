package com.str.favoritelocationsfoursquare.pojo;

import android.support.annotation.NonNull;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


/**
 * Created by str on 25.09.17.
 *
 */

public class FQVenusSearchResponse {

    @SerializedName("meta")
    @Expose
    private Meta meta;
    @SerializedName("response")
    @Expose
    private Response responser;


    public FQVenusSearchResponse() {
    }


    public FQVenusSearchResponse( Meta meta, Response responser) {
        super();
        this.meta = meta;
        this.responser = responser;
    }
    @NonNull
    public Meta getMeta() {
        return meta;
    }

    public void setMeta(Meta meta) {
        this.meta = meta;
    }

    public FQVenusSearchResponse withMeta(Meta meta) {
        this.meta = meta;
        return this;
    }


    /*example with Optional<T> for the future
     public Optional<Response> getResponse() {
        return Optional.fromNullable(responser);
     }
      */

    public Response getResponse() {

            return responser;
    }

    public void setResponse(Response responser) {
        this.responser = responser;
    }

    public FQVenusSearchResponse withResponse(Response responser) {
        this.responser = responser;
        return this;
    }

}

