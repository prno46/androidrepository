package com.str.favoritelocationsfoursquare.ui;

import android.Manifest;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.str.favoritelocationsfoursquare.ConstantValues;
import com.str.favoritelocationsfoursquare.R;
import com.str.favoritelocationsfoursquare.databinding.VenueFragmentBinding;
import com.str.favoritelocationsfoursquare.repository.Repository;
import com.str.favoritelocationsfoursquare.retrofit.FoursquareService;
import com.str.favoritelocationsfoursquare.retrofit.ServiceGenerator;
import com.str.favoritelocationsfoursquare.viewmodel.LocationViewModel;
import com.str.favoritelocationsfoursquare.viewmodel.ViewModelFactory;

/**
 * Created by str on 04.11.17.
 *
 */

public class VenueFragment extends Fragment {

    private final String TAG = "STR " + getClass().getSimpleName();

    public VenueFragment() {}

    private VenueFragmentBinding fragmentBinding;

    private VenueListAdapter adapter;

    private LocationManager locationManager;

    private LocationViewModel locationViewModel;

    private ViewModelFactory factory;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container
            , @Nullable Bundle savedInstanceState) {

        Log.d(TAG, " in onCreateView ");

        locationManager = (LocationManager) getActivity()
                .getApplicationContext()
                .getSystemService(Context.LOCATION_SERVICE);

        fragmentBinding =
                DataBindingUtil.inflate(inflater, R.layout.venue_fragment, container, false);
        fragmentBinding.setFragment(this);

        fragmentBinding.setGone(4); // TODO: 18.12.17 make as final constant

        adapter = new VenueListAdapter();

        fragmentBinding.venuesRecyclerViewInVenueFragment.setAdapter(adapter);
        fragmentBinding.venuesRecyclerViewInVenueFragment
                .addItemDecoration(new DividerItemDecoration(getContext(), LinearLayoutManager.VERTICAL));
        factory = new ViewModelFactory(getActivity().getApplication(), new Repository()
                ,ServiceGenerator.getServiceInstance().createService(FoursquareService.class));

        return fragmentBinding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        Log.d(TAG, " in onActivityCreated ");

        locationViewModel =
                ViewModelProviders.of(this,factory).get(LocationViewModel.class);
        fragmentBinding.setViewModel(locationViewModel);
        subscribeViewModel(locationViewModel);
    }

    private void subscribeViewModel(LocationViewModel viewModel) {

        viewModel.getObservableListOfVenues().observe(this, venues -> {
            if (venues != null) {
                Log.d(TAG, " in onChanged venues != null");
                adapter.setVenueList(venues);
                adapter.notifyDataSetChanged();
                fragmentBinding.setGone(8);// TODO: 18.12.17 make as final constant
            }
        });
        fragmentBinding.executePendingBindings();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        Log.d(TAG, " in onRequestPermissionsResult code: " + requestCode);

        switch (requestCode) {
            case ConstantValues.REQUEST_PERMISSION_CODE:
                permissionsCheck(grantResults);
                break;
            default:
                Log.d(TAG, " Permissions were not granted by user ");
                showSnackbarMessageToUser(R.string.permission_not_granted);
        }
    }

    private void permissionsCheck(@NonNull int[] grantResults) {
        Log.d(TAG, " in permissionsCheck results length: " + grantResults.length);

        if(grantResults.length>0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            showSnackbarMessageToUser(R.string.permission_granted);
        } else {
            showSnackbarMessageToUser(R.string.permission_not_granted);
        }
    }

    //when Button pressed
    public void requestLocationUpdate() {

        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}
                    , ConstantValues.REQUEST_PERMISSION_CODE );
        } else {
            if (isInternetAvailable(getContext().getApplicationContext())) {
                locationManager.requestSingleUpdate(LocationManager.NETWORK_PROVIDER, locationViewModel, null);
                fragmentBinding.setGone(0);// TODO: 18.12.17 make as final constant
            } else {
                showSnackbarMessageToUser(R.string.inernet_status);
            }
        }
    }

    private void showSnackbarMessageToUser(int resourceId){
        Snackbar snackbar = Snackbar.make(fragmentBinding.getRoot(),resourceId, Snackbar.LENGTH_LONG);
        snackbar.show();
    }


    private boolean isInternetAvailable (Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if ((cm == null)) {
            return false;
        }
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnectedOrConnecting();
    }



}


