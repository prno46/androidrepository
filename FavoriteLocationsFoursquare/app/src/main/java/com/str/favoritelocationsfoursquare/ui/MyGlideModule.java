package com.str.favoritelocationsfoursquare.ui;

import com.bumptech.glide.annotation.GlideModule;
import com.bumptech.glide.module.AppGlideModule;

/**
 * Created by str on 12.11.17.
 *
 */

@GlideModule
public class MyGlideModule extends AppGlideModule {}
