package com.str.favoritelocationsfoursquare.ui;

import android.databinding.BindingAdapter;
import android.databinding.DataBindingUtil;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.str.favoritelocationsfoursquare.R;
import com.str.favoritelocationsfoursquare.databinding.SingleVenueInfoBinding;
import com.str.favoritelocationsfoursquare.pojo.Venue;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by str on 06.11.17.
 *
 */

public class VenueListAdapter extends RecyclerView.Adapter<VenueListAdapter.ViewHolder> {

    private  final String TAG = "STR " + getClass().getSimpleName();

    private  List<Venue> venueList;


    VenueListAdapter(){
        this.venueList = new ArrayList<>();
    }

    void setVenueList (List<Venue> venues) {
        this.venueList = venues;
        Log.d(TAG, " in setVenueList venues.size(): " + venueList.size());
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        Log.d(TAG, " in onCreateViewHolder ");

        SingleVenueInfoBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext())
                , R.layout.single_venue_info, parent, false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Log.d(TAG, " in onBindViewHolder ");
        holder.binding.setSingleVenuePhoto(createVenuePhotoUrl(venueList.get(position)));
        holder.binding.setVenue(venueList.get(position));
        holder.binding.executePendingBindings();
    }

    @Override
    public int getItemCount() {
        Log.d(TAG, " in getItemCount count: "+ venueList.size());
        return venueList == null ? 0:venueList.size();
    }

    private String createVenuePhotoUrl(Venue venue) {
        StringBuilder sb = new StringBuilder();

        String prefix = venue.getFeaturedPhotos()!= null
                ? venue.getFeaturedPhotos().getItems().get(0).getPrefix() : " ";

        String width = venue.getFeaturedPhotos()!=null
                ? String.valueOf(venue.getFeaturedPhotos().getItems().get(0).getWidth()) : " ";

        String height = venue.getFeaturedPhotos()!=null
               ? String.valueOf(venue.getFeaturedPhotos().getItems().get(0).getHeight()) : " ";

        String suffix = venue.getFeaturedPhotos()!=null
                ? venue.getFeaturedPhotos().getItems().get(0).getSuffix() : " ";

        sb.append(prefix).append(width).append("x").append(height).append(suffix);

        Log.d(TAG, " in createVenuePhotoUrl  sb: " + sb.toString());
        return sb.toString();
    }

    @BindingAdapter({"imageUrl","placeholder","error"})
    public static void setImageView(ImageView imageView
                                                        , String url
                                                        , Drawable placeholder
                                                        , Drawable error) {

        Log.d("VenueListAdapter", " in setImageView url: "+url);
        Log.d("VenueListAdapter", " in setImageView imageView: "+imageView.getId());

        GlideApp.with(imageView.getContext())
                .load(url)
                .centerCrop()
                .placeholder(placeholder)
                .error(error)
                .override(100,100)
                .into(imageView);
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        final SingleVenueInfoBinding binding;

        ViewHolder(SingleVenueInfoBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
