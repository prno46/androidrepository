package com.str.favoritelocationsfoursquare.retrofit;

import android.arch.lifecycle.LiveData;

import com.str.favoritelocationsfoursquare.pojo.FQVenusSearchResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by str on 26.10.17.
 *
 */

public interface FoursquareService {

    @GET("explore")
    LiveData<FQVenusSearchResponse> getListOfVenues (@Query("ll") String coordinates
            , @Query("venuePhotos") int photosOn
            , @Query("client_id") String clientId
            , @Query("client_secret") String clientSecret
            , @Query("v") String date);


    //for tests only; version with livedata does not work
    @GET("explore")
    Call<FQVenusSearchResponse> getListOfVenues2 (@Query("ll") String coordinates
            , @Query("venuePhotos") int photosOn
            , @Query("client_id") String clientId
            , @Query("client_secret") String clientSecret
            , @Query("v") String date);
}
