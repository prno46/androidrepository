package com.str.favoritelocationsfoursquare.retrofit;

import android.util.Log;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by str on 24.09.17.
 *
 */

public class ServiceGenerator {

    private static final String TAG = "STR in ServiceGenerator";

    private static final String BASE_URL = "https://api.foursquare.com/v2/venues/";

    private static final Object LOCK = new Object();

    private static ServiceGenerator serviceInstance;

    private static Retrofit.Builder builder = new Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(new LiveDataCallAdapterFactory());

    private static Retrofit retrofit = builder.build();

    public <S> S createService(Class<S> serviceClass) {
        Log.d(TAG, " in createService");
        return retrofit.create(serviceClass);
    }

    public synchronized static ServiceGenerator getServiceInstance () {
        if(serviceInstance == null) {
            synchronized (LOCK) {
                if(serviceInstance == null) {
                    serviceInstance = new ServiceGenerator();
                }
            }
        }
        return serviceInstance;
    }

    private ServiceGenerator() {}

    // TODO: 24.09.17  implement retrofit authentication
}
