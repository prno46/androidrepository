package com.str.favoritelocationsfoursquare;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.str.favoritelocationsfoursquare.ui.VenueFragment;


/**
 *“Celem jest stworzenie aplikacji prezentującej listę najbardziej popularnych lokali znajdujących
 * się w pobliżu użytkownika. Do ich pobrania należy użyć API Foursquare.
 * Każdy element listy powinien wyświetlać miniaturkę lokalu, jego nazwę i liczbę osób,
 * które zrobiły tam check-in. Design aplikacji zależy od Ciebie,
 * nie ma potrzeby jednak dopracowywać każdego jego szczegółu.
 *
 Kilka rzeczy, na które warto zwrócić uwagę:
 - zadbaj o architekturę aplikacji
 - można popisać się wykorzystując biblioteki “third-parties”
 - staraj się pisać czysty, modularny kod
 - do pobrania local wykorzystaj endpoint search or explore z API Foursquare’a
 - upewnij się, że aplikacja będzie działać offline (bez crasha i wycieków pamięci)
 - jeżeli wystarczy Ci czasu, testy albo ciekawe rozwiązania UX są mile widziane”
 *
 *
 */
public class MainActivity extends AppCompatActivity {

    // TODO: 11.11.17 refactor needed to use dependency injection
    // TODO: 22.11.17 add info about connection failure + stop progress bar
    // TODO: 22.11.17 add data casching using Room + SQL
    // TODO: 22.11.17 add tests

    /*
        foursquare explore url
        https://api.foursquare.com/v2/venues/explore?ll=54.634636,18.342598&client_id=3EPYLEY4ASFV5TGB51UDLT5OEHVL4ZT3D0KHIF0DCP1X5UKX&client_secret=0USUUTGWYCCFJOANZS0M4OEEBSCCY0SX0TUNLEZ33KZZMGYL&v=20170524
     */

    /* private String coord; //= "40.7,-85";
     private final String CLIENT_ID = "3EPYLEY4ASFV5TGB51UDLT5OEHVL4ZT3D0KHIF0DCP1X5UKX";
     private final String CLIENT_SECRET = "0USUUTGWYCCFJOANZS0M4OEEBSCCY0SX0TUNLEZ33KZZMGYL";

    */

    private final String TAG = "STR " + getClass().getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Log.d(TAG, " in onCreate before fragment creation");

        if( savedInstanceState == null) {
            VenueFragment venueFragment = new VenueFragment();
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.venueFragmentContainer, venueFragment)
                    .commit();
        }
    }
}


