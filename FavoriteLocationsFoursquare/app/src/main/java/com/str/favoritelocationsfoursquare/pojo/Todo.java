package com.str.favoritelocationsfoursquare.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by str on 12.10.17.
 *
 */

public class Todo {
    @SerializedName("count")
    @Expose
    private Long count;

    /**
     * No args constructor for use in serialization
     *
     */
    public Todo() {
    }

    /**
     *
     * @param count
     */
    public Todo(Long count) {
        super();
        this.count = count;
    }

    public Long getCount() {
        return count;
    }

    public void setCount(Long count) {
        this.count = count;
    }
}
