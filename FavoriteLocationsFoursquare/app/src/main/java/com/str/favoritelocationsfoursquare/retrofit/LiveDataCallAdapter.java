package com.str.favoritelocationsfoursquare.retrofit;

import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;
import android.util.Log;

import java.lang.reflect.Type;
import java.util.concurrent.atomic.AtomicBoolean;

import retrofit2.Call;
import retrofit2.CallAdapter;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by str on 27.10.17.
 *
 */

public class LiveDataCallAdapter<R> implements CallAdapter<R, LiveData<R>> {

    final String TAG = getClass().getSimpleName();

    private final Type responseType;

    LiveDataCallAdapter(Type responseType) {
        this.responseType = responseType;
        Log.d(TAG, " in Constructor");
    }

    @Override
    public Type responseType() {
        return responseType;
    }

    @Override
    public LiveData<R> adapt(@NonNull final Call<R> call) {
        Log.d(TAG, " in adapt method before return statement");
        return new LiveData<R>() {
            AtomicBoolean started = new AtomicBoolean(false);
            @Override
            protected void onActive() {
                super.onActive();
                Log.d(TAG, " in onActive method");
                if(started.compareAndSet(false, true)) {
                    call.enqueue(new Callback<R>() {
                        @Override
                        public void onResponse(@NonNull Call<R> call, @NonNull Response<R> response) {
                            Log.d(TAG, " in onResponse method postValue: code: "+response.code());

                            postValue(response.body());
                        }

                        @Override
                        public void onFailure(@NonNull  Call<R> call, @NonNull Throwable t) {
                            Log.d(TAG, " in onFailure methode");
                            postValue(null);
                        }
                    });
                }
            }
        };
    }
}
