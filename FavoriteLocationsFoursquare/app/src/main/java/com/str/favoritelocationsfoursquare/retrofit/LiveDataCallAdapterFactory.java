package com.str.favoritelocationsfoursquare.retrofit;

import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import java.lang.annotation.Annotation;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

import retrofit2.CallAdapter;
import retrofit2.Retrofit;

/**
 * Created by str on 27.10.17.
 *
 */

public class LiveDataCallAdapterFactory extends CallAdapter.Factory {

    private final String TAG = getClass().getSimpleName();

    @Override
    public CallAdapter<?, ?> get(@NonNull Type returnType, @NonNull Annotation[] annotations, @NonNull Retrofit retrofit) {

        if(getRawType(returnType) != LiveData.class) {
            return  null;
        }

        if(!(returnType instanceof ParameterizedType)) {
            throw new IllegalArgumentException("resource must be parameterized");
        }

        Type bodyType = getParameterUpperBound(0, (ParameterizedType) returnType);
//        Log.d(TAG, " bodyType: "+bodyType.toString());
        return new LiveDataCallAdapter<>(bodyType); // FQVenusSearchResponse
    }
}
