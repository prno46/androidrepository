package com.str.favoritelocationsfoursquare.viewmodel;

import android.app.Application;
import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;
import android.support.annotation.NonNull;

import com.str.favoritelocationsfoursquare.repository.Repository;
import com.str.favoritelocationsfoursquare.retrofit.FoursquareService;

/**
 * Created by str on 17.11.17.
 *
 */

public class ViewModelFactory implements ViewModelProvider.Factory {

    private Repository repository;
    private Application application;
    private FoursquareService service;

    public ViewModelFactory(Application application, Repository repository, FoursquareService service) {
        this.repository = repository;
        this.application = application;
        this.service = service;
    }

    @NonNull
    @Override
    @SuppressWarnings("unchecked")
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {

        if(modelClass.isAssignableFrom(LocationViewModel.class)) {
           return  (T) new LocationViewModel(application, repository, service);
        }

        throw new IllegalArgumentException("Unknown viewModel class");
    }
}
