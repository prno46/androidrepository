package com.str.favoritelocationsfoursquare.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by str on 12.10.17.
 *
 */

public class Photos {
    @SerializedName("count")
    @Expose
    private Long count;
    @SerializedName("groups")
    @Expose
    private List<Group_> groups = new ArrayList<Group_>();


    public Photos() {
    }


    public Photos(Long count, List<Group_> groups) {
        super();
        this.count = count;
        this.groups = groups;
    }

    public Long getCount() {
        return count;
    }

    public void setCount(Long count) {
        this.count = count;
    }

    public List<Group_> getGroups() {
        return groups;
    }

    public void setGroups(List<Group_> groups) {
        this.groups = groups;
    }

}
