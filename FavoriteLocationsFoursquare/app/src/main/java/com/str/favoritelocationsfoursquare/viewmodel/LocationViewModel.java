package com.str.favoritelocationsfoursquare.viewmodel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Transformations;
import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;

import com.str.favoritelocationsfoursquare.pojo.Item;
import com.str.favoritelocationsfoursquare.pojo.Response;
import com.str.favoritelocationsfoursquare.pojo.Venue;
import com.str.favoritelocationsfoursquare.repository.Repository;
import com.str.favoritelocationsfoursquare.retrofit.FoursquareService;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by str on 31.10.17.
 * used for test different posibilities
 *
 */

public class LocationViewModel extends AndroidViewModel implements LocationListener {

    private final String TAG = "STR " + getClass().getSimpleName();

    private final FoursquareService service;
    private MutableLiveData<String> locationCoordinates = new MutableLiveData<>();
    private final Repository repository;
    private LiveData<List<Venue>> observableVenues;
    private MutableLiveData<List<Venue>> observableListOfVenues = new MutableLiveData<>();

    LocationViewModel(@NonNull Application application, Repository repository, FoursquareService service) {
        super(application);
        this.repository= repository;
        this.service = service;
        Log.d(TAG, " in Constructor");
    }

    @Override
    public void onLocationChanged(Location location) {
        double lat = location.getLatitude();
        double lng = location.getLongitude();
        locationCoordinates.setValue(String.valueOf(lat) + "," + String.valueOf(lng));

        Log.d(TAG, " in onLocationChanged coords: " + locationCoordinates.getValue());

        getApiResponse(locationCoordinates.getValue());

        getObservableVenues().observeForever(venues -> {
            if (venues != null) {
                Log.d(TAG, " in onChange venues[2] name: " + venues.get(2).getName());
                observableListOfVenues.setValue(venues);
            } else {
                observableListOfVenues.postValue(Collections.emptyList());
            }
        });
    }

    private void getApiResponse(String userLocation) {
        Log.d(TAG, " in getApiResponse userLocation: " + userLocation);

        repository.connectToFoursquareApi(userLocation, service);

        LiveData<Response> observableApiResponse = Transformations
                .switchMap(Repository.isConnectionSuccessfull()
                , input -> repository.getApiResponse());

        LiveData<List<Item>> observableItem = Transformations
                .map(observableApiResponse, response -> response.getGroups().get(0).getItems());

        observableVenues = Transformations.map(observableItem, items -> {
            List<Venue> venues = new ArrayList<>();
            for (Item i: items) {
                venues.add(i.getVenue());
            }
            Log.d(TAG, " in getApiResponse venues: " + venues.size());

            return venues;
        });
    }

    private LiveData<List<Venue>> getObservableVenues() {
        return observableVenues;
    }

    public LiveData<List<Venue>> getObservableListOfVenues() {
        return observableListOfVenues;
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }


    @Override
    public void onProviderDisabled(String provider) {

    }

}
