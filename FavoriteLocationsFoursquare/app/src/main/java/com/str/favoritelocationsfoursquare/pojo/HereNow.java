package com.str.favoritelocationsfoursquare.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by str on 25.09.17.
 *
 */

public class HereNow {

    @SerializedName("count")
    @Expose
    private Long count;
    @SerializedName("summary")
    @Expose
    private String summary;
    @SerializedName("groups")
    @Expose
    private List<Object> groups = new ArrayList<Object>();


    public HereNow() {}


    public HereNow(Long count, String summary, List<Object> groups) {
        super();
        this.count = count;
        this.summary = summary;
        this.groups = groups;
    }

    public Long getCount() {
        return count;
    }

    public void setCount(Long count) {
        this.count = count;
    }

    public HereNow withCount(Long count) {
        this.count = count;
        return this;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public HereNow withSummary(String summary) {
        this.summary = summary;
        return this;
    }

    public List<Object> getGroups() {
        return groups;
    }

    public void setGroups(List<Object> groups) {
        this.groups = groups;
    }

    public HereNow withGroups(List<Object> groups) {
        this.groups = groups;
        return this;
    }

}

