package com.str.favoritelocationsfoursquare.repository;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.util.Log;

import com.str.favoritelocationsfoursquare.ConstantValues;
import com.str.favoritelocationsfoursquare.pojo.FQVenusSearchResponse;
import com.str.favoritelocationsfoursquare.pojo.Response;
import com.str.favoritelocationsfoursquare.retrofit.FoursquareService;

/**
 * Created by str on 17.10.17.
 * used for downloading data from internet
 * and from data base
 */

public class Repository {

    // TODO: 29.10.17 create service for database connection
    // TODO: 21.12.17 refactoring of this class is needed, the list of venues needs to be created here

    private final String TAG = "STR " + getClass().getSimpleName();

    private static final MutableLiveData<Boolean> isConnectionSuccessfull = new MutableLiveData<>();

    private final MutableLiveData<Response> apiResponse = new MutableLiveData<>();


    public Repository() {
        Log.d(TAG, " in Constructor");
    }

    public void connectToFoursquareApi(String userLocation, FoursquareService service) {

        Log.d(TAG, " in connectToFoursquareApi userLocation: "+userLocation);

        fetchDataFromFoursquareApi(userLocation, service).observeForever(fqVenusSearchResponse -> {
            if(fqVenusSearchResponse != null) {
                apiResponse.setValue(fqVenusSearchResponse.getResponse());
                isConnectionSuccessfull.setValue(true);
            } else {
                Log.d(TAG, " in onChange connection failure ");
                isConnectionSuccessfull.setValue(false);
            }
        });
    }

    private LiveData<FQVenusSearchResponse> fetchDataFromFoursquareApi(String userLocation
                                                                , FoursquareService service) {
        return service.getListOfVenues(
                userLocation,
                ConstantValues.VENUE_PHOTOS,
                ConstantValues.CLIENT_ID,
                ConstantValues.CLIENT_SECRET,
                ConstantValues.V_DATE
        );
    }

    public static LiveData<Boolean> isConnectionSuccessfull() {
        return isConnectionSuccessfull;
    }

    public LiveData<Response> getApiResponse() {
        return apiResponse;
    }

}


