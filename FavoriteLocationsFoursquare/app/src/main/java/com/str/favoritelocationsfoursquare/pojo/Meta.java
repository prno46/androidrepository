package com.str.favoritelocationsfoursquare.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by str on 25.09.17.
 *
 */

public class Meta {

    @SerializedName("code")
    @Expose
    private Long code;
    @SerializedName("requestId")
    @Expose
    private String requestId;


    public Meta() {}


    public Meta(Long code, String requestId) {
        super();
        this.code = code;
        this.requestId = requestId;
    }

    public Long getCode() {
        return code;
    }

    public void setCode(Long code) {
        this.code = code;
    }

    public Meta withCode(Long code) {
        this.code = code;
        return this;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public Meta withRequestId(String requestId) {
        this.requestId = requestId;
        return this;
    }
}
