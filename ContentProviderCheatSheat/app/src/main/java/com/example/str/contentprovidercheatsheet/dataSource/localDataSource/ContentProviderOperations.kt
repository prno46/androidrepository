package com.example.str.contentprovidercheatsheet.dataSource.localDataSource

import android.content.ContentProviderOperation
import android.content.ContentResolver
import android.content.ContentValues
import com.example.str.contentprovidercheatsheet.dataSource.localDataSource.sqlDb.Constants.ALBUMS_CONTENT_URI
import com.example.str.contentprovidercheatsheet.dataSource.localDataSource.sqlDb.Constants.ARTIST_CONTENT_URI
import com.example.str.contentprovidercheatsheet.dataSource.localDataSource.sqlDb.Constants.AlbumTable.ALBUMS_COLUMN_ARTIST_ID
import com.example.str.contentprovidercheatsheet.dataSource.localDataSource.sqlDb.Constants.AlbumTable.ALBUMS_COLUMN_NAME
import com.example.str.contentprovidercheatsheet.dataSource.localDataSource.sqlDb.Constants.ArtistTable.ARTIST_COLUMN_NAME
import com.example.str.contentprovidercheatsheet.dataSource.localDataSource.sqlDb.Constants.CONTENT_AUTHORITY
import com.example.str.contentprovidercheatsheet.dataSource.localDataSource.sqlDb.Constants.SONGS_CONTENT_URI
import com.example.str.contentprovidercheatsheet.dataSource.localDataSource.sqlDb.Constants.SongsTable.SONGS_COLUMN_ALBUM_ID
import com.example.str.contentprovidercheatsheet.dataSource.localDataSource.sqlDb.Constants.SongsTable.SONGS_COLUMN_TITLE
import com.example.str.contentprovidercheatsheet.dataSource.localDataSource.sqlDb.Constants.SongsTable.SONGS_COLUMN_TRACK_NO

object ContentProviderOperations {

    /**
     * the ContentProviderOperation.class helps to create a transactional way to input data into
     * multiple tables.
     * WithValueBackReference method takes a column in to which we need to put a value from previous
     * operation and an index of previous operation. The count of index values starts with 0.
     * WithYieldAllowed - gives a sign that the previous operation has finished and ContentProvider
     * can start another one.
     */

    private val operations: ArrayList<ContentProviderOperation> = arrayListOf()

    fun insertBatchOperation(songTitle: String, songTrack: Int, albumName:String, artistName: String, resolver: ContentResolver): Unit {

        val artistOperation: ContentProviderOperation = ContentProviderOperation
                .newInsert(ARTIST_CONTENT_URI)
                .withValue(ARTIST_COLUMN_NAME, artistName)
                .withYieldAllowed(true)
                .build()

        val albumOperation: ContentProviderOperation = ContentProviderOperation
                .newInsert(ALBUMS_CONTENT_URI)
                .withValueBackReference(ALBUMS_COLUMN_ARTIST_ID, 0)// 0 is the index of previous operation
                .withValue(ALBUMS_COLUMN_NAME, albumName)
                .withYieldAllowed(true)
                .build()

        val songOperation: ContentProviderOperation = ContentProviderOperation
                .newInsert(SONGS_CONTENT_URI)
                .withValueBackReference(SONGS_COLUMN_ALBUM_ID, 1)// 1 is the index of previous operation
                .withValue(SONGS_COLUMN_TITLE, songTitle)
                .withValue(SONGS_COLUMN_TRACK_NO, songTrack)
                .withYieldAllowed(true)
                .build()

        operations.apply {
            add(0,artistOperation)
            add(1,albumOperation)
            add(2,songOperation)
        }

        resolver.applyBatch(CONTENT_AUTHORITY, operations)
    }

    /**Below is a previous way of inserting a data to multiple table a using content provider*/

    private fun insertArtist(artistName: String, resolver: ContentResolver): Int {
        val values = ContentValues().apply { put(ARTIST_COLUMN_NAME, artistName) }
        return resolver.insert(ARTIST_CONTENT_URI, values).lastPathSegment.toInt() // this obtains the newRowNumber from returned Uri
    }

    private fun insertAlbum(albumName: String, resolver: ContentResolver, artistId: Int): Int {
        val values = ContentValues().apply {
            put(ALBUMS_COLUMN_NAME, albumName)
            put(ALBUMS_COLUMN_ARTIST_ID, artistId)
        }
        return resolver.insert(ALBUMS_CONTENT_URI, values).lastPathSegment.toInt()
    }

    fun insertSong(songTitle: String, songTrack: Int, albumName:String, artistName: String,resolver: ContentResolver): Int {
        val artistId = insertArtist(artistName, resolver)
        val albumId = insertAlbum(albumName, resolver, artistId)
        val values = ContentValues().apply {
            put(SONGS_COLUMN_TITLE, songTitle)
            put(SONGS_COLUMN_TRACK_NO, songTrack)
            put(SONGS_COLUMN_ALBUM_ID, albumId)
        }
        return resolver.insert(SONGS_CONTENT_URI, values).lastPathSegment.toInt()
    }
}