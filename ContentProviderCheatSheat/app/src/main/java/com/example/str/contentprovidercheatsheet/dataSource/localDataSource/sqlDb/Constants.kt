package com.example.str.contentprovidercheatsheet.dataSource.localDataSource.sqlDb

import android.content.ContentUris
import android.net.Uri
import android.net.Uri.withAppendedPath
import android.provider.BaseColumns
import com.example.str.contentprovidercheatsheet.dataSource.localDataSource.sqlDb.Constants.AlbumTable.ALBUMS_COLUMN_ARTIST_ID
import com.example.str.contentprovidercheatsheet.dataSource.localDataSource.sqlDb.Constants.AlbumTable.ALBUMS_COLUMN_NAME
import com.example.str.contentprovidercheatsheet.dataSource.localDataSource.sqlDb.Constants.AlbumTable.ALBUMS_TABLE
import com.example.str.contentprovidercheatsheet.dataSource.localDataSource.sqlDb.Constants.ArtistTable.ARTIST_COLUMN_NAME
import com.example.str.contentprovidercheatsheet.dataSource.localDataSource.sqlDb.Constants.ArtistTable.ARTIST_TABLE
import com.example.str.contentprovidercheatsheet.dataSource.localDataSource.sqlDb.Constants.SongsTable.SONGS_TABLE

object Constants {

    const val DATABASE_NAME:String = "songs.db"
    const val DB_VERSION: Int = 1
    const val NO_TABLE: String = "no_table"

    /**
     * Creation of URI for content providers:
     * the URI consists of the following parts: 'scheme://authority/path/id'
     *
     *  - scheme    :   is always this -> 'content://'
     *  - authority :   usually  this is a reversed domain address end it ends with '.provider'
     *  - path      :   this is a path to the data -> a table name ... look at ALL_SONGS_CONTENT_URI below
     *  - id        :   uniquely identifies the data set to search
     */


    /**content provider constants*/
    private const val SCHEME: String ="content://"
    const val CONTENT_AUTHORITY: String = "com.example.str.contentprovidercheatsheet.provider"
    private val CONTENT_AUTHORITY_URI: Uri = Uri.parse("$SCHEME$CONTENT_AUTHORITY")

    /** content provider getType method return type - MIME*/
    const val CONTENT_TYPE_ALL_SONGS: String ="vnd.android.cursor.dir/vnd.$CONTENT_AUTHORITY.$SONGS_TABLE"
    const val CONTENT_TYPE_SONG: String ="vnd.android.cursor.dir/vnd.$CONTENT_AUTHORITY.$SONGS_TABLE"
    const val CONTENT_TYPE_ITEM_SONG: String = "vnd.android.cursor.item/vnd.$CONTENT_AUTHORITY.$SONGS_TABLE"
    const val CONTENT_TYPE_ALL_ALBUMS: String = "vnd.android.cursor.dir/vnd.$CONTENT_AUTHORITY.$ALBUMS_TABLE"
    const val CONTENT_TYPE_ITEM_ALBUM: String = "vnd.android.cursor.item/vnd.$CONTENT_AUTHORITY.$ALBUMS_TABLE"
    const val CONTENT_TYPE_ALL_ARTISTS: String = "vnd.android.cursor.dir/vnd.$CONTENT_AUTHORITY.$ARTIST_TABLE"
    const val CONTENT_TYPE_ITEM_ARTIST: String = "vnd.android.cursor.item/vnd.$CONTENT_AUTHORITY.$ARTIST_TABLE"

    /**content uri to access a song table using content provider*/
    val ALL_SONGS_CONTENT_URI: Uri = withAppendedPath(CONTENT_AUTHORITY_URI, NO_TABLE)
    val SONGS_CONTENT_URI: Uri = Uri.withAppendedPath(CONTENT_AUTHORITY_URI, SONGS_TABLE)
    val ALBUMS_CONTENT_URI: Uri = Uri.withAppendedPath(CONTENT_AUTHORITY_URI, ALBUMS_TABLE)
    val ARTIST_CONTENT_URI: Uri = Uri.withAppendedPath(CONTENT_AUTHORITY_URI, ARTIST_TABLE)

    object ArtistTable : BaseColumns {
        const val ARTIST_TABLE: String = "artists"
        const val ARTIST_COLUMN_NAME: String = "artistName"

        val CREATE_ARTIST_TABLE: String = """CREATE TABLE $ARTIST_TABLE
            |(${BaseColumns._ID} INTEGER PRIMARY KEY, $ARTIST_COLUMN_NAME TEXT NOT NULL)""".trimMargin()
    }

    object AlbumTable: BaseColumns {
        const val ALBUMS_TABLE: String = "albums"
        const val ALBUMS_COLUMN_NAME: String ="albumName"
        const val ALBUMS_COLUMN_ARTIST_ID: String ="artistId"
        private const val FOREIGN_KEY_ARTIST: String = "fk_artist"

        val CREATE_ALBUM_TABLE: String = """CREATE TABLE $ALBUMS_TABLE
            |(${BaseColumns._ID} INTEGER PRIMARY KEY, $ALBUMS_COLUMN_NAME TEXT NOT NULL, $ALBUMS_COLUMN_ARTIST_ID INTEGER,
            |CONSTRAINT $FOREIGN_KEY_ARTIST FOREIGN KEY ($ALBUMS_COLUMN_ARTIST_ID)
            |REFERENCES $ARTIST_TABLE(${BaseColumns._ID})
            |ON DELETE CASCADE)""".trimMargin()
    }

    object SongsTable: BaseColumns {
        const val SONGS_TABLE:String = "songs"
        const val SONGS_COLUMN_TITLE: String = "songTitle"
        const val SONGS_COLUMN_TRACK_NO: String = "trackNumber"
        const val SONGS_COLUMN_ALBUM_ID: String = "albumId"
        private const val FOREIGN_KEY_ALBUMS: String = "fk_album"


        val CREATE_SONGS_TABLE: String = """CREATE TABLE $SONGS_TABLE
            |(${BaseColumns._ID} INTEGER PRIMARY KEY, $SONGS_COLUMN_TITLE TEXT NOT NULL,
            |$SONGS_COLUMN_TRACK_NO INTEGER, $SONGS_COLUMN_ALBUM_ID INTEGER,
            |CONSTRAINT $FOREIGN_KEY_ALBUMS FOREIGN KEY ($SONGS_COLUMN_ALBUM_ID)
            |REFERENCES $ALBUMS_TABLE (${BaseColumns._ID})
            |ON DELETE CASCADE)""".trimMargin()

        val QUERY_ALL: String = """SELECT $SONGS_COLUMN_TITLE, $ALBUMS_TABLE.$ALBUMS_COLUMN_NAME,
            |$ARTIST_TABLE.$ARTIST_COLUMN_NAME, $SONGS_COLUMN_TRACK_NO FROM $SONGS_TABLE
            |INNER JOIN $ALBUMS_TABLE ON $SONGS_TABLE.$SONGS_COLUMN_ALBUM_ID = $ALBUMS_TABLE.${BaseColumns._ID}
            |INNER JOIN $ARTIST_TABLE ON $ALBUMS_TABLE.$ALBUMS_COLUMN_ARTIST_ID = $ARTIST_TABLE.${BaseColumns._ID}
            |ORDER BY $ARTIST_TABLE.$ARTIST_COLUMN_NAME ASC""".trimMargin()
    }

    object UriBuilder {
        fun buildUri(contentUri: Uri, id: Long): Uri {
            return ContentUris.withAppendedId(contentUri, id)
        }
    }
}