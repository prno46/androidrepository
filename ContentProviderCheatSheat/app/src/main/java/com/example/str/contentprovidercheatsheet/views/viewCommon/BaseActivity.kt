package com.example.str.contentprovidercheatsheet.views.viewCommon

import android.support.v7.app.AppCompatActivity
import com.example.str.contentprovidercheatsheet.globalSingletons.application.CompositionRoot
import com.example.str.contentprovidercheatsheet.globalSingletons.application.CproviderApp

open class BaseActivity: AppCompatActivity() {


    val cRoot: CompositionRoot by lazy { (application as CproviderApp).compositionRoot }

}