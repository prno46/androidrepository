package com.example.str.contentprovidercheatsheet.globalSingletons.executors

import android.os.Handler
import android.os.Looper
import java.util.concurrent.Executor
import java.util.concurrent.Executors

class ThreadDispatcher constructor(
        val diskIO: Executor = DiskIO(),
        val netWork:Executor = Executors.newFixedThreadPool(THREAD_POOL),
        val mainThread: MainAppThread = MainAppThread){

    companion object MainAppThread: Executor {
        private const val THREAD_POOL:Int = 3
        private val mainThread: Handler = Handler(Looper.getMainLooper())
        override fun execute(command: Runnable?) {
            mainThread.post(command)
        }
    }
}