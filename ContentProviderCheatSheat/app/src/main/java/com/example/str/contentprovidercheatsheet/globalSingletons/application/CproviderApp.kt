package com.example.str.contentprovidercheatsheet.globalSingletons.application

import android.app.Application
import android.util.Log

class CproviderApp: Application() {

    private val TAG: String = "STR ${javaClass.simpleName}"

    val compositionRoot: CompositionRoot by lazy { CompositionRoot(this)}

    override fun onCreate() {
        super.onCreate()
        Log.d(TAG, "in onCreate")
    }
}