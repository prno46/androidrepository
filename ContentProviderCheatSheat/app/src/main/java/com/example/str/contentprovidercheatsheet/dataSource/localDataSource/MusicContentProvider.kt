package com.example.str.contentprovidercheatsheet.dataSource.localDataSource

import android.content.*
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteQueryBuilder
import android.net.Uri
import android.provider.BaseColumns
import android.util.Log
import com.example.str.contentprovidercheatsheet.dataSource.localDataSource.sqlDb.Constants
import com.example.str.contentprovidercheatsheet.dataSource.localDataSource.sqlDb.Constants.ALBUMS_CONTENT_URI
import com.example.str.contentprovidercheatsheet.dataSource.localDataSource.sqlDb.Constants.ARTIST_CONTENT_URI
import com.example.str.contentprovidercheatsheet.dataSource.localDataSource.sqlDb.Constants.AlbumTable.ALBUMS_COLUMN_ARTIST_ID
import com.example.str.contentprovidercheatsheet.dataSource.localDataSource.sqlDb.Constants.AlbumTable.ALBUMS_COLUMN_NAME
import com.example.str.contentprovidercheatsheet.dataSource.localDataSource.sqlDb.Constants.AlbumTable.ALBUMS_TABLE
import com.example.str.contentprovidercheatsheet.dataSource.localDataSource.sqlDb.Constants.ArtistTable.ARTIST_COLUMN_NAME
import com.example.str.contentprovidercheatsheet.dataSource.localDataSource.sqlDb.Constants.ArtistTable.ARTIST_TABLE
import com.example.str.contentprovidercheatsheet.dataSource.localDataSource.sqlDb.Constants.CONTENT_AUTHORITY
import com.example.str.contentprovidercheatsheet.dataSource.localDataSource.sqlDb.Constants.CONTENT_TYPE_ALL_ALBUMS
import com.example.str.contentprovidercheatsheet.dataSource.localDataSource.sqlDb.Constants.CONTENT_TYPE_ALL_ARTISTS
import com.example.str.contentprovidercheatsheet.dataSource.localDataSource.sqlDb.Constants.CONTENT_TYPE_ALL_SONGS
import com.example.str.contentprovidercheatsheet.dataSource.localDataSource.sqlDb.Constants.CONTENT_TYPE_ITEM_ALBUM
import com.example.str.contentprovidercheatsheet.dataSource.localDataSource.sqlDb.Constants.CONTENT_TYPE_ITEM_ARTIST
import com.example.str.contentprovidercheatsheet.dataSource.localDataSource.sqlDb.Constants.CONTENT_TYPE_ITEM_SONG
import com.example.str.contentprovidercheatsheet.dataSource.localDataSource.sqlDb.Constants.CONTENT_TYPE_SONG
import com.example.str.contentprovidercheatsheet.dataSource.localDataSource.sqlDb.Constants.NO_TABLE
import com.example.str.contentprovidercheatsheet.dataSource.localDataSource.sqlDb.Constants.SONGS_CONTENT_URI
import com.example.str.contentprovidercheatsheet.dataSource.localDataSource.sqlDb.Constants.SongsTable.SONGS_COLUMN_ALBUM_ID
import com.example.str.contentprovidercheatsheet.dataSource.localDataSource.sqlDb.Constants.SongsTable.SONGS_COLUMN_TITLE
import com.example.str.contentprovidercheatsheet.dataSource.localDataSource.sqlDb.Constants.SongsTable.SONGS_TABLE
import com.example.str.contentprovidercheatsheet.globalSingletons.application.CproviderApp
import java.util.*
import kotlin.properties.Delegates

class MusicContentProvider : ContentProvider() {

    @Suppress("PrivatePropertyName")
    private val TAG: String = "STR ${javaClass.simpleName}"

    private val db: SQLiteDatabase by lazy { (context.applicationContext as CproviderApp).compositionRoot.sqLiteDb.writableDatabase }
    private val queryBuilder: SQLiteQueryBuilder by lazy { SQLiteQueryBuilder() }
    private val uriMatcher: UriMatcher by lazy { buildUriMatcher() }

    override fun onCreate(): Boolean {
        return true
    }

    override fun query(uri: Uri?, projection: Array<out String>?, selection: String?, selectionArgs: Array<out String>?, sortOrder: String?): Cursor {
        val matcher: Int = uriMatcher.match(uri)
        Log.d(TAG, "matcher = $matcher")
        lateinit var cursor: Cursor
        when (matcher) {
            ALL_SONGS -> {
                queryBuilder.tables = """$SONGS_TABLE INNER JOIN
                    | $ALBUMS_TABLE ON ($SONGS_TABLE.$SONGS_COLUMN_ALBUM_ID = $ALBUMS_TABLE.${BaseColumns._ID})
                    | INNER JOIN $ARTIST_TABLE ON ($ALBUMS_TABLE.$ALBUMS_COLUMN_ARTIST_ID = $ARTIST_TABLE.${BaseColumns._ID})""".trimMargin()

                cursor = queryBuilder.query(db, projection, selection, selectionArgs, null, null, sortOrder)
                cursor.setNotificationUri(context.contentResolver, uri)
            }
            SONG -> {}
            SONG_ID -> {}
            ALBUM -> {}
            ALBUM_ID -> {}
            ARTIST -> {}
            ARTISTS_ID -> {}
        }
        return cursor
    }

    // currently this method is used to insert data to database
    override fun applyBatch(operations: ArrayList<ContentProviderOperation>?): Array<ContentProviderResult> {
        Log.d(TAG, "in applyBatch ")
        db.beginTransaction()
        try {
            val result: Array<ContentProviderResult> = super.applyBatch(operations)
            db.setTransactionSuccessful()
            return  result

        }finally {
            db.endTransaction()
        }
    }

    private fun isInDataBase(table: String, column:String, searchValue: String): Long {
        Log.d(TAG, "in isInDataBase -> table = $table; column = $column; searchValue = $searchValue")
        val columns: Array<String> = arrayOf(BaseColumns._ID, column)
        val selection: String = "$column LIKE ?"
        val selectionArgs: Array<String> = arrayOf(searchValue)
        val cursor: Cursor = db.query(table, columns, selection ,selectionArgs, null, null, null)
        return cursor.use { cr ->
            if(cr.moveToNext() && cr.getString(1) == searchValue) {
                cr.getLong(cr.getColumnIndex("_id"))
            } else {
                -1L
            }
        }
    }

    private fun getRowId(table: String,column: String, values: ContentValues): Long {
        val newRowId = isInDataBase(table, column, values.getAsString(column))
        return when (newRowId) {
            -1L -> db.insert(table, null, values)
            else -> newRowId
        }
    }

    /**
     * this method returns a URI with id of the new row in a specified table
     */
    override fun insert(uri: Uri?, values: ContentValues): Uri {
        val matcher: Int = uriMatcher.match(uri)
        Log.d(TAG, "in insert method -> matcher value = $matcher")

        var newRowId: Long by Delegates.notNull()
        return when(matcher) {
            ARTIST -> {
                Log.d(TAG, "in insert Artist -> values = $values")
                newRowId = getRowId(ARTIST_TABLE,ARTIST_COLUMN_NAME, values)
                Constants.UriBuilder.buildUri(ARTIST_CONTENT_URI, newRowId)
            }
            ALBUM -> {
                Log.d(TAG, "in insert Album -> values = $values")
                newRowId = getRowId(ALBUMS_TABLE, ALBUMS_COLUMN_NAME, values)
                Constants.UriBuilder.buildUri(ALBUMS_CONTENT_URI, newRowId)
            }
            SONG -> {
                Log.d(TAG, "in insert Song -> values = $values")
                newRowId = getRowId(SONGS_TABLE, SONGS_COLUMN_TITLE, values)
                Constants.UriBuilder.buildUri(SONGS_CONTENT_URI, newRowId)
            }
            else -> {throw IllegalArgumentException("Unknown uri: $uri")}
        }
    }

    override fun update(uri: Uri?, values: ContentValues?, selection: String?, selectionArgs: Array<out String>?): Int {
        // implementation will be similar to the one form DBhelperKotlin
        return -1
    }

    override fun delete(uri: Uri?, selection: String?, selectionArgs: Array<out String>?): Int {
        // implementation will be similar to the one form DBhelperKotlin
        return -1
    }

    /**this method returns a MIME type for each of the specified URI */
    override fun getType(uri: Uri?): String {
        Log.d(TAG, "in getType")
        val matcher: Int = uriMatcher.match(uri)
        return when (matcher) {
            ALL_SONGS -> {CONTENT_TYPE_ALL_SONGS}
            SONG -> { CONTENT_TYPE_SONG }
            SONG_ID -> { CONTENT_TYPE_ITEM_SONG }
            ALBUM -> { CONTENT_TYPE_ALL_ALBUMS }
            ALBUM_ID -> { CONTENT_TYPE_ITEM_ALBUM }
            ARTIST -> {CONTENT_TYPE_ALL_ARTISTS }
            ARTISTS_ID -> { CONTENT_TYPE_ITEM_ARTIST }
            else -> { throw IllegalArgumentException("Unknown Uri: $uri") }
        }
    }

    companion object {
        /**UriMatcher is a utility class that helps matching the uri's in content providers
         * URI MATCHER CODES that will be returned when match is positive
         *
         * How it works:
         * pass a SONGS_CONTENT_URI to a method ex: insert.
         * the matcher will evaluate given URI if it match one of the given in addURI method
         * if there is a match, the correct code will be returned for ex. SONG
         */
        const val ALL_SONGS: Int = 500
        const val SONG: Int = 100
        const val SONG_ID: Int = 101
        const val ARTIST: Int = 200
        const val ARTISTS_ID: Int = 201
        const val ALBUM: Int = 300
        const val ALBUM_ID: Int = 301

        fun buildUriMatcher(): UriMatcher {
            val matcher: UriMatcher = UriMatcher(UriMatcher.NO_MATCH)
            return matcher.apply {
                addURI(CONTENT_AUTHORITY, NO_TABLE, ALL_SONGS)
                addURI(CONTENT_AUTHORITY, SONGS_TABLE, SONG)
                addURI(CONTENT_AUTHORITY, "$SONGS_TABLE/#", SONG_ID)
                addURI(CONTENT_AUTHORITY, ALBUMS_TABLE, ALBUM)
                addURI(CONTENT_AUTHORITY, "$ALBUMS_TABLE/#", ALBUM_ID)
                addURI(CONTENT_AUTHORITY, ARTIST_TABLE, ARTIST)
                addURI(CONTENT_AUTHORITY, "$ARTIST_TABLE/#", ARTISTS_ID)
            }
        }
    }
}