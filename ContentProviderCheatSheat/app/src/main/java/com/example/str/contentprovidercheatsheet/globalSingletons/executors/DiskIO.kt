package com.example.str.contentprovidercheatsheet.globalSingletons.executors

import java.util.concurrent.Executor
import java.util.concurrent.Executors

class DiskIO constructor(
        private val diskIO:Executor = Executors.newSingleThreadExecutor()): Executor {

    override fun execute(command: Runnable?) {
        diskIO.execute(command)
    }
}