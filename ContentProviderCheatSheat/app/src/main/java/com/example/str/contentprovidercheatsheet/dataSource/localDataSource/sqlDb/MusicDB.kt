package com.example.str.contentprovidercheatsheet.dataSource.localDataSource.sqlDb

import android.content.Context
import android.database.SQLException
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import com.example.str.contentprovidercheatsheet.dataSource.localDataSource.sqlDb.Constants.AlbumTable.CREATE_ALBUM_TABLE
import com.example.str.contentprovidercheatsheet.dataSource.localDataSource.sqlDb.Constants.ArtistTable.CREATE_ARTIST_TABLE
import com.example.str.contentprovidercheatsheet.dataSource.localDataSource.sqlDb.Constants.DATABASE_NAME
import com.example.str.contentprovidercheatsheet.dataSource.localDataSource.sqlDb.Constants.DB_VERSION
import com.example.str.contentprovidercheatsheet.dataSource.localDataSource.sqlDb.Constants.SongsTable.CREATE_SONGS_TABLE

class MusicDB constructor(context: Context):
        SQLiteOpenHelper(context.applicationContext, DATABASE_NAME, null, DB_VERSION ) {

    override fun onOpen(db: SQLiteDatabase?) {
        super.onOpen(db)
        db?.setForeignKeyConstraintsEnabled(true)
    }

    override fun onCreate(db: SQLiteDatabase?) {
        try {
            db?.execSQL(CREATE_ARTIST_TABLE)
            db?.execSQL(CREATE_ALBUM_TABLE)
            db?.execSQL(CREATE_SONGS_TABLE)
        } catch (e: SQLException) {
            println("Creation of tables was not successful: ${e.message}")
        }
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        TODO("not implemented -> not needed for this one")
    }
}