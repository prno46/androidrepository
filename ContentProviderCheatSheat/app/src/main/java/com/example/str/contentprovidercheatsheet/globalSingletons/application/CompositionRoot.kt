package com.example.str.contentprovidercheatsheet.globalSingletons.application

import android.app.Application
import android.util.Log
import com.example.str.contentprovidercheatsheet.dataSource.localDataSource.sqlDb.MusicDB

class CompositionRoot constructor(private val app: Application){

    @Suppress("PrivatePropertyName")
    private val TAG: String = "STR ${javaClass.simpleName}"

    val sqLiteDb: MusicDB by lazy { MusicDB(app)}
    //private val dispatcher: ThreadDispatcher by lazy { ThreadDispatcher() }

    init {
        Log.d(TAG, "in Init block")
    }

}