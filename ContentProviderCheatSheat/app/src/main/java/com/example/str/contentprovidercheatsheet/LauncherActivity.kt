package com.example.str.contentprovidercheatsheet

import android.content.ContentResolver
import android.content.OperationApplicationException
import android.database.Cursor
import android.database.SQLException
import android.os.Bundle
import android.os.RemoteException
import android.util.Log
import com.example.str.contentprovidercheatsheet.dataSource.localDataSource.ContentProviderOperations
import com.example.str.contentprovidercheatsheet.dataSource.localDataSource.sqlDb.Constants.ALL_SONGS_CONTENT_URI
import com.example.str.contentprovidercheatsheet.dataSource.localDataSource.sqlDb.Constants.AlbumTable.ALBUMS_COLUMN_NAME
import com.example.str.contentprovidercheatsheet.dataSource.localDataSource.sqlDb.Constants.ArtistTable.ARTIST_COLUMN_NAME
import com.example.str.contentprovidercheatsheet.dataSource.localDataSource.sqlDb.Constants.SongsTable.SONGS_COLUMN_TITLE
import com.example.str.contentprovidercheatsheet.dataSource.localDataSource.sqlDb.Constants.SongsTable.SONGS_COLUMN_TRACK_NO
import com.example.str.contentprovidercheatsheet.views.viewCommon.BaseActivity

class LauncherActivity : BaseActivity() {

    @Suppress("PrivatePropertyName")
    private val TAG:String = "STR ${javaClass.simpleName}-> "

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_launcher)
        /**
         * uri          : uri of the specified table
         * projection   : list of columns tu return
         * selection    : filter declaring which rows to return - WHERE clause we can include '?'
         *                 example: '$columnName = ?' or '$columnName LIKE ?'
         * selectionArgs: values for '?' from selection
         */
        val resolver: ContentResolver = contentResolver
        val projection: Array<String> = arrayOf(SONGS_COLUMN_TITLE, SONGS_COLUMN_TRACK_NO, ALBUMS_COLUMN_NAME, ARTIST_COLUMN_NAME)
        try {

            //ContentProviderOperations.insertSong("Master of Puppets", 2, "Master of Puppets", "Metallica")
            ContentProviderOperations.insertBatchOperation("Welcome Home", 4, "Master of Puppets", "Metallica", resolver)
            queryAllSongs(resolver, projection)

        } catch (e: SQLException) {
            println("error occurred:  ${e.message}")
        } catch (e2: RemoteException) {
            println("error occurred:  ${e2.message}")
        } catch (e3: OperationApplicationException){
            println("error occurred:  ${e3.message}")
        }
    }

    private fun queryAllSongs(resolver: ContentResolver, projection: Array<String>): Unit {
        val cursor: Cursor = resolver.query(ALL_SONGS_CONTENT_URI, projection, null, null, null)
        cursor.use {
            while (cursor.moveToNext()) {
                Log.d(TAG, cursor.getString(0))
                Log.d(TAG, cursor.getString(1))
                Log.d(TAG, cursor.getString(2))
                Log.d(TAG, cursor.getString(3))
            }
        }
    }
}
