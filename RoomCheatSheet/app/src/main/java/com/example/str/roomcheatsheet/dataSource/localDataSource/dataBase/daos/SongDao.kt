package com.example.str.roomcheatsheet.dataSource.localDataSource.dataBase.daos

import android.arch.persistence.room.*
import com.example.str.roomcheatsheet.dataSource.localDataSource.dataBase.entities.Songs
import com.example.str.roomcheatsheet.models.SingleSong

@Dao
abstract class SongDao {

    @Suppress("PrivatePropertyName")
    private val TAG: String = "STR ${javaClass.simpleName}"

    @Insert(onConflict = OnConflictStrategy.FAIL)
    abstract fun insertIntoSongs(song: Songs): Long

    @Delete
    abstract fun deleteSong(song: Songs): Int

    @Update(onConflict = OnConflictStrategy.REPLACE)
    abstract fun updateSong(song: Songs): Int

    @Query("SELECT title, trackNo, albumName, artistName, albumsId, artistId, song_id FROM Songs "+
            "INNER JOIN Albums ON Songs.albumsId = Albums.album_id "+
            "INNER JOIN Artists ON Albums.artistId = Artists.artist_id")
    abstract fun getAllSongs(): List<SingleSong>

    @Query("SELECT * FROM Songs WHERE title LIKE :songTitle")
    abstract fun getSingleSongByTitle(songTitle: String): Songs?

    @Query("SELECT song_id FROM Songs WHERE title LIKE :songTitle")
    abstract fun getSongId(songTitle: String): Long?

}