package com.example.str.roomcheatsheet.dataSource.localDataSource.dataBase

import com.example.str.roomcheatsheet.dataSource.localDataSource.dataBase.daos.AlbumDao
import com.example.str.roomcheatsheet.dataSource.localDataSource.dataBase.daos.ArtistDao
import com.example.str.roomcheatsheet.dataSource.localDataSource.dataBase.daos.SongDao

interface DataBase {

    fun songsDao(): SongDao
    fun artistDao(): ArtistDao
    fun albumDao(): AlbumDao
}