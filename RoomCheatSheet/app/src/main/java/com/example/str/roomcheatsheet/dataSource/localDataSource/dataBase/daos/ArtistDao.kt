package com.example.str.roomcheatsheet.dataSource.localDataSource.dataBase.daos

import android.arch.persistence.room.*
import com.example.str.roomcheatsheet.dataSource.localDataSource.dataBase.entities.Artists

@Dao
interface ArtistDao {

    @Insert(onConflict = OnConflictStrategy.FAIL)
    fun insertIntoArtists(artist: Artists): Long

    @Delete
    fun deleteArtist(artist: Artists): Int

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun updateArtist(artist: Artists): Int

    @Query("SELECT artist_id FROM Artists WHERE artistName LIKE :name")
    fun getArtistId(name: String): Long?

    @Query("SELECT * FROM artists WHERE artistName LIKE :name")
    fun getArtistByName(name: String): Artists?
}