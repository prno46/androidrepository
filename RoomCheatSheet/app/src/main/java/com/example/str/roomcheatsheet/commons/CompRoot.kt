package com.example.str.roomcheatsheet.commons

import android.app.Application
import android.util.Log
import com.example.str.roomcheatsheet.dataSource.Repository
import com.example.str.roomcheatsheet.dataSource.RepositoryImpl
import com.example.str.roomcheatsheet.dataSource.localDataSource.dataBase.DataBase
import com.example.str.roomcheatsheet.dataSource.localDataSource.dataBase.SongsDatabase
import com.example.str.roomcheatsheet.executors.ThreadDispatcher

class CompRoot(private val application: Application) {

    @Suppress("PrivatePropertyName")
    private val TAG: String = "STR ${javaClass.simpleName}"

    private val database: DataBase by lazy { SongsDatabase.createDataBase(application, false)}
    private val threadDispatcher: ThreadDispatcher by lazy { ThreadDispatcher() }
    val repository: Repository by lazy { RepositoryImpl(database, threadDispatcher) }

    init {
        Log.d(TAG, "in Init Block: the instance od DB is ${database.hashCode()}")
    }
}