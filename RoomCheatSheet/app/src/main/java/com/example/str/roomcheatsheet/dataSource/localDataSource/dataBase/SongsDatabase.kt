package com.example.str.roomcheatsheet.dataSource.localDataSource.dataBase

import android.app.Application
import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import com.example.str.roomcheatsheet.dataSource.localDataSource.dataBase.entities.Albums
import com.example.str.roomcheatsheet.dataSource.localDataSource.dataBase.entities.Artists
import com.example.str.roomcheatsheet.dataSource.localDataSource.dataBase.entities.Songs

@Database(entities = [Artists::class, Albums::class, Songs::class], version = 1)
abstract class SongsDatabase: RoomDatabase(), DataBase {

    companion object {
        private const val DATA_BASE_NAME: String = "songs.db"

        fun createDataBase(application: Application, memoryOnly: Boolean): SongsDatabase {
            val builder: RoomDatabase.Builder<SongsDatabase> = when(memoryOnly) {
                true -> Room.inMemoryDatabaseBuilder(application.applicationContext, SongsDatabase::class.java)
                else -> Room.databaseBuilder(application.applicationContext, SongsDatabase::class.java, DATA_BASE_NAME)
            }
            return builder.build()
        }
    }
}


/**
 * it is a good practice to export a schema of the database to a file
 * you do tis is build.gradle file adding option below to defaultConfig under
 * android section
 *
 * javaCompileOptions {
        annotationProcessorOptions {
        arguments = ["room.schemaLocation": "$projectDir/schemas".toString()]
        }
    }

 our schema will be saved as a json file in schemas folder
 *
 */