package com.example.str.roomcheatsheet.commons

import android.support.v7.app.AppCompatActivity
import com.example.str.roomcheatsheet.dataSource.Repository
import com.example.str.roomcheatsheet.globalSingletons.MyApplication

open class BaseActivity : AppCompatActivity() {

  /*  val roomDB: SongsDatabase by lazy { (application as MyApplication).root.database as SongsDatabase}
    val backGround: ThreadDispatcher by lazy { (application as MyApplication).root.threadDispatcher }*/

    val repository: Repository by lazy { (application as MyApplication).root.repository}
}
