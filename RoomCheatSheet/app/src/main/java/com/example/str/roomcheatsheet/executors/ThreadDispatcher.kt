package com.example.str.roomcheatsheet.executors

import android.os.Handler
import android.os.Looper
import java.util.concurrent.Executor


class ThreadDispatcher (val diskIO: IOExecutor = IOExecutor(),
                        val mainThread: MainThreadExecutor = MainThreadExecutor) {

    companion object MainThreadExecutor: Executor {
        private val handler: Handler = Handler(Looper.getMainLooper())

        @JvmStatic
        override fun execute(command: Runnable?) {
            handler.post(command)
        }
    }
}