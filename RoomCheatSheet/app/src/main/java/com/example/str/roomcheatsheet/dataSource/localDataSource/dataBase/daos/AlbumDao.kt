package com.example.str.roomcheatsheet.dataSource.localDataSource.dataBase.daos

import android.arch.persistence.room.*
import com.example.str.roomcheatsheet.dataSource.localDataSource.dataBase.entities.Albums

@Dao
interface AlbumDao {

    @Insert(onConflict = OnConflictStrategy.FAIL)
    fun insertIntoAlbums(album: Albums): Long

    @Delete
    fun deleteAlbum(album: Albums): Int

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun updateAlbum(album: Albums): Int

    @Query("SELECT album_id FROM Albums WHERE albumName LIKE :name")
    fun getAlbumId(name: String): Long?

    @Query("SELECT * FROM Albums WHERE albumName LIKE :name")
    fun getAlbumByName(name: String): Albums?
}