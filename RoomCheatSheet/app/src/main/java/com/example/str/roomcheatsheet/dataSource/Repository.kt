package com.example.str.roomcheatsheet.dataSource

import android.support.annotation.NonNull
import com.example.str.roomcheatsheet.dataSource.localDataSource.dataBase.entities.Albums
import com.example.str.roomcheatsheet.dataSource.localDataSource.dataBase.entities.Artists
import com.example.str.roomcheatsheet.dataSource.localDataSource.dataBase.entities.Songs
import com.example.str.roomcheatsheet.models.SingleSong
import com.example.str.roomcheatsheet.temp.LoadListener

interface Repository {

    fun insertNewSong(@NonNull title: String, @NonNull trackNo: Int
                      , @NonNull album: String, @NonNull artist: String)
    fun deleteSong(@NonNull song: Songs)
    fun deleteAlbum(@NonNull album: Albums)
    fun deleteArtist(@NonNull artist: Artists)
    fun updateASong(@NonNull song: Songs)
    fun startLoading()
    fun getAllSongs(): List<SingleSong>
    fun setOnLoadFinishListener(listener: LoadListener)
}