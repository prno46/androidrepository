package com.example.str.roomcheatsheet

import android.os.Bundle
import android.util.Log
import com.example.str.roomcheatsheet.commons.BaseActivity
import com.example.str.roomcheatsheet.dataSource.localDataSource.dataBase.entities.Songs
import com.example.str.roomcheatsheet.models.SingleSong
import com.example.str.roomcheatsheet.temp.LoadListener

class MainActivity : BaseActivity(), LoadListener {

    @Suppress("PrivatePropertyName")
    private val TAG: String = "STR ${javaClass.simpleName}"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        Log.d(TAG, "in onCrete")
        //"Sad but true", 1, "Black Album", "Metallica"
    }

    override fun onStart() {
        super.onStart()
        repository.setOnLoadFinishListener(this)
    }

    override fun onResume() {
        super.onResume()
        repository.startLoading()
    }

    override fun onLoadFinished(songs: List<SingleSong>) {
        Log.d(TAG, "the current list of songs is: -> $songs")
        Log.d(TAG, "the current size of songs is: -> ${songs.size}")

        val title = "Nothing Else Matters"
        val track = 5
        val albumId = songs[songs.size-1].albumsId ?: -1
        val songToDel: Songs = Songs(title, track, albumId)
        songToDel.song_id = songs[songs.size-1].song_id

        //repository.deleteSong(songToDel)
        //repository.updateASong(songToDel)
    }

    /**
     * important information -> when we use Auto-generated primary key
     * we need alo set it before we delete or update an object from data base
     * all the fields from Entity class that are not ignored must be provided
     */

}
