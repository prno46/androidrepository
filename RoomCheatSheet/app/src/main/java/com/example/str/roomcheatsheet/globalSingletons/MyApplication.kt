package com.example.str.roomcheatsheet.globalSingletons

import android.app.Application
import android.util.Log
import com.example.str.roomcheatsheet.commons.CompRoot

class MyApplication: Application() {

    @Suppress("PrivatePropertyName")
    private val TAG: String = "STR ${javaClass.simpleName}"

    val root: CompRoot by lazy { CompRoot(this) }

    override fun onCreate() {
        super.onCreate()
        Log.d(TAG, "in onCreate")
    }
}