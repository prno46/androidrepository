package com.example.str.roomcheatsheet.models

import android.arch.persistence.room.Ignore
import java.util.*

class SingleSong(
        val title: String?,
        val trackNo: Int?,
        val albumName: String?,
        val artistName: String?,
        val albumsId: Long?,
        val artistId: Long?,
        val song_id: Long?
        ) {

    @Ignore
    private val uuid: UUID = UUID.fromString("38400000-8cf0-11bd-b23e-10b96e4ef00d")

    override fun equals(other: Any?): Boolean {
        if(this === other) return true
        if(null === other || javaClass != this.javaClass) return false
        val songs: SingleSong = other as SingleSong
        return Objects.equals(uuid, songs.uuid)
    }

    override fun hashCode(): Int {
        return uuid.hashCode()
    }

    override fun toString(): String {
        val sb = StringBuilder()
        return sb.apply {
            append("Title: $title\n")
            append("TrackNo: $trackNo\n")
            append("Album Name: $albumName\n")
            append("Artist Name: $artistName\n")
            append("AlbumId: $albumsId\n")
            append("ArtistId: $artistId\n")
            append("SongId: $song_id\n")
        }.toString()
    }

    fun uidToInt(): Int {
      return uuid.hashCode()
    }
}

/**
 * this class helps to query the database and returns
 * only the data that we are interested in
 *
 * than in Dao -> query method we use JOIN to get desired data
 * when we use object for exm. Artist + @Embedded annotation we will
 * get all the fields from entity and when we do not use them we get a warning
 * so we need to choose only a data that we need
 *
 * if we match the names of the fields with names of the columns in the tables
 * we do not need to use @ColumnInfo annotation
 *
 */