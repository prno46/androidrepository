package com.example.str.roomcheatsheet.temp

import com.example.str.roomcheatsheet.models.SingleSong

interface LoadListener {

    fun onLoadFinished(songs: List<SingleSong>)
}


