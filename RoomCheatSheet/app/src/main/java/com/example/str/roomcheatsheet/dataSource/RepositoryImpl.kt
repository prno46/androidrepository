package com.example.str.roomcheatsheet.dataSource

import android.support.annotation.NonNull
import android.util.Log
import com.example.str.roomcheatsheet.dataSource.localDataSource.dataBase.DataBase
import com.example.str.roomcheatsheet.dataSource.localDataSource.dataBase.SongsDatabase
import com.example.str.roomcheatsheet.dataSource.localDataSource.dataBase.entities.Albums
import com.example.str.roomcheatsheet.dataSource.localDataSource.dataBase.entities.Artists
import com.example.str.roomcheatsheet.dataSource.localDataSource.dataBase.entities.Songs
import com.example.str.roomcheatsheet.executors.ThreadDispatcher
import com.example.str.roomcheatsheet.models.SingleSong
import com.example.str.roomcheatsheet.temp.LoadListener

class RepositoryImpl (private val room: DataBase,
                      private val doInBackGround: ThreadDispatcher): Repository {

    @Suppress("PrivatePropertyName")
    private val TAG: String = "STR ${javaClass.simpleName}"

    private val memoryCache: MutableMap<Int, SingleSong> by lazy { mutableMapOf<Int, SingleSong>() }
    private var isCacheDirty: Boolean = false
    private var isLoadedFirstTime: Boolean = true
    private lateinit var loadListener: LoadListener

    override fun startLoading() {
        if(isLoadedFirstTime || isCacheDirty) {
            loadAllSongs()
        }
        isLoadedFirstTime = false
    }

    private fun loadAllSongs() {
        val runnable = Runnable {
            val songs: List<SingleSong> = room.songsDao().getAllSongs()
            doInBackGround.mainThread.execute {
                refreshCache(songs)
                loadListener.onLoadFinished(memoryCache.values.toList())
            }
        }
        doInBackGround.diskIO.execute(runnable)
    }

    private fun refreshCache(songs: List<SingleSong>) {
        if(memoryCache.isNotEmpty()) {
            memoryCache.clear()
        }
        for ((key, s: SingleSong) in songs.withIndex()) {
            memoryCache[key] = s
        }
        isCacheDirty = false
    }

   override fun getAllSongs(): List<SingleSong> {
        return memoryCache.values.toList()
    }
    override fun insertNewSong(@NonNull title: String, @NonNull trackNo: Int,
                               @NonNull album: String, @NonNull artist: String) {
        val runnable = Runnable {
            val artistId: Long?
            val albumId: Long?
            var queryRes: Long?
            (room as SongsDatabase).beginTransaction()
            try {
                queryRes = room.artistDao().getArtistId(artist)
                artistId = when(queryRes) {
                    null -> room.artistDao().insertIntoArtists(Artists(artist))
                    else -> {
                        Log.d(TAG, "artist already in database id = $queryRes")
                        queryRes}
                }
                queryRes = room.albumDao().getAlbumId(album)
                albumId = when(queryRes) {
                    null -> room.albumDao().insertIntoAlbums(Albums(album, artistId))
                    else -> {
                        Log.d(TAG, "album already in database id = $queryRes")
                        queryRes}
                }
                room.songsDao().insertIntoSongs(Songs(title, trackNo, albumId))
                room.setTransactionSuccessful()
            }catch (ex: android.database.SQLException) {
                Log.d(TAG, "SQL Error -> ${ex.message}")
            } finally {
                room.endTransaction()
            }
        }
        doInBackGround.diskIO.execute(runnable)
        isCacheDirty = true
    }

    override fun deleteSong(@NonNull song: Songs) {
        val runnable = Runnable {
            val row =  room.songsDao().deleteSong(song)
            Log.d(TAG, "rows deleted :-> $row")
        }
        doInBackGround.diskIO.execute(runnable)
        isCacheDirty = true
    }

    override fun deleteAlbum(@NonNull album: Albums) {
        TODO("to implement")
    }

    override fun deleteArtist(@NonNull artist: Artists) {
        TODO("to implement")
    }

     override fun setOnLoadFinishListener(listener: LoadListener) {
        this.loadListener = listener
    }

    override fun updateASong(@NonNull song: Songs) {
        val runnable = Runnable {
            val row = room.songsDao().updateSong(song)
            Log.d(TAG, "rows updated -> $row")
        }
        doInBackGround.diskIO.execute(runnable)
        isCacheDirty = true
    }
}