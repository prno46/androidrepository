package com.example.str.roomcheatsheet.dataSource.localDataSource.dataBase.entities

import android.arch.persistence.room.Entity
import android.arch.persistence.room.ForeignKey
import android.arch.persistence.room.Index
import android.arch.persistence.room.PrimaryKey
import android.support.annotation.NonNull

@Entity(indices = [(Index(value = ["albumsId"], name = "idy"))],
        foreignKeys = [
                (ForeignKey(
                        entity = Albums::class,
                        parentColumns = ["album_id"],
                        childColumns = ["albumsId"],
                        onDelete = ForeignKey.CASCADE,
                        onUpdate = ForeignKey.NO_ACTION,
                        deferred = true
                ))
        ]
)
class Songs(@NonNull val title: String, @NonNull val trackNo: Int, @NonNull val albumsId: Long?) {

    @Suppress("PropertyName")
    @PrimaryKey(autoGenerate = true) var song_id: Long? = null

}
