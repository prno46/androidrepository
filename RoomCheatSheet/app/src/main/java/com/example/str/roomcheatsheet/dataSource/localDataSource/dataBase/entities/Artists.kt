package com.example.str.roomcheatsheet.dataSource.localDataSource.dataBase.entities

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import android.support.annotation.NonNull

@Entity
class Artists(@NonNull val artistName: String) {

        @Suppress("PropertyName")
        @PrimaryKey(autoGenerate = true) var artist_id: Long? = null


    /** Below example of overriding a hashCode and equals methods with use of UUID */
  /*private val uuid: UUID = UUID.fromString("38400000-8cf0-11bd-b23e-10b96e4ef00d")

    override fun equals(other: Any?): Boolean {
        if(this === other) return true
        if (other === null || javaClass != other.javaClass) return false
        val artists: Artists = other as Artists
        return Objects.equals(uuid, artists.uuid)
    }

    override fun hashCode(): Int {
        return uuid.hashCode()
    }*/
}