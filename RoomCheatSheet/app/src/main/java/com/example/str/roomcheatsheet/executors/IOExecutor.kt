package com.example.str.roomcheatsheet.executors

import java.util.concurrent.Executor
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors

class IOExecutor(): Executor {

    private val iOExecutor: ExecutorService by lazy { Executors.newSingleThreadExecutor() }

    override fun execute(runnable: Runnable?) {
        iOExecutor.execute(runnable)
    }
}