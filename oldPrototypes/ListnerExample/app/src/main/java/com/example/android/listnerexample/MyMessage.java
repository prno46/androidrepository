package com.example.android.listnerexample;

/**
 * Created by str on 15.06.17.
 */

public class MyMessage {


    private final String message ="This is a message from: ";

    private MyListner listner;

    public MyMessage() {}



    public void setListner (MyListner listner){
        this.listner = listner;
    }


    public void displayMessage () {
        listner.showMessage(message);
    }


}
