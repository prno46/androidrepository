package com.example.android.listnerexample;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements MyListner{


    TextView displayMessage;

    MyMessage message;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        displayMessage = (TextView) findViewById(R.id.textView_showMessage);

        message = new MyMessage();
        message.setListner(this);
        message.displayMessage();


    }


    @Override
    public void showMessage(String msg) {
        displayMessage.setText(msg);
    }
}
