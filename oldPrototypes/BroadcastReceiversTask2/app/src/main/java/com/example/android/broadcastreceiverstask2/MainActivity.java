package com.example.android.broadcastreceiverstask2;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.button)
    Button button;

    BroadcastReceiver networkReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);



       networkReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                if (isConnected(context)) {
                    Toast.makeText(context, "Network is on line", Toast.LENGTH_SHORT).show();
                    button.setEnabled(true);

                } else {
                    Toast.makeText(context, "Network is disabled", Toast.LENGTH_SHORT).show();
                    button.setEnabled(false);
                }

            }
       };
    }


    @Override
    protected void onStart() {
        super.onStart();
        IntentFilter intentFilter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        registerReceiver(networkReceiver,intentFilter);
    }


    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(networkReceiver);
    }


    @OnClick(R.id.button)
    public void onButtonClick() {
        Toast.makeText(this, "Network connection is available", Toast.LENGTH_SHORT).show();
    }




    private boolean isConnected (Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

        return networkInfo != null && networkInfo.isConnected()
                && (networkInfo.getType() == ConnectivityManager.TYPE_WIFI);
    }
}
