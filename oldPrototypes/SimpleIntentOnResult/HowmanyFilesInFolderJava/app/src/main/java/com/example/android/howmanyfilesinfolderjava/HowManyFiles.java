package com.example.android.howmanyfilesinfolderjava;

import java.io.BufferedReader;
import java.io.File;

/**
 * Created by str on 04.06.17.
 */

public class HowManyFiles {
    public static void main(String[] args) {

        String path = "/home/str/Downloads/Documents/";

        File folder = new File(path);

        File [] list = folder.listFiles();

        for (File f : list) {
            System.out.println(f.getName());
        }
        
    }
}
