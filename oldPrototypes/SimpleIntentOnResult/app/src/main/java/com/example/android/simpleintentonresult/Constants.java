package com.example.android.simpleintentonresult;

/**
 * Created by str on 10.06.17.
 */

public class Constants {

    private Constants() {}

    static final String KEY = "key";
    static final int MY_REQUEST_CODE = 46;
    static final double DEFAULT_VALUE = 1.98;

    static final String MY_TEXT = "text";
    static final String MY_NUMBER = "number";
    static final String MY_OBJECT = "object";

}
