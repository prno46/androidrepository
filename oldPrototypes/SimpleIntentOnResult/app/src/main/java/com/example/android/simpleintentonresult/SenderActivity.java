package com.example.android.simpleintentonresult;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SenderActivity extends AppCompatActivity {

    @BindView(R.id.textView_receivedMessage)
    TextView displayMessage;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sender);
        ButterKnife.bind(this);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if(requestCode == Constants.MY_REQUEST_CODE && resultCode == RESULT_OK) {
            double receivedData = data.getDoubleExtra(Constants.KEY,Constants.DEFAULT_VALUE);
            displayMessage.setText(String.valueOf(receivedData));
        }
        super.onActivityResult(requestCode, resultCode, data);
    }


    @OnClick(R.id.button_sendText)
    public void sendText() {
        sendIntent(Constants.MY_TEXT);
    }

    @OnClick(R.id.button_sendNumber)
    public void sendNumber() {
        sendIntent(Constants.MY_NUMBER);
    }

    @OnClick(R.id.button_sendObject)
    public void sendObject() {
        sendIntent(Constants.MY_OBJECT);
    }

    private void sendIntent (String action){
        Intent intent = new Intent(this, ReceiverActivity.class);
        intent.putExtra(Constants.KEY,action);
        startActivityForResult(intent,Constants.MY_REQUEST_CODE);
    }
}
