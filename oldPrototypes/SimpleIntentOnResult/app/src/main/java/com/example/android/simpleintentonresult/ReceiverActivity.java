package com.example.android.simpleintentonresult;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import butterknife.ButterKnife;

public class ReceiverActivity extends AppCompatActivity {



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_receiverctivity);
        ButterKnife.bind(this);

        if (getIntent().hasExtra(Constants.KEY)) {

            String match = getIntent().getStringExtra(Constants.KEY);
            double number = sendItBack(match);
            resendIntend(number);
            finish();
        }
    }


    private double sendItBack(String message) {
        switch(message) {
            case Constants.MY_TEXT:
                return 5.875;
            case Constants.MY_NUMBER:
                return 6.097;
            case Constants.MY_OBJECT:
                return 34.764;
            default:
                return 2.654;
        }
    }

    private void resendIntend(double result) {
        Intent intent = new Intent();
        intent.putExtra(Constants.KEY,result);
        setResult(RESULT_OK,intent);
    }

}
