// Generated code from Butter Knife. Do not modify!
package com.example.android.simpleintentonresult;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import java.lang.IllegalStateException;
import java.lang.Override;

public class SenderActivity_ViewBinding implements Unbinder {
  private SenderActivity target;

  private View view2131427423;

  private View view2131427424;

  private View view2131427425;

  @UiThread
  public SenderActivity_ViewBinding(SenderActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public SenderActivity_ViewBinding(final SenderActivity target, View source) {
    this.target = target;

    View view;
    target.displayMessage = Utils.findRequiredViewAsType(source, R.id.textView_receivedMessage, "field 'displayMessage'", TextView.class);
    view = Utils.findRequiredView(source, R.id.button_sendText, "method 'sendText'");
    view2131427423 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.sendText();
      }
    });
    view = Utils.findRequiredView(source, R.id.button_sendNumber, "method 'sendNumber'");
    view2131427424 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.sendNumber();
      }
    });
    view = Utils.findRequiredView(source, R.id.button_sendObject, "method 'sendObject'");
    view2131427425 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.sendObject();
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    SenderActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.displayMessage = null;

    view2131427423.setOnClickListener(null);
    view2131427423 = null;
    view2131427424.setOnClickListener(null);
    view2131427424 = null;
    view2131427425.setOnClickListener(null);
    view2131427425 = null;
  }
}
