package com.example.android.databaseconnection.dataprovider;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.NonNull;

import com.example.android.databaseconnection.Model.Entity;

/**
 * Created by str on 24.05.17.
 */

public class DatabaseHelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "tasks.db";

    private static final String TEXT_TYPE = "TEXT";
    private static final String INTEGER = "INTEGER";
    private static final String COMA = ",";

    private static final String SQL_CREATE_TABLE =
            "CREATE TABLE " + DatabaseContract.EntryFeed.TABLE_NAME
            + "( " + DatabaseContract.EntryFeed._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + DatabaseContract.EntryFeed.COLUMN_NAME_TITLE + TEXT_TYPE + COMA
            + DatabaseContract.EntryFeed.COLUMN_NAME_DATE + INTEGER + COMA
            + DatabaseContract.EntryFeed.COLUMN_NAME_DESCRIPTION + TEXT_TYPE + ")" ;


    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_TABLE);

    }



    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS" + DatabaseContract.EntryFeed.TABLE_NAME);
        onCreate(db);
    }

    /**
     * Inserting a row to a database
     * @param entity
     * @return long
     */
    public long saveTask(@NonNull Entity entity) {
        SQLiteDatabase db = getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(DatabaseContract.EntryFeed.COLUMN_NAME_TITLE, entity.getTitle());
        values.put(DatabaseContract.EntryFeed.COLUMN_NAME_DATE, entity.getDate());
        values.put(DatabaseContract.EntryFeed.COLUMN_NAME_DESCRIPTION, entity.getDescription());

        return db.insert(DatabaseContract.EntryFeed.TABLE_NAME,null,values);
    }
}
