package com.example.android.databaseconnection.ModelView;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.example.android.databaseconnection.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by str on 24.05.17.
 */

public class DatabaseViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.title_textView_id)
    TextView holderTitle;
    @BindView(R.id.date_textView_id)
    TextView holderDate;
    @BindView(R.id.description_textView_id)
    TextView holderDescription;


    public DatabaseViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);


    }


    public void setHolderTitle(String title) {
         holderTitle.setText(title);
    }

    public void setHolderDate(String date) {
        holderDate.setText(date);
    }

    public void setHolderDescription(String description) {
        holderDescription.setText(description);
    }


}
