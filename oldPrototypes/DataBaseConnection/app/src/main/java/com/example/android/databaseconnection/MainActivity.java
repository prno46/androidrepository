package com.example.android.databaseconnection;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.example.android.databaseconnection.Model.Entity;
import com.example.android.databaseconnection.ModelView.DatabaseAdapter;
import com.example.android.databaseconnection.ModelView.DatabaseViewHolder;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    private DatabaseAdapter adapter;

    @BindView(R.id.recyclerView_main_id)
    RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        setUpEntity();

        recyclerView.setHasFixedSize(false);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
    }


    @OnClick(R.id.floatingActionButton)
    public void toNextActivity(View v) {
        Intent intent = new Intent(this,SecondActivity.class);
        startActivity(intent);
    }

    private void setUpEntity(){
        adapter = new DatabaseAdapter();
        List<Entity> ent = new ArrayList<>();
        Entity e1 = new Entity();
        e1.setTitle("title");
        e1.setDate(232323);
        e1.setDescription("to jest opis ");
        ent.add(e1);

        adapter.fillEmptyList(ent);
    }
}
