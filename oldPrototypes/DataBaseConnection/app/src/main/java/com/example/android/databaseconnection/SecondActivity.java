package com.example.android.databaseconnection;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class SecondActivity extends AppCompatActivity {



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
        ButterKnife.bind(this);



    }

    @OnClick(R.id.floatingActionButton_second_activity)
    public void saveToDatabase() {
        Toast.makeText(this, "Second activity toast", Toast.LENGTH_SHORT).show();
    }
}
