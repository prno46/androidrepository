package com.example.android.databaseconnection.dataprovider;

import android.provider.BaseColumns;

/**
 * Created by str on 24.05.17.
 */

public final class DatabaseContract {

    private DatabaseContract() {}

    public static class EntryFeed implements BaseColumns {

        public static final String TABLE_NAME = "tasks";
        public static final String COLUMN_NAME_TITLE = "title";
        public static final String COLUMN_NAME_DATE = "date";
        public static final String COLUMN_NAME_DESCRIPTION = "description";

    }
}
