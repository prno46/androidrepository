package com.example.android.databaseconnection.ModelView;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.android.databaseconnection.Model.Entity;
import com.example.android.databaseconnection.R;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by str on 24.05.17.
 */

public class DatabaseAdapter extends RecyclerView.Adapter<DatabaseViewHolder> {

    private List<Entity> entities;

    public DatabaseAdapter() {
        entities = new ArrayList<>();
    }


    @Override
    public DatabaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_item_layout,parent,false);
        return new DatabaseViewHolder(view);
    }

    @Override
    public void onBindViewHolder(DatabaseViewHolder holder, int position) {
        Entity entity = entities.get(position);
        holder.setHolderDate(String.valueOf(entity.getDate()));
        holder.setHolderTitle(entity.getTitle());
        holder.setHolderDescription(entity.getDescription());

    }

    @Override
    public int getItemCount() {
        return entities.size();
    }


    //TODO - create a method to set up the entity list
    public void fillEmptyList(List<Entity> ent) {
        for( Entity e: ent) {
            entities.add(e);
        }
    }

}
