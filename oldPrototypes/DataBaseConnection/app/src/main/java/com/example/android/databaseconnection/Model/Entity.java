package com.example.android.databaseconnection.Model;

/**
 * Created by str on 24.05.17.
 */

public class Entity {

    private String title;
    private String description;
    private int date;

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public int getDate() {
        return date;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setDate(int date) {
        this.date = date;
    }
}
