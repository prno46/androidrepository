// Generated code from Butter Knife. Do not modify!
package com.example.android.databaseconnection.ModelView;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.example.android.databaseconnection.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class DatabaseViewHolder_ViewBinding implements Unbinder {
  private DatabaseViewHolder target;

  @UiThread
  public DatabaseViewHolder_ViewBinding(DatabaseViewHolder target, View source) {
    this.target = target;

    target.holderTitle = Utils.findRequiredViewAsType(source, R.id.title_textView_id, "field 'holderTitle'", TextView.class);
    target.holderDate = Utils.findRequiredViewAsType(source, R.id.date_textView_id, "field 'holderDate'", TextView.class);
    target.holderDescription = Utils.findRequiredViewAsType(source, R.id.description_textView_id, "field 'holderDescription'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    DatabaseViewHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.holderTitle = null;
    target.holderDate = null;
    target.holderDescription = null;
  }
}
