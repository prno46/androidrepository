package com.example.android.rcviewnetwork;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by str on 11.05.17.
 */

public class CarAdapter extends RecyclerView.Adapter <ViewHolder>{

    private List<CarModel> data = new ArrayList<>();

    public CarAdapter(List<CarModel> data) {
        this.data = data;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cars,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        CarModel carModel = data.get(position);
        holder.getCarModel().setText(carModel.getModel());
        holder.getCarType().setText(carModel.getType());
        holder.getCarPower().setText(String.valueOf(carModel.getPower()));

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public void setData(List<CarModel> data) {
        this.data = data;
        notifyDataSetChanged();
    }
}
