package com.example.android.rcviewnetwork;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by str on 11.05.17.
 */

public class DataProvider {

    private  List<CarModel> cars;

    DataProvider() {
        cars = new ArrayList<>();
        cars.add(new CarModel("VW Golf","Sedan",120));
        cars.add(new CarModel("A3","Hatchback",150));
        cars.add(new CarModel("BMW M3","Sedan",320));
        cars.add(new CarModel("Lexus","Sedan",220));
        cars.add(new CarModel("Honda","Combi",220));
        cars.add(new CarModel("JEEP","SUV",320));
        cars.add(new CarModel("Subaru","4X4",220));
        cars.add(new CarModel("Infinity","Cupe",220));
        cars.add(new CarModel("Alfa Romeo","Sedan",220));
        cars.add(new CarModel("VW Tuareg","4X4",220));
        cars.add(new CarModel("Mitsubishi","4X4",260));
        cars.add(new CarModel("Aston Martin","GT",420));
        cars.add(new CarModel("Bentley","Sedan",220));
        cars.add(new CarModel("Nissan GTR","4X4",420));
    }

    public List<CarModel> getCars() {
        return cars;
    }


}
