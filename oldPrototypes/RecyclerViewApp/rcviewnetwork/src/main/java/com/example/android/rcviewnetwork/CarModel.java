package com.example.android.rcviewnetwork;

/**
 * Created by str on 11.05.17.
 */

public class CarModel {

    private final String model;
    private final String type;
    private final int power;

    public CarModel(String model, String type, int power) {
        this.model = model;
        this.type = type;
        this.power = power;
    }

    public String getModel() {
        return model;
    }

    public String getType() {
        return type;
    }

    public int getPower() {
        return power;
    }
}
