package com.example.android.rcviewnetwork;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

/**
 * Created by str on 11.05.17.
 */

public class ViewHolder extends RecyclerView.ViewHolder {


    private TextView carModel;
    private TextView carType;
    private TextView carPower;

    public ViewHolder(View itemView) {
        super(itemView);

        carModel = (TextView) itemView.findViewById(R.id.car_model_textView);
        carType = (TextView) itemView.findViewById(R.id.car_typ_textView);
        carPower = (TextView) itemView.findViewById(R.id.car_power_textView);
    }

    public TextView getCarModel() {
        return carModel;
    }

    public TextView getCarType() {
        return carType;
    }

    public TextView getCarPower() {
        return carPower;
    }
}
