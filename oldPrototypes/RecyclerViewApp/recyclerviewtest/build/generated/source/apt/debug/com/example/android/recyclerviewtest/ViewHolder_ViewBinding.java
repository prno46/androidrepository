// Generated code from Butter Knife. Do not modify!
package com.example.android.recyclerviewtest;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import java.lang.IllegalStateException;
import java.lang.Override;

public class ViewHolder_ViewBinding implements Unbinder {
  private ViewHolder target;

  @UiThread
  public ViewHolder_ViewBinding(ViewHolder target, View source) {
    this.target = target;

    target.text = Utils.findRequiredViewAsType(source, R.id.test_layout_textView, "field 'text'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    ViewHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.text = null;
  }
}
