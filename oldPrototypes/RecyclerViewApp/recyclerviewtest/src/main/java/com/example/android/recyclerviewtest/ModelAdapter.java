package com.example.android.recyclerviewtest;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by str on 14.05.17.
 */

public class ModelAdapter extends RecyclerView.Adapter<ViewHolder> {

    private List<Model> data;

    public ModelAdapter() {
        data = new ArrayList<>();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.test_layout,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Model model = data.get(position);
        holder.setHolderText(model.getTekst());

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public void loadData(List<Model> models) {
        for (Model m : models){
            data.add(m);
        }

    }
}
