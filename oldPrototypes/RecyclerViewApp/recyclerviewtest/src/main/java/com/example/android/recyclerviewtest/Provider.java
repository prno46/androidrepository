package com.example.android.recyclerviewtest;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by str on 14.05.17.
 */

public class Provider {

    private ModelAdapter modelAdapter;

    public Provider() {
        modelAdapter = new ModelAdapter();
    }

    private List<Model> provide(){
        List<Model> models = new ArrayList<>();
        models.add(new Model());

        for(Model m: models){
            m.setModelTekst("to jest tekst z providera");
        }

        return models;
    }


    public ModelAdapter getModelAdapter() {
        modelAdapter.loadData(provide());
        return modelAdapter;
    }
}
