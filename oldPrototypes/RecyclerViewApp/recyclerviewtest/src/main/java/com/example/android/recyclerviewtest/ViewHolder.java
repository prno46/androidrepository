package com.example.android.recyclerviewtest;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;


/**
 * Created by str on 14.05.17.
 */

class ViewHolder extends RecyclerView.ViewHolder {

    private TextView text;


    public ViewHolder(View itemView) {
        super(itemView);

        text = (TextView) itemView.findViewById(R.id.test_layout_textView);

    }

    public TextView getText() {
        return text;
    }

    public void setHolderText(String s){
        text.setText(s);
    }
}
