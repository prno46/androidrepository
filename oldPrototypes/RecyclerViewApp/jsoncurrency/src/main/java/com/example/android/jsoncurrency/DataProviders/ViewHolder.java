package com.example.android.jsoncurrency.DataProviders;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.example.android.jsoncurrency.R;


/**
 * Created by str on 14.05.17.
 */

public class ViewHolder extends RecyclerView.ViewHolder {


    private TextView currencyName;
    private TextView currencyRatio;


    public ViewHolder(View itemView) {
        super(itemView);

        currencyName = (TextView) itemView.findViewById(R.id.currency_name_textView);
        currencyRatio = (TextView) itemView.findViewById(R.id.currency_ratio_textView);
    }


    public void setCurrencyName(String name) {
        currencyName.setText(name);

    }

    public void setCurrencyRatio(String ratio) {
        currencyRatio.setText(ratio);
    }
}
