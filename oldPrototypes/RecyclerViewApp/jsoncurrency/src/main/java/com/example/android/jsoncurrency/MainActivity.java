package com.example.android.jsoncurrency;

import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.example.android.jsoncurrency.DataProviders.CurrencyAdapter;
import com.example.android.jsoncurrency.DataProviders.CurrencyDataProvider;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {


    @BindView(R.id.swipe_refresh_layout_main_activity)
    SwipeRefreshLayout swipeRefreshLayout;

    @BindView(R.id.recycler_view_layout_main_activity)
    RecyclerView recyclerView;

    private CurrencyDataProvider currencyDataProvider;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        setUp();

        currencyDataProvider.execute("A");
        swipeRefreshLayout.setRefreshing(false);
    }


    @Override
    public void onRefresh() {
        //currencyDataProvider.execute("A");
    }



    private void setUp() {
        currencyDataProvider = new CurrencyDataProvider();
        recyclerView.setHasFixedSize(false);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(currencyDataProvider.getCurrencyAdapter());

    }

}
