package com.example.android.jsoncurrency.Model;

import java.util.List;

/**
 * Created by str on 14.05.17.
 */

public class NBPCurrencyTable {

    private String tableSymbol;
    private String tableNumber;
    private String tableEffectiveDate;
    private List<Currency> currencies;

    public NBPCurrencyTable () {

    }

    public String getTableSymbol() {
        return tableSymbol;
    }

    public String getTableNumber() {
        return tableNumber;
    }

    public String getTableEffectiveDate() {
        return tableEffectiveDate;
    }

    public List<Currency> getCurrencies() {
        return currencies;
    }

    public void setTableSymbol(String tableSymbol) {
        this.tableSymbol = tableSymbol;
    }

    public void setTableNumber(String tableNumber) {
        this.tableNumber = tableNumber;
    }

    public void setTableEffectiveDate(String tableEffectiveDate) {
        this.tableEffectiveDate = tableEffectiveDate;
    }

    public void setCurrencies(List<Currency> currencies) {
        this.currencies = currencies;
    }
}
