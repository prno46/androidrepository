package com.example.android.jsoncurrency.DataProviders;

import android.os.AsyncTask;
import android.widget.Toast;

import com.example.android.jsoncurrency.Model.Currency;
import com.example.android.jsoncurrency.Model.NBPCurrencyTable;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by str on 14.05.17.
 */

public class CurrencyDataProvider extends AsyncTask<String, Void, NBPCurrencyTable> {

    private CurrencyAdapter currencyAdapter;
    private JsonParser jsonParser;


    public CurrencyDataProvider() {
        currencyAdapter = new CurrencyAdapter();
        jsonParser = new JsonParser();

    }


    @Override
    protected NBPCurrencyTable doInBackground(String... params) {
        String symbol = params[0];

        return prepareNBPTable();

    }


    @Override
    protected void onPostExecute(NBPCurrencyTable currencyTable) {
        currencyAdapter.loadDataToList(currencyTable.getCurrencies());
    }

    //ToDo - w tej kalsie dpędzie sie akcja sciagania danych z internetu


    private String getCurrenciesRatioFromIntenet(String tableSymbol) throws IOException  {
        String sourceAddress = "http://api.nbp.pl/api/exchangerates/tables/{table}?format=json".replace("{table}", tableSymbol);
        URL url = new URL(sourceAddress);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        InputStream inputStream = new BufferedInputStream(connection.getInputStream());

        StringBuilder sb = new StringBuilder();
        Reader reader = new BufferedReader(new InputStreamReader(inputStream, Charset.forName("UTF-8")));
        int charCode;
        while((charCode = reader.read()) != -1 ) {
            char character = (char) charCode;
            sb.append(character);
        }
        connection.disconnect();

        return sb.toString();
    }


    // to remove, just for tests
    private NBPCurrencyTable prepareNBPTable() {
        Currency currency = new Currency();
        NBPCurrencyTable currencyTable = new NBPCurrencyTable();
        currencyTable.setTableSymbol("A");
        currencyTable.setTableEffectiveDate("12/23/45");
        currencyTable.setTableNumber("25478");

        currency.setCurrencyName("USD");
        currency.setCurrencyRatio("3.76");

        List<Currency> currencies = new ArrayList<>();
        currencies.add(currency);

        currencyTable.setCurrencies(currencies);
        return currencyTable;
    }


    public CurrencyAdapter getCurrencyAdapter() {
        return currencyAdapter;
    }
}
