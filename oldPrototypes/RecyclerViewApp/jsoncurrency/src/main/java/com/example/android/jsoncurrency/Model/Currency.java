package com.example.android.jsoncurrency.Model;

/**
 * Created by str on 14.05.17.
 */

public class Currency {

    private String currencyName;
    private String currencyCode;
    private String currencyRatio;

    public Currency() {}

    public String getCurrencyName() {
        return currencyName;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public String getCurrencyRatio() {
        return currencyRatio;
    }

    public void setCurrencyName(String currencyName) {
        this.currencyName = currencyName;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public void setCurrencyRatio(String currencyRatio) {
        this.currencyRatio = currencyRatio;
    }
}
