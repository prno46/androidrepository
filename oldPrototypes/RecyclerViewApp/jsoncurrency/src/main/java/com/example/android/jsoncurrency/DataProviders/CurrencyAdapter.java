package com.example.android.jsoncurrency.DataProviders;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.android.jsoncurrency.Model.Currency;
import com.example.android.jsoncurrency.Model.NBPCurrencyTable;
import com.example.android.jsoncurrency.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by str on 14.05.17.
 */

public class CurrencyAdapter extends RecyclerView.Adapter <ViewHolder>{

    private List<Currency> data;

    public CurrencyAdapter() {
        data = new ArrayList<>();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.currency_layout,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Currency currency = data.get(position);
        holder.setCurrencyName(currency.getCurrencyName());
        holder.setCurrencyRatio(currency.getCurrencyRatio());

    }

    @Override
    public int getItemCount() {

        return data.size();
    }



    public void loadDataToList(List<Currency> currencies) {
        for (Currency c: currencies){
            data.add(c);
        }
    }
}
