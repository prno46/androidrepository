// Generated code from Butter Knife. Do not modify!
package com.example.str.calculator;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.Button;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import java.lang.IllegalStateException;
import java.lang.Override;

public class MainActivity_ViewBinding implements Unbinder {
  private MainActivity target;

  @UiThread
  public MainActivity_ViewBinding(MainActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public MainActivity_ViewBinding(MainActivity target, View source) {
    this.target = target;

    target.buttons = Utils.listOf(
        Utils.findRequiredViewAsType(source, R.id.one, "field 'buttons'", Button.class), 
        Utils.findRequiredViewAsType(source, R.id.two, "field 'buttons'", Button.class), 
        Utils.findRequiredViewAsType(source, R.id.three, "field 'buttons'", Button.class), 
        Utils.findRequiredViewAsType(source, R.id.four, "field 'buttons'", Button.class), 
        Utils.findRequiredViewAsType(source, R.id.five, "field 'buttons'", Button.class), 
        Utils.findRequiredViewAsType(source, R.id.six, "field 'buttons'", Button.class), 
        Utils.findRequiredViewAsType(source, R.id.seven, "field 'buttons'", Button.class), 
        Utils.findRequiredViewAsType(source, R.id.eight, "field 'buttons'", Button.class), 
        Utils.findRequiredViewAsType(source, R.id.nine, "field 'buttons'", Button.class), 
        Utils.findRequiredViewAsType(source, R.id.zero, "field 'buttons'", Button.class));
  }

  @Override
  @CallSuper
  public void unbind() {
    MainActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.buttons = null;
  }
}
