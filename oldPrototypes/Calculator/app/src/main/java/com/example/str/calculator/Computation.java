package com.example.str.calculator;

import android.widget.Toast;

/**
 * Created by str on 23.04.17.
 */

public class Computation {


    public static String compute (String s, Operation op) {
        String[] tab;
        int res = 0;
        switch (op.getAction()){

            case "\\+":
                tab = getArr(s, op.getAction());
                for( String a: tab) {
                    res += Integer.parseInt(a);
                }
                break;

            case "\\-":
                tab = getArr(s, op.getAction());
                if (tab[0].equals("")){
                    res = Integer.parseInt(tab[1])*(-1);
                    for (int a=2;a<tab.length;a++){
                        res -= Integer.parseInt(tab[a]);
                    }
                } else {
                    res =Integer.parseInt(tab[0]);
                    for (int a=1;a<tab.length; a++) {
                        res -= Integer.parseInt(tab[a]);
                    }
                }
                break;

            case "\\*":
                tab = getArr(s, op.getAction());
                if (tab[0].equals("")){
                    res = Integer.parseInt(tab[1]);
                    for (int a=2;a<tab.length;a++){
                        res *= Integer.parseInt(tab[a]);
                    }
                } else {
                    res =Integer.parseInt(tab[0]);
                    for (int a=1;a<tab.length; a++) {
                        res *= Integer.parseInt(tab[a]);
                    }
                }
                break;

            case "\\/":
                tab = getArr(s, op.getAction());
                if (tab[0].equals("")) {
                    res = Integer.parseInt(tab[1]);
                    for (int i = 2; i < tab.length; i++) {
                        if (Integer.parseInt(tab[i]) != 0) {
                            res /= Integer.parseInt(tab[i]);
                        } else {
                            res = -1;
                        }
                    }
                } else {
                    res = Integer.parseInt(tab[0]);
                    for (int i = 1; i < tab.length; i++) {
                        if (Integer.parseInt(tab[i]) != 0) {
                            res /= Integer.parseInt(tab[i]);
                        } else {
                            res = -1;
                        }
                    }
                }
                break;
        }

        return String.valueOf(res);
    }


    private static String[] getArr(String st,String sign) {

        return st.split(sign);
    }
}
