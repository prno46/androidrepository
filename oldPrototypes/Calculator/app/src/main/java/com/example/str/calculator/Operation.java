package com.example.str.calculator;

/**
 * Created by str on 20.04.17.
 */

public enum Operation {

    ADD("\\+"), SUB("\\-"), MULT("\\*"), DIV("\\/") ;


    private String action;

    Operation (String action) {
        this.action = action;
    }


    public String getAction() {
        return action;
    }
}
