package com.example.str.calculator;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import butterknife.BindViews;
import butterknife.ButterKnife;


public class MainActivity extends AppCompatActivity {

    private TextView display;
    private String str;

    private Button add;
    private Button sub;
    private Button mult;
    private Button div;

    private Operation operation;

    @BindViews({R.id.one,R.id.two,R.id.three,R.id.four,R.id.five,R.id.six,R.id.seven,R.id.eight,R.id.nine,R.id.zero})
    List<Button> buttons;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        add = (Button) findViewById(R.id.addpn);
        sub = (Button) findViewById(R.id.sub);
        mult = (Button) findViewById(R.id.mult);
        div = (Button) findViewById(R.id.div);

        display = (TextView) findViewById(R.id.textView_pn);


        ButterKnife.bind(this);
    }

    public  void displayNumber (View v){

        Button button =  (Button) v;

        display.append(button.getText().toString());

       if(v == add){
           div.setEnabled(false);
           sub.setEnabled(false);
           mult.setEnabled(false);
           operation = Operation.ADD;
        }
        if (v == sub) {
            add.setEnabled(false);
            mult.setEnabled(false);
            div.setEnabled(false);
            operation = Operation.SUB;
       }
       if (v == mult){
           div.setEnabled(false);
           sub.setEnabled(false);
           add.setEnabled(false);
           operation = Operation.MULT;
       }
       if (v == div){
           sub.setEnabled(false);
           add.setEnabled(false);
           mult.setEnabled(false);
           operation = Operation.DIV;
       }

    }

    public void showResult(View v) {
        str = display.getText().toString();
        switch (operation) {
            case ADD:
                display.setText(str+"\n"+ Computation.compute(str,Operation.ADD));
                add.setEnabled(false);
                buttonsDisable(v);
                break;

            case SUB:
                display.setText(str+"\n"+ Computation.compute(str,Operation.SUB));
                sub.setEnabled(false);
                buttonsDisable(v);
                break;

            case MULT:
                display.setText(str+"\n"+ Computation.compute(str,Operation.MULT));
                mult.setEnabled(false);
                buttonsDisable(v);
                break;

            case DIV:
                display.setText(str+"\n"+ Computation.compute(str,Operation.DIV));
                div.setEnabled(false);
                buttonsDisable(v);
                break;
        }

    }

    public ButterKnife.Action<Button> DISABLE = new ButterKnife.Action<Button>(){

        @Override
        public void apply(Button view, int index) {
            view.setEnabled(false);
        }
    };

    private void buttonsDisable(View v) {
        v.setEnabled(false);
        ButterKnife.apply(buttons, DISABLE);
    }
}

//ToDo - zrobic refactoring MainActivity: uzyc butterKnife; Computation -zbyt duzo powtorek kodu