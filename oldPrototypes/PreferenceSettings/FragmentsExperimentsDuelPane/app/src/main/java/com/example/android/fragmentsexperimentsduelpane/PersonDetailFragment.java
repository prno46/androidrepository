package com.example.android.fragmentsexperimentsduelpane;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.android.fragmentsexperimentsduelpane.data.PersonsInfo;

/**
 * Created by str on 21.07.17.
 */

public class PersonDetailFragment extends Fragment {

    TextView textView;

    public PersonDetailFragment() {}


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.person_detail_fragment,container,false);

        textView = view.findViewById(R.id.textView_detail);
        textView.setText(PersonsInfo.DATA.getPersonDetailsAtIndex(getIndex()));

        return view;
    }


    public int getIndex() {
        return getArguments().getInt("index", 0);
    }
}
