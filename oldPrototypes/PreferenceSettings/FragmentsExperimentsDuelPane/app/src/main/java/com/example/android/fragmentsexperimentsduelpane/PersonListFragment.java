package com.example.android.fragmentsexperimentsduelpane;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.example.android.fragmentsexperimentsduelpane.data.OnPersonSelectedListener;
import com.example.android.fragmentsexperimentsduelpane.data.PersonsInfo;

/**
 * Created by str on 21.07.17.
 */

public class PersonListFragment extends ListFragment {

    private ListView listView;
    private ArrayAdapter<String> adapter;
    private OnPersonSelectedListener listener;


    public PersonListFragment() {}

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            listener = (OnPersonSelectedListener) context;
        } catch (ClassCastException ex) {
            throw new ClassCastException(context.toString() + "must implement OnPersonSelectedListener");
        }

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.person_list_fragment,container,false);
        listView = view.findViewById(android.R.id.list);
        adapter = new ArrayAdapter<>(getContext(),android.R.layout.simple_list_item_1, PersonsInfo.DATA.getPersons());
        listView.setAdapter(adapter);

        return view;
    }


    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
       // super.onListItemClick(l, v, position, id);
        listener.onPersonSelected(position);
    }

    @Override
    public void onPause() {
        super.onPause();
        //adapter.clear();

    }

    @Override
    public void onResume() {
        super.onResume();

    }


}
