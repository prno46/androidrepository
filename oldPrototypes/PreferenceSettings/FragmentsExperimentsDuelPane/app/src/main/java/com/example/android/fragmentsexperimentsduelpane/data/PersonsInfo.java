package com.example.android.fragmentsexperimentsduelpane.data;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by str on 21.07.17.
 */

public enum PersonsInfo {

    DATA;

    private List<String> persons = providePersons();
    private List<String> personDetails = providePersonDetails();


    private List<String> providePersons(){
        List<String> personsName = new ArrayList<>(9);
        personsName.add("adam testowy");
        personsName.add("jola zaspa");
        personsName.add("hania walcowa");
        personsName.add("pawel skoczny");
        personsName.add("marcin rogalski");
        personsName.add("agnieszka kortowa");
        personsName.add("jurek bocian");
        personsName.add("tomek teowy");
        personsName.add("ala kotna");
        personsName.add("pola monola");
        personsName.add("alina kotlina");
        personsName.add("bonifacy kracy");
        personsName.add("demon czarny");
        personsName.add("diabel rogaty");
        return personsName;
    }

    private List<String> providePersonDetails() {
        List<String> details = new ArrayList<>(9);
        details.add("to jest tekst o " + persons.get(0));
        details.add("to jest tekst o " + persons.get(1));
        details.add("to jest tekst o " + persons.get(2));
        details.add("to jest tekst o " + persons.get(3));
        details.add("to jest tekst o " + persons.get(4));
        details.add("to jest tekst o " + persons.get(5));
        details.add("to jest tekst o " + persons.get(6));
        details.add("to jest tekst o " + persons.get(7));
        details.add("to jest tekst o " + persons.get(8));

        return details;
    }

    public String getPersonAtIndex (int index) {
        return persons.get(index);
    }

    public String getPersonDetailsAtIndex (int index) {
        return personDetails.get(index);
    }

    public List<String> getPersons() {
        return persons;
    }

    public List<String> getPersonDetails() {
        return personDetails;
    }
}
