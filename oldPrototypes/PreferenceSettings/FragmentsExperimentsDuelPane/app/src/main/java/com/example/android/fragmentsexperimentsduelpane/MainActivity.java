package com.example.android.fragmentsexperimentsduelpane;

import android.content.Intent;
import android.content.res.Configuration;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.example.android.fragmentsexperimentsduelpane.data.OnPersonSelectedListener;

public class MainActivity extends AppCompatActivity implements OnPersonSelectedListener{

    //private int index;
    private PersonListFragment myFragment;
    private PersonDetailFragment detail;
    private boolean isDualPanel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        isDualPanel = false;
        Log.v("STR",getClass().getSimpleName()+"in onCreate method before added fragment listView");

        if(savedInstanceState == null) {
            myFragment = new PersonListFragment();
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.fragment_container, myFragment)
                    .commit();
        }
        Log.v("STR",getClass().getSimpleName()+" in onCreate method after added fragment listView");

        if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE){
            //detail = new PersonDetailFragment();
           /* detail.setArguments(args());
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.fragmentDetail_container, detail)
                    .commit();*/
           isDualPanel = true;

        }
    }


    @Override
    public void onPersonSelected(int position) {
        if(!isDualPanel) {
            Log.v("STR", getLocalClassName() + " position passed " + position);
            startDetailActivity(position);
        } else {
            detail = new PersonDetailFragment();
            detail.setArguments(args(position));
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.fragmentDetail_container, detail).commit();
        }
    }


    public void startDetailActivity(int index) {
        Intent intent = new Intent(this,DetailActivity.class);
        intent.putExtra("index",index);
        startActivity(intent);

    }

    private Bundle args(int index) {
        Bundle bundle = new Bundle();
        bundle.putInt("index",index);
        return bundle;
    }


    @Override
    protected void onResume() {
        super.onResume();
        Log.v("STR",getClass().getSimpleName()+" in onResume method");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.v("STR",getClass().getSimpleName()+" in onPause method");

    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.v("STR",getClass().getSimpleName()+" in onStop method");
    }

}
