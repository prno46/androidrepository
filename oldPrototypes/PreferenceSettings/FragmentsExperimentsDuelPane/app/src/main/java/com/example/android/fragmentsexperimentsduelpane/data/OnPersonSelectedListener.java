package com.example.android.fragmentsexperimentsduelpane.data;

/**
 * Created by str on 23.07.17.
 */

public interface OnPersonSelectedListener {

    void onPersonSelected (int position);
}
