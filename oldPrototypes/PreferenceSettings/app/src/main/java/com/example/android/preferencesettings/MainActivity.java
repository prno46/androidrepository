package com.example.android.preferencesettings;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.preference.PreferenceManager;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    private final String KEY = "preference_screen";
    private final String PLAYER_ONE_NAME = "pref_playerOneName";


    @BindView(R.id.textView_displaySettings)
    TextView displaySettings;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);


    }

    @Override
    protected void onResume() {
        displayPreferencesSettings();
        super.onResume();
    }

    @OnClick(R.id.button_Settings)
    public void setSettings() {
        Intent intent = new Intent(this,PreferencesActivity.class);
        startActivity(intent);

    }

    private void displayPreferencesSettings(){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        StringBuilder sb = new StringBuilder();

        sb.append(sharedPreferences.getString(PLAYER_ONE_NAME,""));

        displaySettings.setText(sb.toString());
    }

}
