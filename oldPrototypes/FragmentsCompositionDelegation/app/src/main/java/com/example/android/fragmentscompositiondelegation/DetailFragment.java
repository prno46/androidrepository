package com.example.android.fragmentscompositiondelegation;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * Created by str on 04.06.17.
 */

public class DetailFragment extends Fragment {

    private TextView titleTextView;
    private TextView contentTextView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_detail, container, false);

        titleTextView = (TextView) view.findViewById(R.id.titleTextView);
        contentTextView = (TextView) view.findViewById(R.id.contentTextView);

        return view;
    }

    public void showBasicData() {
        titleTextView.setText(R.string.basic_data);
        contentTextView.setText("SOME TEXT TO DISPLAY");

    }

    public void showDetailData() {
        titleTextView.setText(R.string.detail_data);
        contentTextView.setText("DETAIL DATA TO DISPLAY");
    }
}
