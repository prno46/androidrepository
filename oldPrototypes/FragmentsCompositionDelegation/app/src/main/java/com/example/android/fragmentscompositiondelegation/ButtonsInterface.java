package com.example.android.fragmentscompositiondelegation;

/**
 * Created by str on 04.06.17.
 */

public interface ButtonsInterface {

    void showBasicData();

    void showDetailData();

}
