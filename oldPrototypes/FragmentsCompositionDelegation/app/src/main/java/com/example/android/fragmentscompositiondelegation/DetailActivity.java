package com.example.android.fragmentscompositiondelegation;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class DetailActivity extends AppCompatActivity {

    public static final int BASIC = 0;
    public static final int DETAIL = 1;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        final int data = getIntent().getIntExtra("data", BASIC);

        DetailFragment fragment = (DetailFragment) getSupportFragmentManager()
                .findFragmentById(R.id.detailFragment);

        if(data == BASIC) {
            fragment.showDetailData();
        } else if (data == DETAIL) {
            fragment.showDetailData();
        }

    }
}
