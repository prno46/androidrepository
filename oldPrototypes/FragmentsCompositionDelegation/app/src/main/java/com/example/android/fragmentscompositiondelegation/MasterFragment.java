package com.example.android.fragmentscompositiondelegation;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

/**
 * Created by str on 04.06.17.
 */

public class MasterFragment extends Fragment{



    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_master, container, false);
        Button basicDataButton = (Button) view.findViewById(R.id.basicDataButton);
        Button detailDataButton = (Button) view.findViewById(R.id.detailDataButton);

        final ButtonsInterface buttonsInterface = (ButtonsInterface) getActivity();
        basicDataButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttonsInterface.showBasicData();
            }
        });

        detailDataButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttonsInterface.showDetailData();
            }
        });

        return view;
    }
}
