package com.example.android.fragmentscompositiondelegation;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity implements  ButtonsInterface {


    private DetailFragment fragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        fragment = (DetailFragment) getSupportFragmentManager().
                findFragmentById(R.id.detailFragment);

        if(fragment !=null && fragment.isInLayout()){
            fragment.showBasicData();
        }
    }



    @Override
    public void showBasicData() {

        if(fragment != null && fragment.isInLayout()) {
            fragment.showBasicData();
        } else {
            Intent intent = new Intent(this, DetailActivity.class);
            intent.putExtra("data", DetailActivity.BASIC);
            startActivity(intent);
        }
    }



    @Override
    public void showDetailData() {

        if(fragment != null && fragment.isInLayout()){
            fragment.showDetailData();
        } else {
            Intent intent = new Intent(this, DetailActivity.class);
            intent.putExtra("data", DetailActivity.DETAIL);
            startActivity(intent);
        }
    }


}
