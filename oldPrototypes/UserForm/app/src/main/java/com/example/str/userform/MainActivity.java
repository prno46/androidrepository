package com.example.str.userform;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;


public class MainActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener{

    private EditText myName;
    private EditText myLastName;
    private EditText myAge;
    private Spinner spinner;
    private ArrayAdapter<CharSequence> adapter;
    String spinnerText = "";
    ArrayList<String> optionList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        myName = (EditText) findViewById(R.id.my_name);
        myLastName = (EditText) findViewById(R.id.my_lastName);
        myAge = (EditText) findViewById(R.id.my_age);
        spinner = (Spinner) findViewById(R.id.my_spinner);

        spinner.setOnItemSelectedListener(this);
        adapter = ArrayAdapter.createFromResource(this, R.array.nationality, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);

        optionList = new ArrayList<>();

    }


    public void showText (View v){
        optionList.add(myName.getText().toString());
        optionList.add(myLastName.getText().toString());
        optionList.add(myAge.getText().toString());
        optionList.add(this.spinnerText);

        Intent i = new Intent(getApplicationContext(), SecondActivity.class);
        i.putStringArrayListExtra("Message", optionList);

        startActivity(i);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        this.spinnerText = parent.getItemAtPosition(position).toString();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
