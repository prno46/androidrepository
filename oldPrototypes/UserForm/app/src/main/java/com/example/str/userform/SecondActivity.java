package com.example.str.userform;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import org.w3c.dom.Text;

public class SecondActivity extends AppCompatActivity {

    private TextView myTextViewName;
    private TextView myTextViewLastName;
    private TextView myTextViewAge;
    private TextView myTextViewSpinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        myTextViewName = (TextView) findViewById(R.id.my_name_textView);
        myTextViewLastName = (TextView) findViewById(R.id.my_last_name_textView);
        myTextViewAge = (TextView) findViewById(R.id.my_textView_age);
        myTextViewSpinner = (TextView) findViewById(R.id.my_textView_spinner);


        if (getIntent().hasExtra("Message")){
            myTextViewName.setText(getIntent().getStringArrayListExtra("Message").get(0));
            myTextViewLastName.setText(getIntent().getStringArrayListExtra("Message").get(1));
            myTextViewAge.setText(getIntent().getStringArrayListExtra("Message").get(2));
            myTextViewSpinner.setText(getIntent().getStringArrayListExtra("Message").get(3));

        }
    }

    public void confirm (View v){
        //ToDo - what to do here

    }
}
