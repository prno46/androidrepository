package examples.sda.jsonapp;


import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import butterknife.BindView;
import butterknife.ButterKnife;
import examples.sda.jsonapp.DataStructure.CurrencyAdapter;
import examples.sda.jsonapp.DataStructure.NbpCurrencyProvider;

public class MainActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {

    @BindView(R.id.recycler_view_main)
    private RecyclerView recyclerView;

    @BindView(R.id.swipe_refresh_layout_main)
    private SwipeRefreshLayout swipeRefreshLayout;

    private CurrencyAdapter currencyAdapter;
    private NbpCurrencyProvider nbpCurrencyProvider;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        nbpCurrencyProvider = new NbpCurrencyProvider();


    }

    private void setUp() {
        recyclerView.setHasFixedSize(false);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        //currencyAdapter = new CurrencyAdapter(nbpCurrencyProvider);

    }


    @Override
    public void onRefresh() {
        // aktualizacja danych do recyclerView
    }

    //Todo - wykorzystac broadcaster do odświeżenia danych po zaladowaniu
}

