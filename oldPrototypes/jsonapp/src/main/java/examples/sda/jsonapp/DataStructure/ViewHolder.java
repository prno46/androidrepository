package examples.sda.jsonapp.DataStructure;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import examples.sda.jsonapp.R;

/**
 * Created by RENT on 2017-05-13.
 */

public class ViewHolder extends RecyclerView.ViewHolder  {


    @BindView(R.id.currency_name_textView)
    TextView currencyName;

    @BindView(R.id.currency_exchange_textView)
    TextView currencyRatio;


    public ViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(itemView);

    }

    public TextView getCurrencyName() {
        return currencyName;
    }

    public TextView getCurrencyRatio() {
        return currencyRatio;
    }
}
