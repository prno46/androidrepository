package examples.sda.jsonapp.DataStructure;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import examples.sda.jsonapp.Model.Table;
import examples.sda.jsonapp.R;

/**
 * Created by RENT on 2017-05-13.
 */

public class CurrencyAdapter extends RecyclerView.Adapter <ViewHolder> {

    private List<Table> currencyData;



    public CurrencyAdapter(List<Table> currencyData) {
        currencyData = new ArrayList<>();
        this.currencyData = currencyData;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.currency_layout, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Table table = currencyData.get(position);
        holder.getCurrencyName().setText(table.getTableContent().get(position).getCurrencyName());
        holder.getCurrencyRatio().setText(table.getTableContent().get(position).getCurrencyCode());
    }

    @Override
    public int getItemCount() {

        return currencyData.size();
    }


    //ToDo - dopisanie metody do aktualizacji danych (metoda set data)
    // ToDo - metoda do wylączenia swipLayout
}
