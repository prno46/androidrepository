package examples.sda.jsonapp.Model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by RENT on 2017-05-13.
 */

public class Table {

    private final String tableName;
    private final String tableNumber;
    private final String tableExpirationDate;
    private List<Currency> tableContent;

    public Table(String tableName, String tableNumber, String tableValidationDate) {
        tableContent = new ArrayList<>();
        this.tableName = tableName;
        this.tableNumber = tableNumber;
        this.tableExpirationDate = tableValidationDate;
    }

    public String getTableName() {
        return tableName;
    }

    public String getTableNumber() {
        return tableNumber;
    }

    public String getTableExpirationDate() {
        return tableExpirationDate;
    }

    public List<Currency> getTableContent() {
        return tableContent;
    }
}
