package examples.sda.jsonapp.Model;

/**
 * Created by RENT on 2017-05-13.
 */

public class Currency {

    private final String currencyName;
    private final String currencyCode;
    private final double currencyRatio;


    public Currency(String currencyName, String currencyCode, double currencyRatio) {
        this.currencyName = currencyName;
        this.currencyCode = currencyCode;
        this.currencyRatio = currencyRatio;
    }


    public String getCurrencyName() {
        return currencyName;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public double getCurrencyRatio() {
        return currencyRatio;
    }
}
