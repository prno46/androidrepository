package com.example.str.applicationlifecycle;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class SecondActivity extends AppCompatActivity {

    private TextView label;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        label = (TextView) findViewById(R.id.textView);

        if(getIntent().hasExtra("Parametr")){
            label.setText(getIntent().getStringExtra("Parametr"));
        }
    }

}
