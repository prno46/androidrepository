package com.example.str.applicationlifecycle;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import static android.R.id.input;

public class MainActivity extends AppCompatActivity {

    private EditText input;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        input = (EditText) findViewById(R.id.editText);
    }

    public void wykonaj_metode(View v) {

        if(v instanceof Button) {
            System.out.println("cisniety button");
        }

        Intent i = new Intent(getApplicationContext(), SecondActivity.class);
        i.putExtra("Parametr", input.getText().toString());
        startActivity(i);
    }

}
