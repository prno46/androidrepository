package  com.example.android.justjava;

/**
 * Add your package below. Package name can be found in the project's AndroidManifest.xml file.
 * This is the package name our example uses:
 *
 * package com.example.android.justjava;
 */

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import static android.R.attr.name;

/**
 * This app displays an order form to order coffee.
 */
public class MainActivity extends AppCompatActivity {

    private int quantity =0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    }

    /**
     * This method is called when the order button is clicked.
     */
    public void submitOrder(View view) {
        CheckBox whippedCreamCheckBox = (CheckBox) findViewById(R.id.cream_check_box);
        CheckBox chocolate = (CheckBox) findViewById(R.id.chocolate_check_box);
        EditText name = (EditText) findViewById(R.id.name_edit_text);
        String userName = name.getText().toString();
        boolean hasWhippedCream = whippedCreamCheckBox.isChecked();
        boolean hasChocolate = chocolate.isChecked();
        String subject = "Order of "+quantity+" coffees ";
        int price = calculatePrice(hasWhippedCream, hasChocolate);
        displayMessage(orderSummary(price, hasWhippedCream, hasChocolate, userName));
        sendOrderByEmail(subject, orderSummary(price,hasWhippedCream,hasChocolate,userName));
    }

    /**
     * This method displays the given quantity value on the screen.
     */
    private void display(int number) {
        TextView quantityTextView = (TextView) findViewById(R.id.quantity_text_view);
        quantityTextView.setText("" + number);
    }


    /**
     * This method displays the given text on the screen.
     */
    private void displayMessage(String message) {
        TextView orderSummaryTextView = (TextView) findViewById(R.id.order_summary_text_view);
        orderSummaryTextView.setText(message);
    }

    public void increment (View v) {
        if(quantity == 100){
            Toast.makeText(this, "You cannot have more then 100 coffees", Toast.LENGTH_SHORT).show();
            return;
        }
        quantity++;
        display(quantity);
    }

    public void decrement (View v) {
        if (quantity==1) {
            Toast.makeText(this, "You cannot have less then 1 coffee", Toast.LENGTH_SHORT).show();
            return;
        }
        quantity--;
        display(quantity);
    }

    private int calculatePrice(boolean hasCream, boolean hasChocolate ){
        int price = 5;
        if (hasCream){
            price += 1;
        }
        if (hasChocolate){
            price += 2;
        }
        return price*quantity;
    }

    private String orderSummary (int price, boolean addWhippedCream, boolean addChocolate, String name){
        String priceMessage = " Name: " +name;
        priceMessage += "\n add Whipped cream ? "+addWhippedCream;
        priceMessage += "\n add Chocolate ? "+addChocolate;
        priceMessage += "\n Quantity "+quantity;
        priceMessage += "\n Price "+price;
        priceMessage += "\n Thank you!";
        return priceMessage;
    }

    private void sendOrderByEmail(String subject, String message) {

        Intent intent = new Intent(Intent.ACTION_SENDTO);
        intent.setData(Uri.parse("mailto:"));
        intent.putExtra(Intent.EXTRA_SUBJECT, subject);
        intent.putExtra(Intent.EXTRA_TEXT,message);
        if(intent.resolveActivity(getPackageManager())!= null) {
            startActivity(intent);
        }

    }
}
