package com.example.android.brodcastreceiveraslistner;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    TextView toDisplay;


    private final String key = "key";
    private final IntentFilter intentFilter = new IntentFilter("com.example.android.MY_ACTION");

    LocalBroadcastManager localBroadcastManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        toDisplay = (TextView) findViewById(R.id.textView_display);
        localBroadcastManager = LocalBroadcastManager.getInstance(this);
        localBroadcastManager.registerReceiver(myReceiver,intentFilter);

        new MyMessage(this);
    }


    private BroadcastReceiver myReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if(intent.hasExtra(key)){
                Toast.makeText(context, "intent has extra kay", Toast.LENGTH_SHORT).show();
                String receivedMessage = intent.getStringExtra(key);
                toDisplay.setText(receivedMessage);
            } else {
                Toast.makeText(context, "intent has not extra key", Toast.LENGTH_SHORT).show();
            }
        }
    };


    @Override
    protected void onDestroy() {
        localBroadcastManager.unregisterReceiver(myReceiver);
        super.onDestroy();
    }
}
