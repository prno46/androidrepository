package com.example.android.brodcastreceiveraslistner;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.content.LocalBroadcastManager;

/**
 * Created by str on 15.06.17.
 */

public class MyMessage {

    private final String message = "This is a message from MyMessage.class";
    private final String key = "key";
    private final String intentFilter = "com.example.android.MY_ACTION";


    private LocalBroadcastManager localBroadcastManager;

    public MyMessage(Context context) {
        Intent intent = new Intent(intentFilter);
        intent.putExtra(key,message);
        localBroadcastManager = LocalBroadcastManager.getInstance(context);
        localBroadcastManager.sendBroadcast(intent);
    }

}
