package com.example.str.accelerometer;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements SensorEventListener {

    private TextView axis_x_label;
    private TextView axis_y_label;
    private TextView axis_z_label;

    private SensorManager sensorManager;
    private Sensor accelerometer;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        axis_x_label = (TextView) findViewById(R.id.axis_x);
        axis_y_label = (TextView) findViewById(R.id.axis_y);
        axis_z_label = (TextView) findViewById(R.id.axis_z);

        sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        if(sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER)!= null) {
            accelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
            sensorManager.registerListener(this, accelerometer, SensorManager.SENSOR_DELAY_NORMAL);
        }

    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        if(sensorEvent.sensor.getType() == Sensor.TYPE_ACCELEROMETER){
            float value_x = sensorEvent.values[0];
            String string_value_x = String.valueOf(value_x);
            axis_x_label.setText(string_value_x);

            float value_y = sensorEvent.values[1];
            String string_value_y = String.valueOf(value_y);
            axis_y_label.setText(string_value_y);

            float value_z = sensorEvent.values[2];
            String string_value_z = String.valueOf(value_z);
            axis_z_label.setText(string_value_z);


        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
        // we are doing nothing here for now
    }
}
