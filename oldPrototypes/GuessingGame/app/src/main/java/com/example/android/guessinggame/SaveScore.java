package com.example.android.guessinggame;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.example.android.guessinggame.BestScoresList.SHARED_FILE_NAME;

public class SaveScore extends AppCompatActivity {

    private int userScore;

    @BindView(R.id.saveLogin_scoreInfo_textView_id)
    TextView displayUserScore;

    @BindView(R.id.saveLoginInfo_textView_id)
    TextView displayLoginMessage;
    @BindView(R.id.userLogin_input_editText_id)
    EditText userLogin;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_save_score);
        ButterKnife.bind(this);

        Intent intent = getIntent();
        if (intent.hasExtra(GuessingGame.USER_SCORE)){
            Toast.makeText(this, "Intent has arrived", Toast.LENGTH_SHORT).show();
            userScore = intent.getIntExtra(GuessingGame.USER_SCORE,0);
            displayUserScore.setText("Your score is: "+String.valueOf(userScore));
        }
    }



    @OnClick(R.id.saveData_button_id)
    public void saveUserData(View v) {
        SharedPreferences preferences = getSharedPreferences(SHARED_FILE_NAME, MODE_PRIVATE);
        SharedPreferences.Editor save = preferences.edit();
        save.putInt(userLogin.getText().toString(),userScore);
        save.apply();
        finish();
    }
}
