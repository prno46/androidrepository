package com.example.android.guessinggame;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class GuessingGame extends AppCompatActivity {

    public static final String USER_SCORE = "userScore";

    private int noToGuess;

    private int attempt = 1;

    @BindView(R.id.game_messages_textView_id)
    TextView messages;
    @BindView(R.id.show_number_to_guess_id)
    TextView numberToGuess;
    @BindView(R.id.number_of_attempts_id)
    TextView attempts;
    @BindView(R.id.user_input_id)
    EditText userinput;

    @BindView(R.id.start_game_button_id)
    Button start;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_guessing_game);
        ButterKnife.bind(this);

    }


    private int getRandomNumber() {
        Random random = new Random();
        int toGuess = random.nextInt(50)+1;
        return toGuess;
    }


    private void toLoginActivity(int score) {
        Intent intent = new Intent(this,SaveScore.class);
        intent.putExtra(USER_SCORE,score);
        attempt = 1;
        startActivity(intent);
    }

    @OnClick(R.id.start_game_button_id)
    public void startToPlay(View v) {
        noToGuess = getRandomNumber();
        messages.setText("Guess the number from range 1 - 50");
        attempts.setText(String.valueOf(attempt));
        userinput.setText("");
        numberToGuess.setText(String.valueOf(noToGuess));
        v.setEnabled(false);
    }


    @OnClick(R.id.user_input_confirmation_button_id)
    public void checkUserGuess (View v) {

        int userGuess = Integer.valueOf(userinput.getText().toString());
        if (userGuess != noToGuess) {
            ++ attempt;
            messages.setText("Guess again numbers 1-50");
            attempts.setText(String.valueOf(attempt));
            userinput.setText("");
        } else {
            ++attempt;
            attempts.setText(String.valueOf(attempt));
            Toast.makeText(this, "Bravo you have guessed", Toast.LENGTH_SHORT).show();
            start.setEnabled(true);
            toLoginActivity(attempt);
        }
    }
}
