package com.example.android.guessinggame;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class BestScoresList extends AppCompatActivity {

    public static final String SHARED_FILE_NAME = "fake_data3";

    @BindView(R.id.listview_with_fab)
    ListView listView;

    private List<String> items;
    private ArrayAdapter<String> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_best_scores_list);
        ButterKnife.bind(this);

        //fakeSharedPrefSaved();

        items = new ArrayList<>();
        items = sharedList();

        adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, items);
        listView.setAdapter(adapter);


    }

    @Override
    protected void onResume() {
        super.onResume();
        List<String> items2;
        items2 = sharedList();
        adapter.clear();
        adapter.addAll(items2);
        adapter.notifyDataSetChanged();

    }

    @OnClick(R.id.floating_action_button_fab_with_listview)
    public void startGuessingGame(View v) {

        Intent intent = new Intent(this,GuessingGame.class);
        startActivity(intent);
    }


    private List<String> sharedList() {
        SharedPreferences preferences = getSharedPreferences(SHARED_FILE_NAME, MODE_PRIVATE);
        List mlist = new ArrayList<>();
        Map<String, ? > map;

        List<Map.Entry<String, Integer>> seList;
        map = preferences.getAll();
        Map<String, Integer> sotredMap = new LinkedHashMap<>();

        for (Map.Entry<String,?> entry : map.entrySet()){
            sotredMap.put(entry.getKey(), Integer.parseInt(entry.getValue().toString()));
        }

        seList = sortTheMap(sotredMap);
        for (Map.Entry<String, Integer> key : seList ){
            mlist.add(key);
        }

        return  mlist;
    }


    private List<Map.Entry<String, Integer>> sortTheMap(Map<String, Integer> map) {

        List<Map.Entry<String, Integer>> list =
                new LinkedList<>(map.entrySet());

        Collections.sort(list, new Comparator<Map.Entry<String, Integer>>() {
            public int compare(Map.Entry<String, Integer> o1,
                               Map.Entry<String, Integer> o2) {
                return (o1.getValue()).compareTo(o2.getValue());
            }
        });

        return list;
    }


}
