// Generated code from Butter Knife. Do not modify!
package com.example.android.guessinggame;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import java.lang.IllegalStateException;
import java.lang.Override;

public class SaveScore_ViewBinding implements Unbinder {
  private SaveScore target;

  private View view2131558533;

  @UiThread
  public SaveScore_ViewBinding(SaveScore target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public SaveScore_ViewBinding(final SaveScore target, View source) {
    this.target = target;

    View view;
    target.displayUserScore = Utils.findRequiredViewAsType(source, R.id.saveLogin_scoreInfo_textView_id, "field 'displayUserScore'", TextView.class);
    target.displayLoginMessage = Utils.findRequiredViewAsType(source, R.id.saveLoginInfo_textView_id, "field 'displayLoginMessage'", TextView.class);
    target.userLogin = Utils.findRequiredViewAsType(source, R.id.userLogin_input_editText_id, "field 'userLogin'", EditText.class);
    view = Utils.findRequiredView(source, R.id.saveData_button_id, "method 'saveUserData'");
    view2131558533 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.saveUserData(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    SaveScore target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.displayUserScore = null;
    target.displayLoginMessage = null;
    target.userLogin = null;

    view2131558533.setOnClickListener(null);
    view2131558533 = null;
  }
}
