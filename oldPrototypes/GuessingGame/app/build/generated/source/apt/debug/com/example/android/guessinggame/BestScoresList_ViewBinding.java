// Generated code from Butter Knife. Do not modify!
package com.example.android.guessinggame;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.ListView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import java.lang.IllegalStateException;
import java.lang.Override;

public class BestScoresList_ViewBinding implements Unbinder {
  private BestScoresList target;

  private View view2131558523;

  @UiThread
  public BestScoresList_ViewBinding(BestScoresList target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public BestScoresList_ViewBinding(final BestScoresList target, View source) {
    this.target = target;

    View view;
    target.listView = Utils.findRequiredViewAsType(source, R.id.listview_with_fab, "field 'listView'", ListView.class);
    view = Utils.findRequiredView(source, R.id.floating_action_button_fab_with_listview, "method 'startGuessingGame'");
    view2131558523 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.startGuessingGame(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    BestScoresList target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.listView = null;

    view2131558523.setOnClickListener(null);
    view2131558523 = null;
  }
}
