// Generated code from Butter Knife. Do not modify!
package com.example.android.guessinggame;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import java.lang.IllegalStateException;
import java.lang.Override;

public class GuessingGame_ViewBinding implements Unbinder {
  private GuessingGame target;

  private View view2131558528;

  private View view2131558529;

  @UiThread
  public GuessingGame_ViewBinding(GuessingGame target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public GuessingGame_ViewBinding(final GuessingGame target, View source) {
    this.target = target;

    View view;
    target.messages = Utils.findRequiredViewAsType(source, R.id.game_messages_textView_id, "field 'messages'", TextView.class);
    target.numberToGuess = Utils.findRequiredViewAsType(source, R.id.show_number_to_guess_id, "field 'numberToGuess'", TextView.class);
    target.attempts = Utils.findRequiredViewAsType(source, R.id.number_of_attempts_id, "field 'attempts'", TextView.class);
    target.userinput = Utils.findRequiredViewAsType(source, R.id.user_input_id, "field 'userinput'", EditText.class);
    view = Utils.findRequiredView(source, R.id.start_game_button_id, "field 'start' and method 'startToPlay'");
    target.start = Utils.castView(view, R.id.start_game_button_id, "field 'start'", Button.class);
    view2131558528 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.startToPlay(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.user_input_confirmation_button_id, "method 'checkUserGuess'");
    view2131558529 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.checkUserGuess(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    GuessingGame target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.messages = null;
    target.numberToGuess = null;
    target.attempts = null;
    target.userinput = null;
    target.start = null;

    view2131558528.setOnClickListener(null);
    view2131558528 = null;
    view2131558529.setOnClickListener(null);
    view2131558529 = null;
  }
}
