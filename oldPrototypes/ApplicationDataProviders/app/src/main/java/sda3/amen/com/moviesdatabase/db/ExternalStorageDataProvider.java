package sda3.amen.com.moviesdatabase.db;

import android.os.Environment;
import android.util.Log;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.text.ParseException;
import java.util.LinkedList;
import java.util.List;

import sda3.amen.com.moviesdatabase.model.Movie;

/**
 * Created by amen on 6/1/17.
 */

public class ExternalStorageDataProvider implements IDataProvider {

    private static final String FILE_PATH = "external.txt";

    @Override
    public List<Movie> getAllMovies() {
        List<Movie> list = new LinkedList<>();

        try {
            File inputFile = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + FILE_PATH);
            FileInputStream fis = new FileInputStream(inputFile);
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(fis));

            // przeczytaj linie, jesli nie bedzie niczego na wejsciu to buffered reader zwróci
            // null z metody readLine
            String line = bufferedReader.readLine();

            // dopoki linia nie bedzie pusta
            while (line != null) {
                try {
                    Movie singleMovie = new Movie(line);
                    list.add(singleMovie);
                } catch (ParseException pe) {
                    Log.e(getClass().getName(), pe.getMessage());
                }
                // czytaj nastepna linie
                line = bufferedReader.readLine();
            }

            bufferedReader.close();
        } catch (IOException ioe) {
            Log.e(getClass().getName(), ioe.getMessage());
        }

        return list;
    }

    @Override
    public void saveAllMovies(List<Movie> list) {
        try {
            FileOutputStream fos = new FileOutputStream(Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + FILE_PATH);
            PrintWriter printWriter = new PrintWriter(fos);

            for (Movie singleMovie : list) {
                printWriter.println(singleMovie.toDatabaseFormat());
            }

            printWriter.close();
        } catch (IOException ioe) {
            Log.e(getClass().getName(), ioe.getMessage());
        }
    }
}
