package sda3.amen.com.moviesdatabase;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import butterknife.BindView;
import butterknife.ButterKnife;
import sda3.amen.com.moviesdatabase.db.ExternalStorageDataProvider;
import sda3.amen.com.moviesdatabase.db.IDataProvider;
import sda3.amen.com.moviesdatabase.db.InternalDatabaseDataProvider;
import sda3.amen.com.moviesdatabase.db.InternalStorageDataProvider;
import sda3.amen.com.moviesdatabase.db.SharedPreferencesDataProvider;
import sda3.amen.com.moviesdatabase.model.Movie;
import sda3.amen.com.moviesdatabase.model.ProviderType;

public class ViewMoviesActivity extends AppCompatActivity {

    @BindView(R.id.moviesList)
    protected ListView moviesList;

    private ArrayAdapter<Movie> listAdapter;
    private IDataProvider provider;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_movies);
        ButterKnife.bind(this);

        if (getIntent().hasExtra("PROVIDER_TYPE")) {
            String providerType = getIntent().getStringExtra("PROVIDER_TYPE");
            ProviderType type = ProviderType.valueOf(providerType);

            // utworzenie providera
            if (type == ProviderType.SHARED_PREFERENCES) {
                provider = new SharedPreferencesDataProvider(this);
            } else if (type == ProviderType.INTERNAL_STORAGE) {
                provider = new InternalStorageDataProvider(this);
            } else if (type == ProviderType.EXTERNAL_STORAGE) {
                provider = new ExternalStorageDataProvider();
            } else if (type == ProviderType.SQLITE) {
                provider = new InternalDatabaseDataProvider(this);
            }
        }

        // tworzymy adapter (potrzebny do dodawania elementów do listView)
        listAdapter = new ArrayAdapter<Movie>(this,
                android.R.layout.simple_list_item_1,
                provider.getAllMovies());

        // podpinamy adapter do listView
        moviesList.setAdapter(listAdapter);

    }
}
