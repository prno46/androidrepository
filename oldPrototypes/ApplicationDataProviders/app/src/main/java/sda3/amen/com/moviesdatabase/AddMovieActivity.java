package sda3.amen.com.moviesdatabase;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import sda3.amen.com.moviesdatabase.db.ExternalStorageDataProvider;
import sda3.amen.com.moviesdatabase.db.IDataProvider;
import sda3.amen.com.moviesdatabase.db.InternalDatabaseDataProvider;
import sda3.amen.com.moviesdatabase.db.InternalStorageDataProvider;
import sda3.amen.com.moviesdatabase.db.SharedPreferencesDataProvider;
import sda3.amen.com.moviesdatabase.model.Movie;
import sda3.amen.com.moviesdatabase.model.ProviderType;

public class AddMovieActivity extends AppCompatActivity {

    private IDataProvider provider;

    @BindView(R.id.editTitle)
    protected EditText editTitle;

    @BindView(R.id.editUrl)
    protected EditText editURL;

    @BindView(R.id.editRelease)
    protected EditText editRelease;

    @BindView(R.id.editDuration)
    protected EditText editDuration;

    @BindView(R.id.editSeenTimes)
    protected EditText editSeenTimes;

    @BindView(R.id.checkSeen)
    protected CheckBox checkSeen;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_movie);
        ButterKnife.bind(this);

        if (getIntent().hasExtra("PROVIDER_TYPE")) {
            String providerType = getIntent().getStringExtra("PROVIDER_TYPE");
            ProviderType type = ProviderType.valueOf(providerType);

            // utworzenie providera
            if (type == ProviderType.SHARED_PREFERENCES) {
                provider = new SharedPreferencesDataProvider(this);
            } else if (type == ProviderType.INTERNAL_STORAGE) {
                provider = new InternalStorageDataProvider(this);
            } else if (type == ProviderType.EXTERNAL_STORAGE) {
                provider = new ExternalStorageDataProvider();
            } else if (type == ProviderType.SQLITE) {
                provider = new InternalDatabaseDataProvider(this);
            }
        }
    }

    @OnClick(R.id.buttonSave)
    protected void createMovie() {
        // tworzymy obiekt
        Movie newMovie = new Movie();

        // ustawiamy mu wszystkie jego pola (poza id, które zostanie przydzielone przez bazę)
        newMovie.setTitle(editTitle.getText().toString());
        newMovie.setGraphicsURL(editURL.getText().toString());

        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
            newMovie.setReleaseDate(sdf.parse(editRelease.getText().toString()));
        } catch (ParseException pe) {
            Toast.makeText(this, "Wrong date format! Use yyyy/MM/dd.", Toast.LENGTH_SHORT).show();
            return;
        }

        newMovie.setDuration(Integer.parseInt(editDuration.getText().toString()));
        newMovie.setWatchedBy(Integer.parseInt(editSeenTimes.getText().toString()));
        newMovie.setSeen(checkSeen.isChecked());

        // zapis do bazy
        // pobranie wszystkich filmów
        List<Movie> movies = provider.getAllMovies();

        // dodanie nowego filmu do kolekcji
        movies.add(newMovie);

        // wysłanie filmów do zapisu
        provider.saveAllMovies(movies);
        finish();
    }
}
