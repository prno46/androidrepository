package com.example.android.greendaotestproject.recycler.view;


import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;
import com.example.android.greendaotestproject.R;
import com.example.android.greendaotestproject.model.User;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by str on 26.08.17.
 *
 */

public class DBAdapter extends RecyclerView.Adapter<DBAdapter.ViewHolder> {

    public interface DatabaseOperations {
        void onUserEdit(long id, String[] values);
    }

    public static final String TAG_INTENT_LONG = "J23";
    public static final String TAG_INTENT_STRING = "maniana";
    private List<User> users;

    private DatabaseOperations databaseOperations;

    public DBAdapter () {
        users = new ArrayList<>();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.single_user_layout, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final User user = users.get(position);
        holder.userName.setText(user.getName());
        holder.userCreationDate.setText(user.getCreatedAt());

        holder.rootView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(view.getContext(), "item db id " + user.getId()
                                                                    , Toast.LENGTH_SHORT).show();
                long userId = user.getId();
                String[] values =  {user.getName(), user.getCreatedAt()};

                int updateOrDelete = values.length >0 ? 1:0;
                Log.d("STR", getClass().getSimpleName() + "updateOrDelete: " + updateOrDelete);

                databaseOperations.onUserEdit(userId, values);
            }
        });
    }

    @Override
    public int getItemCount() {
        return users.size();
    }

    public void addUser(User user) {
        users.add(user);
    }

    public void clearAdapter(){
        users.clear();
    }

    public void setDatabaseOperationsListener(DatabaseOperations listener) {
        this.databaseOperations = listener;
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        private TextView userName = null;
        private TextView userCreationDate = null;
        ConstraintLayout rootView;

        ViewHolder(View itemView) {
            super(itemView);
            userName = itemView.findViewById(R.id.textView_user_name);
            userCreationDate = itemView.findViewById(R.id.textView_creationDate);
            rootView = itemView.findViewById(R.id.single_user_display);
        }
    }

}
