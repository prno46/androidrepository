package com.example.android.greendaotestproject.model;

import org.greenrobot.greendao.converter.PropertyConverter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by str on 29.08.17.
 *
 */

public class DateConverter implements PropertyConverter<Long, String> {

    private SimpleDateFormat simpleDateFormat;
    private final String DATE_FORMAT = "dd/MM/yyyy";

    @Override
    public Long convertToEntityProperty(String databaseValue) {

        simpleDateFormat = new SimpleDateFormat(DATE_FORMAT, Locale.GERMANY);
        Date readDate = null;
        try {
            readDate = simpleDateFormat.parse(databaseValue);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return readDate !=null ? readDate.getTime() : System.currentTimeMillis();
    }

    @Override
    public String convertToDatabaseValue(Long entityProperty) {
        simpleDateFormat = new SimpleDateFormat(DATE_FORMAT, Locale.GERMANY);
        Date newDate = new Date(entityProperty);
        return simpleDateFormat.format(newDate);
    }
}
