package com.example.android.greendaotestproject;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import com.example.android.greendaotestproject.database.DataBaseInstance;
import com.example.android.greendaotestproject.database.DatabaseContract;
import com.example.android.greendaotestproject.model.User;
import com.example.android.greendaotestproject.recycler.view.DBAdapter;
import com.squareup.leakcanary.RefWatcher;

import org.greenrobot.greendao.async.AsyncOperation;
import org.greenrobot.greendao.async.AsyncOperationListener;


import java.util.List;

public class MainActivity extends AppCompatActivity implements AsyncOperationListener
                                                                    , DBAdapter.DatabaseOperations{
    final static int INSERT_REQUEST_CODE = 46;
    final static int UPDATE_REQUEST_CODE = 111;
    final static int DELETE_REQUEST_CODE = 112;

    RecyclerView recyclerView;
    DBAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        recyclerView = findViewById(R.id.recycler_view_mainActivity);
        initUsersList();
        initDatabase();
        reLoadData();
        Log.d("STR", getClass().getSimpleName() +" in onCreate method after reload data ");
    }

    private void initUsersList() {
        adapter = new DBAdapter();
        adapter.setDatabaseOperationsListener(this);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setHasFixedSize(false);
        recyclerView.setAdapter(adapter);

        LinearLayoutManager layoutManager =
                                new LinearLayoutManager(this, LinearLayoutManager.VERTICAL,false);
        recyclerView.setLayoutManager(layoutManager);
        DividerItemDecoration dividerItemDecoration =
                new DividerItemDecoration(recyclerView.getContext(),layoutManager.getOrientation());
        recyclerView.addItemDecoration(dividerItemDecoration);
    }

    private void initDatabase() {
        DatabaseContract dataBaseInstance = DataBaseInstance.getInstance(this);
        dataBaseInstance.setDatabaseConnection();
        dataBaseInstance.setOperationListener(this);
    }

    @Override // AsyncOperationListener method
    public void onAsyncOperationCompleted(AsyncOperation operation) {
            switch (operation.getType()){
                case LoadAll:
                    @SuppressWarnings("unchecked")
                    List<User> asop =  (List<User>) operation.getResult();
                    for (User u : asop)  {
                        adapter.addUser(u);
                    }
                    break;

                case Update:
                    reLoadData();
                    break;

                case Delete:
                    reLoadData();
                    break;

                default:
                    throw new IllegalArgumentException("such an operation is not supported "
                                                                            + operation.getType());
            }
    }

    public void onAddNewUserButton(View view) {
        Intent intent = new Intent(this, AddNewUserActivity.class);
        startActivityForResult(intent, INSERT_REQUEST_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d("STR", getClass().getSimpleName() +" in onActivityResult requestCode "+ requestCode);
        Log.d("STR", getClass().getSimpleName() +" in onActivityResult resultCode "+ resultCode);
        if(data != null){
            DataBaseInstance dbi = DataBaseInstance.getInstance(this);
            switch(requestCode) {
                case INSERT_REQUEST_CODE:
                    final long newUserId = dbi.getNextAvailableUserId();
                    String[] values = data.getStringArrayExtra(AddNewUserActivity.INSERT_NEW_USER);
                    User user = new User(newUserId,values[0],values[1],"");
                    dbi.insertNewUser(user);
                    break;

                case UPDATE_REQUEST_CODE:
                    Log.d("STR", getClass().getSimpleName() +" case UPDATE "+ resultCode);
                    final long userId = data.getLongExtra(DBAdapter.TAG_INTENT_LONG, 99);
                    final String[] userData = data
                                                .getStringArrayExtra(DBAdapter.TAG_INTENT_STRING);
                    User newUser = new User(userId, userData[0], userData[1], "");

                    if(resultCode != DELETE_REQUEST_CODE) {
                        Log.d("STR", getClass().getSimpleName() +" in update IF "+ resultCode);
                        dbi.updateUserData(newUser);
                    } else {
                        Log.d("STR", getClass().getSimpleName() +" in delete IF "+ resultCode);
                        dbi.deleteUserFromDatabase(newUser);
                    }
                    break;
            }
        } else {
            Log.d("STR", getClass().getSimpleName() +" no data from intent ");
        }
        super.onActivityResult(requestCode, resultCode, data);
    }


    @Override
    public void onUserEdit(long id, String[] values) {
        Intent intent = new Intent(this, EditUserActivity.class);
        intent.putExtra(DBAdapter.TAG_INTENT_LONG, id);
        intent.putExtra(DBAdapter.TAG_INTENT_STRING, values);
        startActivityForResult(intent, UPDATE_REQUEST_CODE);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d("STR", getClass().getSimpleName() + " in onResume");
        adapter.notifyDataSetChanged();
    }

    private void reLoadData() {
        Log.d("STR", getClass().getSimpleName() + " in reLoadData");
        DataBaseInstance dbi = DataBaseInstance.getInstance(this);
        adapter.clearAdapter();
        dbi.loadUserFromDAO();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        RefWatcher refWatcher = MyApplication.getRefWatcher(this);
        refWatcher.watch(this);
    }
}
