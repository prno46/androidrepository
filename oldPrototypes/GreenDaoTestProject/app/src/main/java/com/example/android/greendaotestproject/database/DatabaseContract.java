package com.example.android.greendaotestproject.database;

import org.greenrobot.greendao.async.AsyncOperationListener;

/**
 * Created by str on 24.08.17.
 *
 */


public interface DatabaseContract {

    void setDatabaseConnection();
    void setOperationListener (AsyncOperationListener listener);
}
