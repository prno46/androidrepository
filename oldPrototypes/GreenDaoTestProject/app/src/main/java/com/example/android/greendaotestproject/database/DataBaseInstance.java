package com.example.android.greendaotestproject.database;

import android.content.Context;
import android.util.Log;
import com.example.android.greendaotestproject.model.DaoMaster;
import com.example.android.greendaotestproject.model.DaoSession;
import com.example.android.greendaotestproject.model.User;
import org.greenrobot.greendao.async.AsyncOperationListener;
import org.greenrobot.greendao.async.AsyncSession;


/**
 * Created by str on 24.08.17.
 *
 */

public class DataBaseInstance  implements DatabaseContract {

    private  Context context;
    private static DataBaseInstance instance;
    private DaoSession daoSession;
    private AsyncSession asyncSession;

    private DataBaseInstance (Context context) {
        this.context = context;
    }

    public static synchronized DataBaseInstance getInstance(Context context) {
        if(instance == null) {
            instance = new DataBaseInstance(context.getApplicationContext());
        }
        return instance;
    }

    @Override
    public void setDatabaseConnection() {
        daoSession = new DaoMaster(new DaoMaster.DevOpenHelper(context, "greendao_test.db" )
                .getReadableDatabase())
                .newSession();
        startAsyncSession();
    }

    public void insertNewUser(User user){
        asyncSession.insert(user);
    }

    private void startAsyncSession() {
        asyncSession = daoSession.startAsyncSession();
    }

    public void loadUserFromDAO(){
        asyncSession.loadAll(User.class);
    }

    public void updateUserData(User user) {
        asyncSession.update(user);
    }

    public void deleteUserFromDatabase(User user) {
        asyncSession.delete(user);
    }

    @Override
    public void setOperationListener (AsyncOperationListener listener) {
        asyncSession.setListener(listener);
    }

    public long getNextAvailableUserId() {
        long nextId = daoSession.getUserDao().count();
        nextId = nextId+2;
        return nextId;
    }
}
