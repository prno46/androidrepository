package com.example.android.greendaotestproject;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class AddNewUserActivity extends AppCompatActivity {

    public static String INSERT_NEW_USER = "values";
    EditText newUserName;
    EditText userCreationDate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_new_user);

        newUserName = findViewById(R.id.editText_userName);
        userCreationDate = findViewById(R.id.editText_creationDate);
    }

    public void onSavingNewUser(View view) {
        String[] values = new String[2];
        if(newUserName.getText() != null) {
            values[0] = newUserName.getText().toString();
            values[1] = userCreationDate.getText().toString();
            sendIntentBack(values);
        }
        finish();
    }

    private void sendIntentBack(String[] values) {
        Intent intent = new Intent();
        intent.putExtra(INSERT_NEW_USER, values);
        setResult(RESULT_OK,intent);
    }


}
