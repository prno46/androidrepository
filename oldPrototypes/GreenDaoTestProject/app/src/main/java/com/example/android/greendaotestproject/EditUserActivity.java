package com.example.android.greendaotestproject;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import com.example.android.greendaotestproject.recycler.view.DBAdapter;
import com.squareup.leakcanary.RefWatcher;

public class EditUserActivity extends AppCompatActivity {

    EditText name;
    EditText createdAt;
    long userId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_user);

        name = findViewById(R.id.editText_userName);
        createdAt = findViewById(R.id.editText_creationDate);

        if((getIntent().hasExtra(DBAdapter.TAG_INTENT_LONG))
                                        && (getIntent().hasExtra(DBAdapter.TAG_INTENT_STRING))) {

            userId = getIntent().getLongExtra(DBAdapter.TAG_INTENT_LONG, -1);

            String uName = getIntent().getStringArrayExtra(DBAdapter.TAG_INTENT_STRING)[0];
            String uCreationDate = getIntent().getStringArrayExtra(DBAdapter.TAG_INTENT_STRING)[1];

            name.setText(uName);
            createdAt.setText(uCreationDate);
        }
    }

    public void onEditedDataSave(View view) {
        String[] nameAndCreationDate = {name.getText().toString(), createdAt.getText().toString()};
        Intent intent = new Intent();
        intent.putExtra(DBAdapter.TAG_INTENT_LONG, userId);
        intent.putExtra(DBAdapter.TAG_INTENT_STRING, nameAndCreationDate);
        setResult(MainActivity.UPDATE_REQUEST_CODE, intent);
        finish();
    }

    public void onEditedDataDelete(View view) {
        String[] nameAndCreationDate = {name.getText().toString(), createdAt.getText().toString()};
        Intent intent = new Intent();
        intent.putExtra(DBAdapter.TAG_INTENT_LONG, userId);
        intent.putExtra(DBAdapter.TAG_INTENT_STRING, nameAndCreationDate);
        setResult(MainActivity.DELETE_REQUEST_CODE, intent);
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        RefWatcher refWatcher = MyApplication.getRefWatcher(this);
        refWatcher.watch(this);
    }
}
