package com.example.android.android_me.ui;

import android.content.Intent;
import android.content.res.Configuration;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.GridView;
import android.widget.Toast;

import com.example.android.android_me.R;
import com.example.android.android_me.data.AndroidImageAssets;

public class MainActivity extends AppCompatActivity implements MasterListFragment.OnImageClickListener{

    private int headIndex;
    private int bodyIndex;
    private int legsIndex;

    public static final String HEAD_INDEX = "headIndex";
    public static final String BODY_INDEX = "bodyIndex";
    public static final String LEGS_INDEX = "legsIndex";

    private boolean mTwoPane;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

            //two-pane layout
            if (findViewById(R.id.android_me_linear_layout) != null) {
                mTwoPane = true;

                GridView gridView = (GridView) findViewById(R.id.images_grid_view);
                gridView.setNumColumns(2);

                Button nextButton = (Button) findViewById(R.id.next_button);
                nextButton.setVisibility(View.GONE);

                if (savedInstanceState == null) {
                    BodyPartFragment headFragment = new BodyPartFragment();
                    headFragment.setImageIds(AndroidImageAssets.getHeads());

                    BodyPartFragment bodyPartFragment = new BodyPartFragment();
                    bodyPartFragment.setImageIds(AndroidImageAssets.getBodies());

                    BodyPartFragment legsPartFragment = new BodyPartFragment();
                    legsPartFragment.setImageIds(AndroidImageAssets.getLegs());

                    FragmentManager fragmentManager = getSupportFragmentManager();
                    fragmentManager.beginTransaction()
                            .add(R.id.head_container, headFragment)
                            .add(R.id.body_container, bodyPartFragment)
                            .add(R.id.legs_container, legsPartFragment)
                            .commit();
                }

            } else {
                mTwoPane = false;
            }

    }


    @Override
    public void onImageSelected(int imagePosition) {

        int bodyPartNumber = imagePosition/12;
        int listIndex = imagePosition - 12*bodyPartNumber;

        if(mTwoPane) {
            BodyPartFragment newFragment = new BodyPartFragment();

            switch (bodyPartNumber){
                case 0:
                    newFragment.setImageIds(AndroidImageAssets.getHeads());
                    newFragment.setListIndex(listIndex);
                    getSupportFragmentManager().beginTransaction()
                            .replace(R.id.head_container, newFragment)
                            .commit();
                    break;
                case  1:
                    newFragment.setImageIds(AndroidImageAssets.getBodies());
                    newFragment.setListIndex(listIndex);
                    getSupportFragmentManager().beginTransaction()
                            .replace(R.id.body_container, newFragment)
                            .commit();
                    break;
                case 2:
                    newFragment.setImageIds(AndroidImageAssets.getLegs());
                    newFragment.setListIndex(listIndex);
                    getSupportFragmentManager().beginTransaction()
                            .replace(R.id.legs_container, newFragment)
                            .commit();
                    break;
                default: break;
            }

        }else {

            switch (bodyPartNumber) {
                case 0:
                    headIndex = listIndex;
                    break;
                case 1:
                    bodyIndex = listIndex;
                    break;
                case 2:
                    legsIndex = listIndex;
                    break;
                default:
                    Toast.makeText(this, "no such image " + imagePosition, Toast.LENGTH_SHORT).show();
            }

            Bundle bundle = new Bundle();
            bundle.putInt(HEAD_INDEX, headIndex);
            bundle.putInt(BODY_INDEX, bodyIndex);
            bundle.putInt(LEGS_INDEX, legsIndex);

            final Intent intent = new Intent(this, AndroidMeActivity.class);
            intent.putExtras(bundle);

            Button nextButton = (Button) findViewById(R.id.next_button);
            nextButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(intent);
                }
            });
        }
    }

}
