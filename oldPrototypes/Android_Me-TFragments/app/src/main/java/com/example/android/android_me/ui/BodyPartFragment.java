package com.example.android.android_me.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.example.android.android_me.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by str on 16.07.17.
 */

public class BodyPartFragment extends Fragment {

    private List<Integer> ImageIds;
    private int listIndex;

    private final String CURRENT_IMAGE_LIST_VALUE = "currentImageValue";
    private final String CURRENT_INDEX_VALUE = "currentIndexValue";

    public BodyPartFragment() {}

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        if(savedInstanceState != null) {
            ImageIds = savedInstanceState.getIntegerArrayList(CURRENT_IMAGE_LIST_VALUE);
            listIndex = savedInstanceState.getInt(CURRENT_INDEX_VALUE);
        }

        View rootView = inflater.inflate(R.layout.fragment_body_part, container,false);

        final ImageView imageView = (ImageView) rootView.findViewById(R.id.body_part_image_view);

        if(ImageIds != null) {
            imageView.setImageResource(ImageIds.get(listIndex));

            imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(listIndex < ImageIds.size()-1) {
                        listIndex ++;
                    } else {
                        listIndex = 0;
                    }
                    imageView.setImageResource(ImageIds.get(listIndex));
                }
            });
        }

        return rootView;
    }


    public void setImageIds(List<Integer> imageIds) {
        this.ImageIds = imageIds;
    }

    public void setListIndex(int listIndex) {
        this.listIndex = listIndex;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putIntegerArrayList(CURRENT_IMAGE_LIST_VALUE, (ArrayList<Integer>) ImageIds);
        outState.putInt(CURRENT_INDEX_VALUE, listIndex);
    }
}
