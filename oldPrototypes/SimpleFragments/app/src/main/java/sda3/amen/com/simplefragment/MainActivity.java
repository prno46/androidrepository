package sda3.amen.com.simplefragment;

import android.Manifest;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import org.json.JSONException;

import java.io.IOException;

import butterknife.ButterKnife;
import butterknife.OnClick;
import sda3.amen.com.simplefragment.interfaces.IFragmentSwitcher;


public class MainActivity extends AppCompatActivity implements IFragmentSwitcher {

    // zapis produktów do pliku
    @OnClick(R.id.buttonLoad)
    public void load(){
        try {
            DataProvider.INSTANCE.loadProducts();
        } catch (JSONException|IOException e) {
            Toast.makeText(this, "Exception parsing JSON from Gson.", Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
    }

    // zapis produktów do pliku
    @OnClick(R.id.buttonSave)
    public void save(){
        try {
            DataProvider.INSTANCE.saveProducts();
        } catch (JSONException e) {
            Toast.makeText(this, "Exception parsing JSON from Gson.", Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
    }

    // button dodawanie
    @OnClick(R.id.buttonBlank)
    protected void blank() {
        FragmentManager manager = getFragmentManager();
        BlankFragment fragment = null;
        fragment = new BlankFragment();

        FragmentTransaction ft = manager.beginTransaction();
        ft.replace(R.id.replaceableFrame, fragment).commit();
    }

    // button lista
    @OnClick(R.id.buttonSecond)
    protected void second() {
        FragmentManager manager = getFragmentManager();
        SecondFragment fragment = null;
        fragment = new SecondFragment();
        fragment.setFragmentSwitcher(this);

        FragmentTransaction ft = manager.beginTransaction();
        ft.replace(R.id.replaceableFrame, fragment).commit();
    }

    @OnClick(R.id.button_save_to_sqlDatabase)
    protected void saveToDatabase() {
        // action to save to database

    }

    @OnClick(R.id.button_load_from_sqlDatabase)
    protected void loadFromDatabase() {
        //action to load from database
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        isStoragePermissionGranted();
    }

    @Override
    public void showProduct(int position) {
        // tworzymy fragment do wyświetlenia
        BlankFragment fragment = new BlankFragment();
        // stworzenie argumentu
        Bundle bundle = new Bundle();
        bundle.putInt("ItemPosition", position);

        //przekazanie argumentu do fragmentu
        fragment.setArguments(bundle);

        // uruchomienie activity
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.replace(R.id.replaceableFrame, fragment).commit();
    }



    public void isStoragePermissionGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                // uprawnienia wcześniej przyznane
                Log.d(getClass().getName(), "Permission already granted");
            } else {
                Log.d(getClass().getName(), "Requesting permission");
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        1);
            }
        } else {
            Log.d(getClass().getName(), "Permission already granted");
        }
    }
}
