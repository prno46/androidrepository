package sda3.amen.com.simplefragment;


import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnItemClick;
import sda3.amen.com.simplefragment.interfaces.IFragmentSwitcher;
import sda3.amen.com.simplefragment.model.Product;


/**
 * A simple {@link Fragment} subclass.
 */
public class SecondFragment extends Fragment {

    @BindView(R.id.productList)
    protected ListView productList;

    @OnItemClick(R.id.productList)
    protected void itemSelected(int position) {
        switcher.showProduct(position);
    }

    // tak na prawde to jest MainActivity
    private IFragmentSwitcher switcher;
    /**
     * Ustawianie activity/switchera
     */
    public void setFragmentSwitcher(IFragmentSwitcher switcher) {
        this.switcher = switcher;
    }

    private ArrayAdapter<Product> productArrayAdapter;

    public SecondFragment() {
        // we fragmencie potrzebny jest pusty konstruktor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_second, container, false);
        // bindowanie butterknife
        ButterKnife.bind(this, v);

        // adapter i spinner - podpięcie
        productArrayAdapter = new ArrayAdapter<>(getContext(),
                android.R.layout.simple_list_item_1,
                DataProvider.INSTANCE.getProducts());
        productList.setAdapter(productArrayAdapter);

        return v;
    }
}
