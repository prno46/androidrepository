package com.str.retrofittraining;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        GitHubService service = ServiceGenerator.createService(GitHubService.class);

        // our method from GitHubService.class
        Call<List<GitHubRepo>> call = service.reposForUser("fs-opensource");

        call.enqueue(new Callback<List<GitHubRepo>>() {
            @Override
            public void onResponse(Call<List<GitHubRepo>> call, Response<List<GitHubRepo>> response) {
                if(response.isSuccessful()) {
                    for(GitHubRepo repo : response.body()){
                        Log.d("REPO: ", repo.getName() + " (ID: " + repo.getId() + ")");
                    }
                } else {
                    Log.e("REQUEST FAILED: ", "Cannot request GitHub repositories");
                }
            }

            @Override
            public void onFailure(Call<List<GitHubRepo>> call, Throwable t) {
                Log.e("ERROR FETCHING REPOS : ", t.getMessage());
            }
        });
    }
}
