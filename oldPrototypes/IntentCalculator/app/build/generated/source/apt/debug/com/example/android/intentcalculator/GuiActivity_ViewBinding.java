// Generated code from Butter Knife. Do not modify!
package com.example.android.intentcalculator;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import java.lang.IllegalStateException;
import java.lang.Override;

public class GuiActivity_ViewBinding implements Unbinder {
  private GuiActivity target;

  private View view2131427424;

  private View view2131427425;

  private View view2131427426;

  private View view2131427427;

  @UiThread
  public GuiActivity_ViewBinding(GuiActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public GuiActivity_ViewBinding(final GuiActivity target, View source) {
    this.target = target;

    View view;
    target.firstNumber = Utils.findRequiredViewAsType(source, R.id.editText_firstNumber, "field 'firstNumber'", EditText.class);
    target.secondNumber = Utils.findRequiredViewAsType(source, R.id.editText_secondNumber, "field 'secondNumber'", EditText.class);
    target.showResult = Utils.findRequiredViewAsType(source, R.id.textView_showResult, "field 'showResult'", TextView.class);
    view = Utils.findRequiredView(source, R.id.button_add, "method 'addOperation'");
    view2131427424 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.addOperation();
      }
    });
    view = Utils.findRequiredView(source, R.id.button_sub, "method 'subOperation'");
    view2131427425 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.subOperation();
      }
    });
    view = Utils.findRequiredView(source, R.id.button_mult, "method 'multOperation'");
    view2131427426 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.multOperation();
      }
    });
    view = Utils.findRequiredView(source, R.id.button_div, "method 'divOperation'");
    view2131427427 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.divOperation();
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    GuiActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.firstNumber = null;
    target.secondNumber = null;
    target.showResult = null;

    view2131427424.setOnClickListener(null);
    view2131427424 = null;
    view2131427425.setOnClickListener(null);
    view2131427425 = null;
    view2131427426.setOnClickListener(null);
    view2131427426 = null;
    view2131427427.setOnClickListener(null);
    view2131427427 = null;
  }
}
