package com.example.android.intentcalculator;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class LogicActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_logic);

        if(getIntent().hasExtra(MyConstants.KEY)) {
            double[] array = getIntent().getDoubleArrayExtra(MyConstants.KEY);
            String operation = getIntent().getStringExtra(MyConstants.OPERATION);
            String result = getResult(operation,array);

            sendIntentBack(result);
            finish();
        }

    }


    private String getResult (String operation, double[] array) {

        switch(operation){
            case MyConstants.ADD:
                return String.valueOf(array[0] + array[1]);

            case MyConstants.SUB:
                return String.valueOf(array[0]-array[1]);

            case MyConstants.MULT:
                return String.valueOf(array[0]*array[1]);

            case MyConstants.DIV:
                return String.valueOf(array[0]/array[1]);

            default:
                return "null";
        }
    }

    private void sendIntentBack(String res){
        Intent data = new Intent();
        data.putExtra(MyConstants.KEY, res);
        setResult(RESULT_OK,data);
    }

}
