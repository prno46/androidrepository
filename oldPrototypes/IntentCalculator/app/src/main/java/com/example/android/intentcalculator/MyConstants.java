package com.example.android.intentcalculator;

/**
 * Created by str on 11.06.17.
 */

public class MyConstants {

    private MyConstants() {}

    public static final String KEY = "key";
    public static final int MY_REQUEST_CODE = 46;

    /**
     * Constants for operations to calculate values
     * used in switch method in LogicActivity
     */

    public static final String OPERATION = "opeartion";
    public static final String ADD = "add";
    public static final String SUB = "sub";
    public static final String MULT = "mult";
    public static final String DIV = "div";


}
