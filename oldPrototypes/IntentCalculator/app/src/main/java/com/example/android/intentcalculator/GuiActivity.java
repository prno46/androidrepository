package com.example.android.intentcalculator;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class GuiActivity extends AppCompatActivity {

    @BindView(R.id.editText_firstNumber)
    EditText firstNumber;

    @BindView(R.id.editText_secondNumber)
    EditText secondNumber;

    @BindView(R.id.textView_showResult)
    TextView showResult;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gui);
        ButterKnife.bind(this);

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if(requestCode == MyConstants.MY_REQUEST_CODE && resultCode == RESULT_OK){
            String result = data.getStringExtra(MyConstants.KEY);
            showResult.setText(result);
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    @OnClick(R.id.button_add)
    public void addOperation() {
        sendIntent(MyConstants.KEY,MyConstants.ADD);

    }

    @OnClick(R.id.button_sub)
    public void subOperation() {
        sendIntent(MyConstants.KEY,MyConstants.SUB);
    }

    @OnClick(R.id.button_mult)
    public void multOperation() {
        sendIntent(MyConstants.KEY,MyConstants.MULT);
    }

    @OnClick(R.id.button_div)
    public void divOperation() {
        sendIntent(MyConstants.KEY,MyConstants.DIV);
    }

    private void sendIntent(String key, String operation) {
        double first = Double.parseDouble(firstNumber.getText().toString());
        double second = Double.parseDouble(secondNumber.getText().toString());
        double[] array = {first,second};

        divByZeroCheck(operation,array);

        Intent intent = new Intent(this,LogicActivity.class);
        intent.putExtra(key,array);
        intent.putExtra(MyConstants.OPERATION,operation);
        startActivityForResult(intent,MyConstants.MY_REQUEST_CODE);
    }

    private void divByZeroCheck(String operation, double[] array) {
        if (operation.equalsIgnoreCase(MyConstants.DIV) && array[1] == 0){
            Toast.makeText(this, "Dividing by zero is not allowed", Toast.LENGTH_LONG).show();
        }
    }

}
