package com.example.android.tasktimerdatabase;

import android.database.Cursor;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

/**
 * Created by str on 13.08.17.
 */

class CursorRecyclerViewAdapter extends RecyclerView.Adapter<CursorRecyclerViewAdapter.TaskViewHolder> {


    private Cursor cursor;
    private OnTaskClickListener mListener;

    interface OnTaskClickListener {
        void onEditClick(Task task);
        void onDeleteClick(Task task);
    }


    public CursorRecyclerViewAdapter(Cursor cursor, OnTaskClickListener listener) {
        Log.d("STR", getClass().getSimpleName() + " Constructor method");

        this.cursor = cursor;
        this.mListener = listener;
    }

    @Override
    public TaskViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Log.d("STR", getClass().getSimpleName() + " onCreateViewHolder method");

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.task_list_items,  parent, false);
        return new TaskViewHolder(view);
    }

    @Override
    public void onBindViewHolder(TaskViewHolder holder, int position) {
        Log.d("STR", getClass().getSimpleName() + " onBindViewHolder method");

        if((cursor == null) || (cursor.getCount() == 0)) {
            holder.name.setText(R.string.instructions_heading);
            holder.description.setText(R.string.instructions);

            holder.editButton.setVisibility(View.GONE);
            holder.deleteButton.setVisibility(View.GONE);
        }else {
            if(!cursor.moveToPosition(position)) {
                throw new IllegalStateException("could not move cursor to position "+position);
            }
            final Task task = new Task(cursor.getLong(cursor.getColumnIndex(TaskContract.Columns._ID))
                    ,cursor.getString(cursor.getColumnIndex(TaskContract.Columns.TASKS_NAME))
                    ,cursor.getString(cursor.getColumnIndex(TaskContract.Columns.TASKS_DESCRIPTION))
                    ,cursor.getInt(cursor.getColumnIndex(TaskContract.Columns.TASKS_SORTORDER)));

            holder.name.setText(task.getName());
            holder.description.setText(task.getDescription());
            holder.editButton.setVisibility(View.VISIBLE);
            holder.deleteButton.setVisibility(View.VISIBLE);

            // onClickListener for buttons edit and delete Task
            View.OnClickListener buttonListener = new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    switch(view.getId()){
                        case R.id.tli_edit:
                            if(mListener != null) {
                                mListener.onEditClick(task);
                            }
                            break;
                        case R.id.tli_delete:
                            if (mListener != null){
                                mListener.onDeleteClick(task);
                            }
                            break;
                        default:
                            throw new IllegalArgumentException(getClass().getSimpleName() + " buttonListener.onClick");
                    }
                }
            };

            holder.editButton.setOnClickListener(buttonListener);
            holder.deleteButton.setOnClickListener(buttonListener);
        }
    }

    @Override
    public int getItemCount() {
        Log.d("STR", getClass().getSimpleName() + " getItemCount method");

        if((cursor == null) || (cursor.getCount() == 0)) {
            return 1;
        }else {

            return cursor.getCount();
        }
    }

    /**
     * Swap in a new Cursor, returning the old Cursor.
     * The returned old  Cursor is <em>not</em> closed.
     * (called when the cursor used by the adapter has changed)
     * (the method is created cause there is no Cursor adapter for RecyclerView)
     *
     * @param newCursor The new Cursor to be used
     * @return Returns the previously set Cursor, or null if there wasn't one.
     * If the given new Cursor is the same instance as the previously set
     * Cursor, null is also returned
     */
    Cursor swapCursor(Cursor newCursor) {
        Log.d("STR", getClass().getSimpleName() + " swapCursor method");

        if (newCursor == cursor) {
            return null;
        }
        final Cursor oldCursor = cursor;
        cursor = newCursor;
        if(newCursor != null) {
            // notify the observers about new cursor
            notifyDataSetChanged();
        }else {
            // notify observers about the lack of data set
            notifyItemRangeRemoved(0,getItemCount());
        }
        return oldCursor;
    }


    static class TaskViewHolder extends RecyclerView.ViewHolder {

        TextView name = null;
        TextView description = null;
        ImageButton editButton = null;
        ImageButton deleteButton = null;

        public TaskViewHolder(View itemView) {
            super(itemView);

            Log.d("STR", getClass().getSimpleName() + " TaskViewHolder method");

            this.name = itemView.findViewById(R.id.tli_name);
            this.description = itemView.findViewById(R.id.tli_description);
            this.editButton= itemView.findViewById(R.id.tli_edit);
            this.deleteButton = itemView.findViewById(R.id.tli_delete);
        }
    }

}
