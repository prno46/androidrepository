package com.example.android.tasktimerdatabase;

import java.io.Serializable;

/**
 * Class is to use to store the values of the fields that we put to database
 * it is a taskPojo. It is going to be used with bundles so it has to implement Serializable
 * interface.
 *
 * Created by str on 13.08.17.
 */

class Task implements Serializable{

    // date of the creation of class as a serial version to deserialize
    // the instance of this class correctly
    public static final long serialVersionUID = 20170813L;

    private long m_Id;
    private final String mName;
    private final String mDescription;
    private final int sortOrder;


    public Task(long id, String mName, String mDescription, int sortOrder) {
        this.m_Id = id;
        this.mName = mName;
        this.mDescription = mDescription;
        this.sortOrder = sortOrder;
    }


    public long getId() {
        return m_Id;
    }

    public String getName() {
        return mName;
    }

    String getDescription() {
        return mDescription;
    }

    int getSortOrder() {
        return sortOrder;
    }

    public void setId(long id) {
        this.m_Id = id;
    }

    @Override
    public String toString() {
        return "Task{" +
                "m_Id=" + m_Id +
                ", mName='" + mName + '\'' +
                ", mDescription='" + mDescription + '\'' +
                ", sortOrder=" + sortOrder +
                '}';
    }
}
