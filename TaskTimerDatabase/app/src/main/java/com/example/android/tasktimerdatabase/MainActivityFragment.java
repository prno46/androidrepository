package com.example.android.tasktimerdatabase;

import android.database.Cursor;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.security.InvalidParameterException;

/**
 * A placeholder fragment containing a simple view.
 * Uses cursorLoader.
 * Displays / contains Recycler view.
 */
public class MainActivityFragment extends Fragment implements LoaderManager.LoaderCallbacks <Cursor> {

    public static final int LOADER_ID = 0;
    private CursorRecyclerViewAdapter adapter;


    public MainActivityFragment() {
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        Log.d("STR", getClass().getSimpleName() + " onActivityCreated method");

        super.onActivityCreated(savedInstanceState);
        getLoaderManager().initLoader(LOADER_ID, null, this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.d("STR", getClass().getSimpleName() + " onCreateView method");

        View view = inflater.inflate(R.layout.fragment_main, container, false);

        RecyclerView recyclerView =  view.findViewById(R.id.task_list);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        // passing null cause we do not have data yet
        // the data will be passed in onLoadFinished method
        adapter = new CursorRecyclerViewAdapter(null, (CursorRecyclerViewAdapter.OnTaskClickListener) getActivity());
        recyclerView.setAdapter(adapter);

        return view;
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        Log.d("STR", getClass().getSimpleName() + " onCreateLoader method");

        // columns fetched from database
        String[] projection = {TaskContract.Columns._ID, TaskContract.Columns.TASKS_NAME
                                , TaskContract.Columns.TASKS_DESCRIPTION
                                , TaskContract.Columns.TASKS_SORTORDER};

        //COLLATE NOCASE - to show the tasks in alphabetical order
        String sortOrder = TaskContract.Columns.TASKS_SORTORDER + "," +TaskContract.Columns.TASKS_NAME+" COLLATE NOCASE";

        switch (id) {
           case LOADER_ID:
               // fragment is attached to activity that is why we pass getActivity instead of getContext
               return new CursorLoader(getActivity(), TaskContract.CONTENT_URI,
                                projection,
                                null,
                                null,
                                sortOrder);
           default:
               throw new InvalidParameterException(getClass().getSimpleName()+" onCreateLoader called with invalid id " +id);
       }

    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        Log.d("STR", getClass().getSimpleName() + " onLoadFinished method");
        adapter.swapCursor(data);
        int count = adapter.getItemCount();

        Log.v("STR","onLoadFinished: count = " + count);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        Log.d("STR", getClass().getSimpleName() + " onLoadReset method");
        adapter.swapCursor(null);

    }
}
