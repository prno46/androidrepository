package com.example.android.tasktimerdatabase;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by str on 06.08.17.
 *
 * Only the Application Content Provider should use this class
 * {@link AppProvider}
 */

class AppDatabase extends SQLiteOpenHelper {

    public static final String TAG = "MyDataBase";

    public static final String DATABASE_NAME = "TaskTimer.db";
    public static final int DATABASE_VERSION = 1;

    private static AppDatabase instance = null;


    private AppDatabase (Context context){
        super (context, DATABASE_NAME, null , DATABASE_VERSION);
        Log.d("STR", getClass().getSimpleName() + " Constructor method");
    }


    static AppDatabase getInstance(Context context) {
        Log.d("STR", " AppDatabase.class getInstance method");

        if(instance == null) {
            instance = new AppDatabase(context);
        }
        return  instance;
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.d("STR", getClass().getSimpleName() + " onCreate method");

        String sSQL = "CREATE TABLE " + TaskContract.TABLE_NAME + " ("
                + TaskContract.Columns._ID + " INTEGER PRIMARY KEY NOT NULL,"
                + TaskContract.Columns.TASKS_NAME + " TEXT NOT NULL, "
                + TaskContract.Columns.TASKS_DESCRIPTION + " TEXT, "
                + TaskContract.Columns.TASKS_SORTORDER + " INTEGER);";

        db.execSQL(sSQL);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.d("STR", getClass().getSimpleName() + " onUpgrade method");
    }
}
