package com.example.android.tasktimerdatabase;

import android.support.v7.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements CursorRecyclerViewAdapter.OnTaskClickListener,
                                                AddEditActivityFragment.OnSaveClicked, AppDialog.DialogEvents {


    private boolean mTwoPane = false;
    public static final int DIALOG_ID_DELETE = 1;
    private static final int DIALOG_ID_CANCEL_EDIT = 2;

    private AlertDialog mDialog = null;  // dismiss onStop to avoid memory leaks

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d("STR", getClass().getSimpleName() + " onCreate method");

        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // checking if there is a frame to display a fragment for detail content
        // checking if layout task_details_container is activated
        if(findViewById(R.id.task_details_container) != null) {
            mTwoPane = true;
        }

    }

    // removes Fragment when Save button is clicked
    // when in twoPaneMode
    // method from OnSavedClicked
    @Override
    public void onSaveClicked() {
        Log.d("STR", getClass().getSimpleName() + " onSaveClicked method");

        FragmentManager fragmentManager = getSupportFragmentManager();
        Fragment fragment = fragmentManager.findFragmentById(R.id.task_details_container);
        if (fragment != null) {
            getSupportFragmentManager().beginTransaction()
                    .remove(fragment)
                    .commit();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        Log.d("STR", getClass().getSimpleName() + " onCreateOptionsMenu method");
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Log.d("STR", getClass().getSimpleName() + " onOptionsItemSelected method");
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        switch (id) {
            case R.id.menumain_addtask :
                taskEditRequest(null);
                break;
            case R.id.menumain_showDurations:
                break;
            case R.id.menumain_settings:
                break;
            case R.id.menumain_showAbout:
                showAboutDialog();
                break;
            case R.id.menumain_generate:
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    public void showAboutDialog() {
        Log.d("STR", getClass().getSimpleName() + " showAboutDialog method");
        View messageView = getLayoutInflater().inflate(R.layout.about, null, false);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.app_name);
        builder.setIcon(R.mipmap.ic_launcher);

        builder.setView(messageView);

        // adding a button to dismiss the dialog
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if(mDialog != null && mDialog.isShowing()) {
                    mDialog.dismiss();

                }
            }
        });

        mDialog = builder.create();
        mDialog.setCanceledOnTouchOutside(true);

        TextView tv = messageView.findViewById(R.id.about_version);
        tv.setText("v" + BuildConfig.VERSION_NAME);

        mDialog.show();

    }
    // method from OnTaskClickListener
    @Override
    public void onEditClick(Task task) {
        Log.d("STR", getClass().getSimpleName() + " onEditClick method");
        taskEditRequest(task);
    }
    // method from OnTaskClickListener
    @Override
    public void onDeleteClick(Task task) {
        Log.d("STR", getClass().getSimpleName() + " onDeleteClick method");
        AppDialog dialog = new AppDialog();
        Bundle args = new Bundle();
        args.putInt(AppDialog.DIALOG_ID, DIALOG_ID_DELETE);
        args.putString(AppDialog.DIALOG_MESSAGE,getString(R.string.deldiag_message, task.getId(), task.getName()));
        args.putInt(AppDialog.DIALOG_POSITIVE_RID, R.string.deldiag_positive_caption);

        args.putLong("TaskId", task.getId());

        dialog.setArguments(args);
        dialog.show(getSupportFragmentManager(), null);

    }

    private void taskEditRequest(Task task){
        Log.d("STR", getClass().getSimpleName() + " taskEditRequest (private) method");
        if(mTwoPane) {
            AddEditActivityFragment fragment = new AddEditActivityFragment();

            Bundle arguments = new Bundle();
            arguments.putSerializable(Task.class.getSimpleName(), task);
            fragment.setArguments(arguments);

            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.task_details_container, fragment)
                    .commit();

        } else {
            // code for phone users
            Intent detailIntent = new Intent(this, AddEditActivity.class);
            if (task!=null) {
                detailIntent.putExtra(Task.class.getSimpleName(), task); // cause Task class implements serializable
                startActivity(detailIntent);
            }else {
                // for adding a new task
                startActivity(detailIntent);
            }
        }
    }

    @Override
    public void onPositiveDialogResult(int dialogId, Bundle args) {
        Log.d("STR", getClass().getSimpleName() + " onPositiveDialogResult method");
        // for landscape mode
        switch(dialogId) {
            case DIALOG_ID_DELETE:
                long taskId = args.getLong("TaskId");
                if(BuildConfig.DEBUG && taskId == 0) throw new AssertionError("Task ID is zero");
                getContentResolver().delete(TaskContract.buildTaskUri(taskId), null, null);
                break;

            case DIALOG_ID_CANCEL_EDIT:
                // we do not need any action here
                break;
        }
    }

    @Override
    public void onNegativeDialogResult(int dialogId, Bundle args) {
        Log.d("STR", getClass().getSimpleName() + " onNegativeDialogResult method");
        // for landscape mode
        switch (dialogId) {
            case DIALOG_ID_DELETE:
                // no action required
                break;
            case DIALOG_ID_CANCEL_EDIT:
                finish();
                break;
        }
    }

    @Override
    public void onDialogCancelled(int dialogId) {
        Log.d("STR", getClass().getSimpleName() + " onDialogCancelled method");
        // no action required for now
    }

    @Override
    public void onBackPressed() {
        Log.d("STR", getClass().getSimpleName() + " onBackPressed method");
        // for landscape mode
        FragmentManager fragmentManager = getSupportFragmentManager();
        AddEditActivityFragment fragment = (AddEditActivityFragment) fragmentManager.findFragmentById(R.id.task_details_container);
        if((fragment == null) || fragment.canClose()) {
            super.onBackPressed();
        } else {
            // show dialog to confirm quitting
            AppDialog dialog = new AppDialog();
            Bundle args = new Bundle();
            args.putInt(AppDialog.DIALOG_ID, DIALOG_ID_CANCEL_EDIT);
            args.putString(AppDialog.DIALOG_MESSAGE, getString(R.string.cancelEditDiag_message));
            args.putInt(AppDialog.DIALOG_POSITIVE_RID, R.string.cancelEditDiag_positive_caption);
            args.putInt(AppDialog.DIALOG_NEGATIVE_RID, R.string.cancelEditDiag_negative_caption);

            dialog.setArguments(args);
            dialog.show(getSupportFragmentManager(), null);
        }
    }


    @Override
    protected void onStop() {
        Log.d("STR", getClass().getSimpleName() + " onStop method");
        // dismiss the custom dialog "about" when flip the screen
        super.onStop();
        if(mDialog != null && mDialog.isShowing()) {
            mDialog.dismiss();
        }
    }
}
