package com.example.android.tasktimerdatabase;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

/**
 * Created by str on 06.08.17.
 *
 * only class that uses {@link AppDatabase}
 */

public class AppProvider  extends ContentProvider {

    private AppDatabase mOpenHelper;

    public static final UriMatcher sUriMatcher = buildUriMatcher();

    static final String CONTENT_AUTHORITY = "com.example.android.tasktimerdatabase.provider";
    public static final Uri CONTENT_AUTHORITY_URI = Uri.parse("content://" + CONTENT_AUTHORITY);

    private static final int TASKS = 100;
    private static final int TASKS_ID = 101;

    private static final int TIMINGS = 200;
    private static final int TIMINGS_ID = 201;

    private static final int TASK_DURATIONS = 400;
    private static final int TASK_DURATIONS_ID = 401;


    @Override
    public boolean onCreate() {
        Log.d("STR", getClass().getSimpleName() + " onCreate method");
        mOpenHelper = AppDatabase.getInstance(getContext());
        return false;
    }

    @Nullable
    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        Log.v("STR","called with uri " + uri);
        Log.d("STR", getClass().getSimpleName() + " query method");

        final int match = sUriMatcher.match(uri);
        SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();

        switch (match) {
            case TASKS:
                queryBuilder.setTables(TaskContract.TABLE_NAME);
                break;
            case TASKS_ID:
                queryBuilder.setTables(TaskContract.TABLE_NAME);
                long taskId = TaskContract.getTaskId(uri);
                queryBuilder.appendWhere(TaskContract.Columns._ID+ " = " + taskId);
                break;

            /*case TIMINGS:
                queryBuilder.setTables(TimingContract.TABLE_NAME);
                break;
            case TIMINGS_ID:
                queryBuilder.setTables(TimingContract.TABLE_NAME);
                long timingId = TimingContract.getTimingId(uri);
                queryBuilder.appendWhere(TimingContract.Columns._ID+ " = " + timingId);
                break;

            case TASK_DURATIONS:
                queryBuilder.setTables(DurationContract.TABLE_NAME);
                break;
            case TASK_DURATIONS_ID:
                queryBuilder.setTables(DurationContract.TABLE_NAME);
                long durationId = DurationContract.getDurationId(uri);
                queryBuilder.appendWhere(DurationContract.Columns._ID+ " = " + durationId);
                break;*/
            default:
                throw new IllegalArgumentException("Unknown URI" + uri);
        }

        SQLiteDatabase db = mOpenHelper.getReadableDatabase();
        Cursor cursor = queryBuilder.query(db, projection, selection, selectionArgs, null,null,sortOrder);

        // To notify cursor about the changes when we add a new task
        cursor.setNotificationUri(getContext().getContentResolver(), uri);

        return cursor;
    }

    @Nullable
    @Override
    // this is for MIME types
    public String getType(@NonNull Uri uri) {
        Log.d("STR", getClass().getSimpleName() + " getType method");

        final int match = sUriMatcher.match(uri);
        switch(match) {
            case TASKS:
                return TaskContract.CONTENT_TYPE;
            case TASKS_ID:
                return TaskContract.CONTENT_ITEM_TYPE;
            /*case TIMINGS:
                return TimingContract.Timings.CONTENT_TYPE;
            case TIMINGS_ID:
                return TimingContract.Timings.CONTENT_ITEM_TYPE;
            case TASK_DURATIONS:
                return DurationContract.TaskDurations.CONTENT_TYPE;
            case TASK_DURATIONS_ID:
                return DurationContract.TaskDurations.CONTENT_ITEM_TYPE;*/
            default:
                throw new IllegalArgumentException("Unknown URI" + uri);
        }
    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, @Nullable ContentValues contentValues) {
        Log.d("STR", getClass().getSimpleName() + " insert method");

        final int match = sUriMatcher.match(uri);
        final SQLiteDatabase db;
        Uri returnUri;
        long recordId;

        switch(match){
            case TASKS:
                db = mOpenHelper.getWritableDatabase();
                recordId = db.insert(TaskContract.TABLE_NAME, null, contentValues);
                if(recordId >=0) {
                    returnUri = TaskContract.buildTaskUri(recordId);
                }else {
                    throw new android.database.SQLException("Failed to insert into "+uri.toString());
                }
                break;
            case TIMINGS:
                /*db = mOpenHelper.getWritableDatabase();
                recordId = db.insert(TimingContract.);
                if(recordId>=0) {
                    returnUri = TimingContract.Timings.buildTimingUri(recordId);
                }else {
                    throw new android.database.SQLException("Failed to insert into "+uri.toString());
                }
                break;*/
            default:
                throw new IllegalArgumentException("Unknown URI" + uri);
        }
        if(recordId >=0){
            getContext().getContentResolver().notifyChange(uri, null);
        }
        return returnUri;
    }

    @Override
    public int delete(@NonNull Uri uri, @Nullable String selection, @Nullable String[] selectionArgs) {
        Log.d("STR", getClass().getSimpleName() + " delete method");

        final int match = sUriMatcher.match(uri);
        final SQLiteDatabase db;
        String selectionCriteria;
        int count;

        switch (match) {
            case TASKS:
                db= mOpenHelper.getWritableDatabase();
                count = db.delete(TaskContract.TABLE_NAME, selection, selectionArgs);
                break;

            case TASKS_ID:
                db = mOpenHelper.getWritableDatabase();
                long taskId = TaskContract.getTaskId(uri);
                selectionCriteria = TaskContract.Columns._ID + " = " + taskId;
                if((selection != null) && (selection.length()>0)) {
                    selectionCriteria += " AND (" + selection + ")";
                }
                count = db.delete(TaskContract.TABLE_NAME, selectionCriteria, selectionArgs);
                break;

            /*case TIMINGS:
                db= mOpenHelper.getWritableDatabase();
                count = db.delete(TimingContract.TABLE_NAME, selection, selectionArgs);
                break;

            case TIMINGS_ID:
                db = mOpenHelper.getWritableDatabase();
                long timingsId = TimingContract.getTaskId(uri);
                selectionCriteria = TimingContract.Columns._ID + " = " + timingsId;
                if((selection != null) && (selection.length()>0)) {
                    selectionCriteria += " AND (" + selection + ")";
                }
                count = db.delete(TimingContract.TABLE_NAME, selectionCriteria, selectionArgs);
                break;*/

            default:
                throw new IllegalArgumentException("Unknown URI " + uri);
        }

        //to notify about the changes in database -> cursorLoader
        if(count > 0){
            getContext().getContentResolver().notifyChange(uri, null);
        }
        return count;
    }

    @Override
    public int update(@NonNull Uri uri, @Nullable ContentValues values, @Nullable String selection, @Nullable String[] selectionArgs) {
        Log.d("STR", getClass().getSimpleName() + " update method");

        final int match = sUriMatcher.match(uri);
        final SQLiteDatabase db;
        String selectionCriteria;
        int count;

        switch (match) {
            case TASKS:
                db= mOpenHelper.getWritableDatabase();
                count = db.update(TaskContract.TABLE_NAME, values, selection, selectionArgs);
                break;

            case TASKS_ID:
                db = mOpenHelper.getWritableDatabase();
                long taskId = TaskContract.getTaskId(uri);
                selectionCriteria = TaskContract.Columns._ID + " = " + taskId;
                if((selection != null) && (selection.length()>0)) {
                    selectionCriteria += " AND (" + selection + ")";
                }
                count = db.update(TaskContract.TABLE_NAME, values, selectionCriteria, selectionArgs);
                break;

            /*case TIMINGS:
                db= mOpenHelper.getWritableDatabase();
                count = db.update(TimingContract.TABLE_NAME, values, selection, selectionArgs);
                break;

            case TIMINGS_ID:
                db = mOpenHelper.getWritableDatabase();
                long timingsId = TimingContract.getTaskId(uri);
                selectionCriteria = TimingContract.Columns._ID + " = " + timingsId;
                if((selection != null) && (selection.length()>0)) {
                    selectionCriteria += " AND (" + selection + ")";
                }
                count = db.update(TimingContract.TABLE_NAME, values, selectionCriteria, selectionArgs);
                break;*/

            default:
                throw new IllegalArgumentException("Unknown URI " + uri);
        }

        // to notify about the changes in database -> cursorLoader
        if(count > 0){
            getContext().getContentResolver().notifyChange(uri, null);
        }
        return count;
    }

    /**
     * Aids to match uri in content provider when we need to match
     * against a Uri. Result is used to build query return type, insert
     * or delete a row.
     *
     * @return matcher
     */
    private static UriMatcher buildUriMatcher() {
        Log.d("STR", " AppProvider.class  buildUriMatcher (static) method");

        final UriMatcher matcher = new UriMatcher(UriMatcher.NO_MATCH);
        matcher.addURI(CONTENT_AUTHORITY, TaskContract.TABLE_NAME, TASKS);
        matcher.addURI(CONTENT_AUTHORITY, TaskContract.TABLE_NAME + "/#", TASKS_ID);

       /* matcher.addURI(CONTENT_AUTHORITY, TimingContract.TABLE_NAME, TIMINGS);
        matcher.addURI(CONTENT_AUTHORITY, TimingContract.TABLE_NAME + " /#", TIMINGS_ID);

        matcher.addURI(CONTENT_AUTHORITY, DurationContract.TABLE_NAME, TASK_DURATIONS);
        matcher.addURI(CONTENT_AUTHORITY, DurationContract.TABLE_NAME + " /#", TASK_DURATIONS_ID);*/

        return matcher;
    }
}
