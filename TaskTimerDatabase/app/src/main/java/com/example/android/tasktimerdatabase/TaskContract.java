package com.example.android.tasktimerdatabase;

import android.content.ContentUris;
import android.net.Uri;
import android.provider.BaseColumns;
import android.util.Log;

import static com.example.android.tasktimerdatabase.AppProvider.CONTENT_AUTHORITY;
import static com.example.android.tasktimerdatabase.AppProvider.CONTENT_AUTHORITY_URI;

/**
 * Created by str on 06.08.17.
 */

public class TaskContract {

    static final String TABLE_NAME = "Tasks";


    public static class Columns {

        public static final String _ID = BaseColumns._ID;
        public static final String TASKS_NAME = "Name";
        public static final String TASKS_DESCRIPTION = "Description";
        public static final String TASKS_SORTORDER = "SortOrder";

        private Columns() {}
    }

    /**
     * The URI to access the Task table using a content provider (AppProvider)
     * it's value -> content://com.example.android.tasktimerdatabase.provider/Tasks
     * where Tasks is a table name
     */
    public static final Uri CONTENT_URI = Uri.withAppendedPath(CONTENT_AUTHORITY_URI, TABLE_NAME);


    /**
     * Those Uri's below are to use with MIME TYPES -> method getType in AppProvider class
     *
     */
    static final String CONTENT_TYPE = "vnd.android.cursor.dir/vnd." + CONTENT_AUTHORITY + "." + TABLE_NAME;
    static final String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/vnd." + CONTENT_AUTHORITY + "." + TABLE_NAME;

    static Uri buildTaskUri (long taskId) {
        Log.d("STR", " TaskContract.class buildTaskUri method");

        return ContentUris.withAppendedId(CONTENT_URI,taskId);
    }

    static long getTaskId (Uri uri) {
        Log.d("STR", " TaskContract.class getTaskId method");

        return ContentUris.parseId(uri);
    }
}
