package com.example.android.tasktimerdatabase;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

/**
 * A placeholder fragment containing a simple view.
 */
public class AddEditActivityFragment extends Fragment {

    public enum FragmentEditMode {EDIT, ADD}
    private FragmentEditMode mMode;

    private EditText nNameTextView;
    private EditText mDescriptionTextView;
    private EditText mSortOrdertextView;
    private Button mSaveButton;
    private OnSaveClicked mSaveListener;

    // To Remove fragment after saveButton is clicked
    interface OnSaveClicked {
        void onSaveClicked();
    }

    public AddEditActivityFragment() {
    }

    // used with dialog and backPress
    public boolean canClose() {
        Log.d("STR", getClass().getSimpleName() + " canClose method");
        return false;
    }
    @Override
    public void onAttach(Context context) {
        Log.d("STR", getClass().getSimpleName() + " onAttached method");
        super.onAttach(context);
        Activity activity = getActivity();
        if(!(activity instanceof OnSaveClicked)){
            throw new ClassCastException(activity.getClass().getSimpleName()
                    + " must implement AddActivityFragment.OnSaveClicked interface");
        }
        mSaveListener = (OnSaveClicked) activity;
    }


    @Override
    public void onDetach() {
        Log.d("STR", getClass().getSimpleName() + " onDetach method");
        super.onDetach();
        mSaveListener = null;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.d("STR", getClass().getSimpleName() + " onCreateView method");

        View view = inflater.inflate(R.layout.fragment_add_edit, container, false);

        nNameTextView = view.findViewById(R.id.addedit_name);
        mDescriptionTextView = view.findViewById(R.id.addedit_description);
        mSortOrdertextView = view.findViewById(R.id.addedit_sortorder);

        mSaveButton = view.findViewById(R.id.addediit_save);

        Bundle arguments = getArguments(); //arguments were set in MainActivity line 81

        final Task task;
        if (arguments != null) {
            task = (Task) arguments.getSerializable(Task.class.getSimpleName());
            if(task !=null){
                nNameTextView.setText(task.getName());
                mDescriptionTextView.setText(task.getDescription());
                mSortOrdertextView.setText(Integer.toString(task.getSortOrder()));
                mMode = FragmentEditMode.EDIT;

            }else {
                 // add new task
                mMode = FragmentEditMode.ADD;
            }
        } else {

            task = null;
            mMode = FragmentEditMode.ADD;
        }

        mSaveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // update database if at least one field has changed
                int so; // repeated conversion to int
                if (mSortOrdertextView.length() > 0){
                    so = Integer.parseInt(mSortOrdertextView.getText().toString());
                }else {
                    so = 0;
                }

                // this is the way how you create a contentResolver in Fragment
                // you have to do this through the Activity to which this Fragment is attached to
                ContentResolver contentResolver = getActivity().getContentResolver();
                ContentValues contentValues = new ContentValues();

                switch (mMode) {
                    case ADD:
                        if(nNameTextView.length() >0){
                            contentValues.put(TaskContract.Columns.TASKS_NAME, nNameTextView.getText().toString());
                            contentValues.put(TaskContract.Columns.TASKS_DESCRIPTION, mDescriptionTextView.getText().toString());
                            contentValues.put(TaskContract.Columns.TASKS_SORTORDER, so);

                            contentResolver.insert(TaskContract.CONTENT_URI,contentValues);
                        }
                        break;

                    case EDIT:
                        if(!nNameTextView.getText().toString().equals(task.getName())) {
                            contentValues.put(TaskContract.Columns.TASKS_NAME, nNameTextView.getText().toString());
                        }
                        if(!mDescriptionTextView.getText().toString().equals(task.getDescription())){
                            contentValues.put(TaskContract.Columns.TASKS_DESCRIPTION, mDescriptionTextView.getText().toString());
                        }
                        if(so != task.getSortOrder()){
                            contentValues.put(TaskContract.Columns.TASKS_SORTORDER, so);
                        }

                        if(contentValues.size() !=0){
                            contentResolver.update(TaskContract.buildTaskUri(task.getId()), contentValues, null, null);
                        }
                        break;
                }

                if(mSaveListener != null) {
                    mSaveListener.onSaveClicked();
                }
            }
        });

        return view;
    }
}
