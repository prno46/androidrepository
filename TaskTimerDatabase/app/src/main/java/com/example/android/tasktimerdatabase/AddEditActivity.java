package com.example.android.tasktimerdatabase;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;

public class AddEditActivity extends AppCompatActivity implements
                AddEditActivityFragment.OnSaveClicked, AppDialog.DialogEvents {

    private static final int DIALOG_ID_CANCEL_EDIT = 5;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d("STR", getClass().getSimpleName() + " onCreate method");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_edit);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        AddEditActivityFragment editActivityFragment = new AddEditActivityFragment();

        Bundle arguments = getIntent().getExtras();
        editActivityFragment.setArguments(arguments);

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.fragment_container_addEditActivity, editActivityFragment);
        transaction.commit();
    }

    @Override
    public void onSaveClicked() {
        Log.d("STR", getClass().getSimpleName() + " onSaveClicked method");
        finish();
    }

    @Override
    public void onBackPressed() {
        Log.d("STR", getClass().getSimpleName() + " onBackPressed method");
        FragmentManager fragmentManager = getSupportFragmentManager();
        AddEditActivityFragment fragment = (AddEditActivityFragment) fragmentManager.findFragmentById(R.id.fragment);
        if(fragment.canClose()) {
            super.onBackPressed();
        } else {
            // show dialog to confirm quitting
            showConfirmationDialog();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Log.d("STR", getClass().getSimpleName() + " onOptionItemSelected method");
        // for handling the upButton -> backAction
        switch(item.getItemId()) {
            case android.R.id.home:
                AddEditActivityFragment fragment = (AddEditActivityFragment) getSupportFragmentManager()
                        .findFragmentById(R.id.fragment);
                if(fragment.canClose()) {
                    return super.onOptionsItemSelected(item);
                }else {
                    showConfirmationDialog();
                    return  true;
                }
                default:
                    return super.onOptionsItemSelected(item);
        }
    }

    private void showConfirmationDialog() {
        Log.d("STR", getClass().getSimpleName() + " showConfirmationDialog (private) method");
        AppDialog dialog = new AppDialog();
        Bundle args = new Bundle();
        args.putInt(AppDialog.DIALOG_ID, DIALOG_ID_CANCEL_EDIT);
        args.putString(AppDialog.DIALOG_MESSAGE, getString(R.string.cancelEditDiag_message));
        args.putInt(AppDialog.DIALOG_POSITIVE_RID, R.string.cancelEditDiag_positive_caption);
        args.putInt(AppDialog.DIALOG_NEGATIVE_RID, R.string.cancelEditDiag_negative_caption);

        dialog.setArguments(args);
        dialog.show(getSupportFragmentManager(), null);
    }

    @Override
    public void onPositiveDialogResult(int dialogId, Bundle args) {
        Log.d("STR", getClass().getSimpleName() + " onPositiveDialogResult method");
        // allow to continue editing the task
        // no action required


    }

    @Override
    public void onNegativeDialogResult(int dialogId, Bundle args) {
        Log.d("STR", getClass().getSimpleName() + " onNegativeDialogResult method");
        // should return to the task list in main activity
        finish();
    }

    @Override
    public void onDialogCancelled(int dialogId) {
        Log.d("STR", getClass().getSimpleName() + " onDialogCancelled method");
        // no action required
    }
}
